// function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

// function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

// function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// /*! jQuery v3.5.1 | (c) JS Foundation and other contributors | jquery.org/license */
// !function (e, t) {
//   "use strict";

//   "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && "object" == _typeof(module.exports) ? module.exports = e.document ? t(e, !0) : function (e) {
//     if (!e.document) throw new Error("jQuery requires a window with a document");
//     return t(e);
//   } : t(e);
// }("undefined" != typeof window ? window : this, function (C, e) {
//   "use strict";

//   var t = [],
//       r = Object.getPrototypeOf,
//       s = t.slice,
//       g = t.flat ? function (e) {
//     return t.flat.call(e);
//   } : function (e) {
//     return t.concat.apply([], e);
//   },
//       u = t.push,
//       i = t.indexOf,
//       n = {},
//       o = n.toString,
//       v = n.hasOwnProperty,
//       a = v.toString,
//       l = a.call(Object),
//       y = {},
//       m = function m(e) {
//     return "function" == typeof e && "number" != typeof e.nodeType;
//   },
//       x = function x(e) {
//     return null != e && e === e.window;
//   },
//       E = C.document,
//       c = {
//     type: !0,
//     src: !0,
//     nonce: !0,
//     noModule: !0
//   };

//   function b(e, t, n) {
//     var r,
//         i,
//         o = (n = n || E).createElement("script");
//     if (o.text = e, t) for (r in c) {
//       (i = t[r] || t.getAttribute && t.getAttribute(r)) && o.setAttribute(r, i);
//     }
//     n.head.appendChild(o).parentNode.removeChild(o);
//   }

//   function w(e) {
//     return null == e ? e + "" : "object" == _typeof(e) || "function" == typeof e ? n[o.call(e)] || "object" : _typeof(e);
//   }

//   var f = "3.5.1",
//       S = function S(e, t) {
//     return new S.fn.init(e, t);
//   };

//   function p(e) {
//     var t = !!e && "length" in e && e.length,
//         n = w(e);
//     return !m(e) && !x(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e);
//   }

//   S.fn = S.prototype = {
//     jquery: f,
//     constructor: S,
//     length: 0,
//     toArray: function toArray() {
//       return s.call(this);
//     },
//     get: function get(e) {
//       return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e];
//     },
//     pushStack: function pushStack(e) {
//       var t = S.merge(this.constructor(), e);
//       return t.prevObject = this, t;
//     },
//     each: function each(e) {
//       return S.each(this, e);
//     },
//     map: function map(n) {
//       return this.pushStack(S.map(this, function (e, t) {
//         return n.call(e, t, e);
//       }));
//     },
//     slice: function slice() {
//       return this.pushStack(s.apply(this, arguments));
//     },
//     first: function first() {
//       return this.eq(0);
//     },
//     last: function last() {
//       return this.eq(-1);
//     },
//     even: function even() {
//       return this.pushStack(S.grep(this, function (e, t) {
//         return (t + 1) % 2;
//       }));
//     },
//     odd: function odd() {
//       return this.pushStack(S.grep(this, function (e, t) {
//         return t % 2;
//       }));
//     },
//     eq: function eq(e) {
//       var t = this.length,
//           n = +e + (e < 0 ? t : 0);
//       return this.pushStack(0 <= n && n < t ? [this[n]] : []);
//     },
//     end: function end() {
//       return this.prevObject || this.constructor();
//     },
//     push: u,
//     sort: t.sort,
//     splice: t.splice
//   }, S.extend = S.fn.extend = function () {
//     var e,
//         t,
//         n,
//         r,
//         i,
//         o,
//         a = arguments[0] || {},
//         s = 1,
//         u = arguments.length,
//         l = !1;

//     for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == _typeof(a) || m(a) || (a = {}), s === u && (a = this, s--); s < u; s++) {
//       if (null != (e = arguments[s])) for (t in e) {
//         r = e[t], "__proto__" !== t && a !== r && (l && r && (S.isPlainObject(r) || (i = Array.isArray(r))) ? (n = a[t], o = i && !Array.isArray(n) ? [] : i || S.isPlainObject(n) ? n : {}, i = !1, a[t] = S.extend(l, o, r)) : void 0 !== r && (a[t] = r));
//       }
//     }

//     return a;
//   }, S.extend({
//     expando: "jQuery" + (f + Math.random()).replace(/\D/g, ""),
//     isReady: !0,
//     error: function error(e) {
//       throw new Error(e);
//     },
//     noop: function noop() {},
//     isPlainObject: function isPlainObject(e) {
//       var t, n;
//       return !(!e || "[object Object]" !== o.call(e)) && (!(t = r(e)) || "function" == typeof (n = v.call(t, "constructor") && t.constructor) && a.call(n) === l);
//     },
//     isEmptyObject: function isEmptyObject(e) {
//       var t;

//       for (t in e) {
//         return !1;
//       }

//       return !0;
//     },
//     globalEval: function globalEval(e, t, n) {
//       b(e, {
//         nonce: t && t.nonce
//       }, n);
//     },
//     each: function each(e, t) {
//       var n,
//           r = 0;

//       if (p(e)) {
//         for (n = e.length; r < n; r++) {
//           if (!1 === t.call(e[r], r, e[r])) break;
//         }
//       } else for (r in e) {
//         if (!1 === t.call(e[r], r, e[r])) break;
//       }

//       return e;
//     },
//     makeArray: function makeArray(e, t) {
//       var n = t || [];
//       return null != e && (p(Object(e)) ? S.merge(n, "string" == typeof e ? [e] : e) : u.call(n, e)), n;
//     },
//     inArray: function inArray(e, t, n) {
//       return null == t ? -1 : i.call(t, e, n);
//     },
//     merge: function merge(e, t) {
//       for (var n = +t.length, r = 0, i = e.length; r < n; r++) {
//         e[i++] = t[r];
//       }

//       return e.length = i, e;
//     },
//     grep: function grep(e, t, n) {
//       for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) {
//         !t(e[i], i) !== a && r.push(e[i]);
//       }

//       return r;
//     },
//     map: function map(e, t, n) {
//       var r,
//           i,
//           o = 0,
//           a = [];
//       if (p(e)) for (r = e.length; o < r; o++) {
//         null != (i = t(e[o], o, n)) && a.push(i);
//       } else for (o in e) {
//         null != (i = t(e[o], o, n)) && a.push(i);
//       }
//       return g(a);
//     },
//     guid: 1,
//     support: y
//   }), "function" == typeof Symbol && (S.fn[Symbol.iterator] = t[Symbol.iterator]), S.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
//     n["[object " + t + "]"] = t.toLowerCase();
//   });

//   var d = function (n) {
//     var e,
//         d,
//         b,
//         o,
//         i,
//         h,
//         f,
//         g,
//         w,
//         u,
//         l,
//         T,
//         C,
//         a,
//         E,
//         v,
//         s,
//         c,
//         y,
//         S = "sizzle" + 1 * new Date(),
//         p = n.document,
//         k = 0,
//         r = 0,
//         m = ue(),
//         x = ue(),
//         A = ue(),
//         N = ue(),
//         D = function D(e, t) {
//       return e === t && (l = !0), 0;
//     },
//         j = {}.hasOwnProperty,
//         t = [],
//         q = t.pop,
//         L = t.push,
//         H = t.push,
//         O = t.slice,
//         P = function P(e, t) {
//       for (var n = 0, r = e.length; n < r; n++) {
//         if (e[n] === t) return n;
//       }

//       return -1;
//     },
//         R = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
//         M = "[\\x20\\t\\r\\n\\f]",
//         I = "(?:\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
//         W = "\\[" + M + "*(" + I + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + M + "*\\]",
//         F = ":(" + I + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
//         B = new RegExp(M + "+", "g"),
//         $ = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
//         _ = new RegExp("^" + M + "*," + M + "*"),
//         z = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
//         U = new RegExp(M + "|>"),
//         X = new RegExp(F),
//         V = new RegExp("^" + I + "$"),
//         G = {
//       ID: new RegExp("^#(" + I + ")"),
//       CLASS: new RegExp("^\\.(" + I + ")"),
//       TAG: new RegExp("^(" + I + "|[*])"),
//       ATTR: new RegExp("^" + W),
//       PSEUDO: new RegExp("^" + F),
//       CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
//       bool: new RegExp("^(?:" + R + ")$", "i"),
//       needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
//     },
//         Y = /HTML$/i,
//         Q = /^(?:input|select|textarea|button)$/i,
//         J = /^h\d$/i,
//         K = /^[^{]+\{\s*\[native \w/,
//         Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
//         ee = /[+~]/,
//         te = new RegExp("\\\\[\\da-fA-F]{1,6}" + M + "?|\\\\([^\\r\\n\\f])", "g"),
//         ne = function ne(e, t) {
//       var n = "0x" + e.slice(1) - 65536;
//       return t || (n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320));
//     },
//         re = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
//         ie = function ie(e, t) {
//       return t ? "\0" === e ? "\uFFFD" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e;
//     },
//         oe = function oe() {
//       T();
//     },
//         ae = be(function (e) {
//       return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase();
//     }, {
//       dir: "parentNode",
//       next: "legend"
//     });

//     try {
//       H.apply(t = O.call(p.childNodes), p.childNodes), t[p.childNodes.length].nodeType;
//     } catch (e) {
//       H = {
//         apply: t.length ? function (e, t) {
//           L.apply(e, O.call(t));
//         } : function (e, t) {
//           var n = e.length,
//               r = 0;

//           while (e[n++] = t[r++]) {
//             ;
//           }

//           e.length = n - 1;
//         }
//       };
//     }

//     function se(t, e, n, r) {
//       var i,
//           o,
//           a,
//           s,
//           u,
//           l,
//           c,
//           f = e && e.ownerDocument,
//           p = e ? e.nodeType : 9;
//       if (n = n || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return n;

//       if (!r && (T(e), e = e || C, E)) {
//         if (11 !== p && (u = Z.exec(t))) if (i = u[1]) {
//           if (9 === p) {
//             if (!(a = e.getElementById(i))) return n;
//             if (a.id === i) return n.push(a), n;
//           } else if (f && (a = f.getElementById(i)) && y(e, a) && a.id === i) return n.push(a), n;
//         } else {
//           if (u[2]) return H.apply(n, e.getElementsByTagName(t)), n;
//           if ((i = u[3]) && d.getElementsByClassName && e.getElementsByClassName) return H.apply(n, e.getElementsByClassName(i)), n;
//         }

//         if (d.qsa && !N[t + " "] && (!v || !v.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
//           if (c = t, f = e, 1 === p && (U.test(t) || z.test(t))) {
//             (f = ee.test(t) && ye(e.parentNode) || e) === e && d.scope || ((s = e.getAttribute("id")) ? s = s.replace(re, ie) : e.setAttribute("id", s = S)), o = (l = h(t)).length;

//             while (o--) {
//               l[o] = (s ? "#" + s : ":scope") + " " + xe(l[o]);
//             }

//             c = l.join(",");
//           }

//           try {
//             return H.apply(n, f.querySelectorAll(c)), n;
//           } catch (e) {
//             N(t, !0);
//           } finally {
//             s === S && e.removeAttribute("id");
//           }
//         }
//       }

//       return g(t.replace($, "$1"), e, n, r);
//     }

//     function ue() {
//       var r = [];
//       return function e(t, n) {
//         return r.push(t + " ") > b.cacheLength && delete e[r.shift()], e[t + " "] = n;
//       };
//     }

//     function le(e) {
//       return e[S] = !0, e;
//     }

//     function ce(e) {
//       var t = C.createElement("fieldset");

//       try {
//         return !!e(t);
//       } catch (e) {
//         return !1;
//       } finally {
//         t.parentNode && t.parentNode.removeChild(t), t = null;
//       }
//     }

//     function fe(e, t) {
//       var n = e.split("|"),
//           r = n.length;

//       while (r--) {
//         b.attrHandle[n[r]] = t;
//       }
//     }

//     function pe(e, t) {
//       var n = t && e,
//           r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
//       if (r) return r;
//       if (n) while (n = n.nextSibling) {
//         if (n === t) return -1;
//       }
//       return e ? 1 : -1;
//     }

//     function de(t) {
//       return function (e) {
//         return "input" === e.nodeName.toLowerCase() && e.type === t;
//       };
//     }

//     function he(n) {
//       return function (e) {
//         var t = e.nodeName.toLowerCase();
//         return ("input" === t || "button" === t) && e.type === n;
//       };
//     }

//     function ge(t) {
//       return function (e) {
//         return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && ae(e) === t : e.disabled === t : "label" in e && e.disabled === t;
//       };
//     }

//     function ve(a) {
//       return le(function (o) {
//         return o = +o, le(function (e, t) {
//           var n,
//               r = a([], e.length, o),
//               i = r.length;

//           while (i--) {
//             e[n = r[i]] && (e[n] = !(t[n] = e[n]));
//           }
//         });
//       });
//     }

//     function ye(e) {
//       return e && "undefined" != typeof e.getElementsByTagName && e;
//     }

//     for (e in d = se.support = {}, i = se.isXML = function (e) {
//       var t = e.namespaceURI,
//           n = (e.ownerDocument || e).documentElement;
//       return !Y.test(t || n && n.nodeName || "HTML");
//     }, T = se.setDocument = function (e) {
//       var t,
//           n,
//           r = e ? e.ownerDocument || e : p;
//       return r != C && 9 === r.nodeType && r.documentElement && (a = (C = r).documentElement, E = !i(C), p != C && (n = C.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", oe, !1) : n.attachEvent && n.attachEvent("onunload", oe)), d.scope = ce(function (e) {
//         return a.appendChild(e).appendChild(C.createElement("div")), "undefined" != typeof e.querySelectorAll && !e.querySelectorAll(":scope fieldset div").length;
//       }), d.attributes = ce(function (e) {
//         return e.className = "i", !e.getAttribute("className");
//       }), d.getElementsByTagName = ce(function (e) {
//         return e.appendChild(C.createComment("")), !e.getElementsByTagName("*").length;
//       }), d.getElementsByClassName = K.test(C.getElementsByClassName), d.getById = ce(function (e) {
//         return a.appendChild(e).id = S, !C.getElementsByName || !C.getElementsByName(S).length;
//       }), d.getById ? (b.filter.ID = function (e) {
//         var t = e.replace(te, ne);
//         return function (e) {
//           return e.getAttribute("id") === t;
//         };
//       }, b.find.ID = function (e, t) {
//         if ("undefined" != typeof t.getElementById && E) {
//           var n = t.getElementById(e);
//           return n ? [n] : [];
//         }
//       }) : (b.filter.ID = function (e) {
//         var n = e.replace(te, ne);
//         return function (e) {
//           var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
//           return t && t.value === n;
//         };
//       }, b.find.ID = function (e, t) {
//         if ("undefined" != typeof t.getElementById && E) {
//           var n,
//               r,
//               i,
//               o = t.getElementById(e);

//           if (o) {
//             if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
//             i = t.getElementsByName(e), r = 0;

//             while (o = i[r++]) {
//               if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
//             }
//           }

//           return [];
//         }
//       }), b.find.TAG = d.getElementsByTagName ? function (e, t) {
//         return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : d.qsa ? t.querySelectorAll(e) : void 0;
//       } : function (e, t) {
//         var n,
//             r = [],
//             i = 0,
//             o = t.getElementsByTagName(e);

//         if ("*" === e) {
//           while (n = o[i++]) {
//             1 === n.nodeType && r.push(n);
//           }

//           return r;
//         }

//         return o;
//       }, b.find.CLASS = d.getElementsByClassName && function (e, t) {
//         if ("undefined" != typeof t.getElementsByClassName && E) return t.getElementsByClassName(e);
//       }, s = [], v = [], (d.qsa = K.test(C.querySelectorAll)) && (ce(function (e) {
//         var t;
//         a.appendChild(e).innerHTML = "<a id='" + S + "'></a><select id='" + S + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + M + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + M + "*(?:value|" + R + ")"), e.querySelectorAll("[id~=" + S + "-]").length || v.push("~="), (t = C.createElement("input")).setAttribute("name", ""), e.appendChild(t), e.querySelectorAll("[name='']").length || v.push("\\[" + M + "*name" + M + "*=" + M + "*(?:''|\"\")"), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + S + "+*").length || v.push(".#.+[+~]"), e.querySelectorAll("\\\f"), v.push("[\\r\\n\\f]");
//       }), ce(function (e) {
//         e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
//         var t = C.createElement("input");
//         t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + M + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:");
//       })), (d.matchesSelector = K.test(c = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && ce(function (e) {
//         d.disconnectedMatch = c.call(e, "*"), c.call(e, "[s!='']:x"), s.push("!=", F);
//       }), v = v.length && new RegExp(v.join("|")), s = s.length && new RegExp(s.join("|")), t = K.test(a.compareDocumentPosition), y = t || K.test(a.contains) ? function (e, t) {
//         var n = 9 === e.nodeType ? e.documentElement : e,
//             r = t && t.parentNode;
//         return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)));
//       } : function (e, t) {
//         if (t) while (t = t.parentNode) {
//           if (t === e) return !0;
//         }
//         return !1;
//       }, D = t ? function (e, t) {
//         if (e === t) return l = !0, 0;
//         var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
//         return n || (1 & (n = (e.ownerDocument || e) == (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !d.sortDetached && t.compareDocumentPosition(e) === n ? e == C || e.ownerDocument == p && y(p, e) ? -1 : t == C || t.ownerDocument == p && y(p, t) ? 1 : u ? P(u, e) - P(u, t) : 0 : 4 & n ? -1 : 1);
//       } : function (e, t) {
//         if (e === t) return l = !0, 0;
//         var n,
//             r = 0,
//             i = e.parentNode,
//             o = t.parentNode,
//             a = [e],
//             s = [t];
//         if (!i || !o) return e == C ? -1 : t == C ? 1 : i ? -1 : o ? 1 : u ? P(u, e) - P(u, t) : 0;
//         if (i === o) return pe(e, t);
//         n = e;

//         while (n = n.parentNode) {
//           a.unshift(n);
//         }

//         n = t;

//         while (n = n.parentNode) {
//           s.unshift(n);
//         }

//         while (a[r] === s[r]) {
//           r++;
//         }

//         return r ? pe(a[r], s[r]) : a[r] == p ? -1 : s[r] == p ? 1 : 0;
//       }), C;
//     }, se.matches = function (e, t) {
//       return se(e, null, null, t);
//     }, se.matchesSelector = function (e, t) {
//       if (T(e), d.matchesSelector && E && !N[t + " "] && (!s || !s.test(t)) && (!v || !v.test(t))) try {
//         var n = c.call(e, t);
//         if (n || d.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n;
//       } catch (e) {
//         N(t, !0);
//       }
//       return 0 < se(t, C, null, [e]).length;
//     }, se.contains = function (e, t) {
//       return (e.ownerDocument || e) != C && T(e), y(e, t);
//     }, se.attr = function (e, t) {
//       (e.ownerDocument || e) != C && T(e);
//       var n = b.attrHandle[t.toLowerCase()],
//           r = n && j.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !E) : void 0;
//       return void 0 !== r ? r : d.attributes || !E ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null;
//     }, se.escape = function (e) {
//       return (e + "").replace(re, ie);
//     }, se.error = function (e) {
//       throw new Error("Syntax error, unrecognized expression: " + e);
//     }, se.uniqueSort = function (e) {
//       var t,
//           n = [],
//           r = 0,
//           i = 0;

//       if (l = !d.detectDuplicates, u = !d.sortStable && e.slice(0), e.sort(D), l) {
//         while (t = e[i++]) {
//           t === e[i] && (r = n.push(i));
//         }

//         while (r--) {
//           e.splice(n[r], 1);
//         }
//       }

//       return u = null, e;
//     }, o = se.getText = function (e) {
//       var t,
//           n = "",
//           r = 0,
//           i = e.nodeType;

//       if (i) {
//         if (1 === i || 9 === i || 11 === i) {
//           if ("string" == typeof e.textContent) return e.textContent;

//           for (e = e.firstChild; e; e = e.nextSibling) {
//             n += o(e);
//           }
//         } else if (3 === i || 4 === i) return e.nodeValue;
//       } else while (t = e[r++]) {
//         n += o(t);
//       }

//       return n;
//     }, (b = se.selectors = {
//       cacheLength: 50,
//       createPseudo: le,
//       match: G,
//       attrHandle: {},
//       find: {},
//       relative: {
//         ">": {
//           dir: "parentNode",
//           first: !0
//         },
//         " ": {
//           dir: "parentNode"
//         },
//         "+": {
//           dir: "previousSibling",
//           first: !0
//         },
//         "~": {
//           dir: "previousSibling"
//         }
//       },
//       preFilter: {
//         ATTR: function ATTR(e) {
//           return e[1] = e[1].replace(te, ne), e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4);
//         },
//         CHILD: function CHILD(e) {
//           return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && se.error(e[0]), e;
//         },
//         PSEUDO: function PSEUDO(e) {
//           var t,
//               n = !e[6] && e[2];
//           return G.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && X.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3));
//         }
//       },
//       filter: {
//         TAG: function TAG(e) {
//           var t = e.replace(te, ne).toLowerCase();
//           return "*" === e ? function () {
//             return !0;
//           } : function (e) {
//             return e.nodeName && e.nodeName.toLowerCase() === t;
//           };
//         },
//         CLASS: function CLASS(e) {
//           var t = m[e + " "];
//           return t || (t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) && m(e, function (e) {
//             return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "");
//           });
//         },
//         ATTR: function ATTR(n, r, i) {
//           return function (e) {
//             var t = se.attr(e, n);
//             return null == t ? "!=" === r : !r || (t += "", "=" === r ? t === i : "!=" === r ? t !== i : "^=" === r ? i && 0 === t.indexOf(i) : "*=" === r ? i && -1 < t.indexOf(i) : "$=" === r ? i && t.slice(-i.length) === i : "~=" === r ? -1 < (" " + t.replace(B, " ") + " ").indexOf(i) : "|=" === r && (t === i || t.slice(0, i.length + 1) === i + "-"));
//           };
//         },
//         CHILD: function CHILD(h, e, t, g, v) {
//           var y = "nth" !== h.slice(0, 3),
//               m = "last" !== h.slice(-4),
//               x = "of-type" === e;
//           return 1 === g && 0 === v ? function (e) {
//             return !!e.parentNode;
//           } : function (e, t, n) {
//             var r,
//                 i,
//                 o,
//                 a,
//                 s,
//                 u,
//                 l = y !== m ? "nextSibling" : "previousSibling",
//                 c = e.parentNode,
//                 f = x && e.nodeName.toLowerCase(),
//                 p = !n && !x,
//                 d = !1;

//             if (c) {
//               if (y) {
//                 while (l) {
//                   a = e;

//                   while (a = a[l]) {
//                     if (x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) return !1;
//                   }

//                   u = l = "only" === h && !u && "nextSibling";
//                 }

//                 return !0;
//               }

//               if (u = [m ? c.firstChild : c.lastChild], m && p) {
//                 d = (s = (r = (i = (o = (a = c)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === k && r[1]) && r[2], a = s && c.childNodes[s];

//                 while (a = ++s && a && a[l] || (d = s = 0) || u.pop()) {
//                   if (1 === a.nodeType && ++d && a === e) {
//                     i[h] = [k, s, d];
//                     break;
//                   }
//                 }
//               } else if (p && (d = s = (r = (i = (o = (a = e)[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === k && r[1]), !1 === d) while (a = ++s && a && a[l] || (d = s = 0) || u.pop()) {
//                 if ((x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) && ++d && (p && ((i = (o = a[S] || (a[S] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] = [k, d]), a === e)) break;
//               }

//               return (d -= v) === g || d % g == 0 && 0 <= d / g;
//             }
//           };
//         },
//         PSEUDO: function PSEUDO(e, o) {
//           var t,
//               a = b.pseudos[e] || b.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
//           return a[S] ? a(o) : 1 < a.length ? (t = [e, e, "", o], b.setFilters.hasOwnProperty(e.toLowerCase()) ? le(function (e, t) {
//             var n,
//                 r = a(e, o),
//                 i = r.length;

//             while (i--) {
//               e[n = P(e, r[i])] = !(t[n] = r[i]);
//             }
//           }) : function (e) {
//             return a(e, 0, t);
//           }) : a;
//         }
//       },
//       pseudos: {
//         not: le(function (e) {
//           var r = [],
//               i = [],
//               s = f(e.replace($, "$1"));
//           return s[S] ? le(function (e, t, n, r) {
//             var i,
//                 o = s(e, null, r, []),
//                 a = e.length;

//             while (a--) {
//               (i = o[a]) && (e[a] = !(t[a] = i));
//             }
//           }) : function (e, t, n) {
//             return r[0] = e, s(r, null, n, i), r[0] = null, !i.pop();
//           };
//         }),
//         has: le(function (t) {
//           return function (e) {
//             return 0 < se(t, e).length;
//           };
//         }),
//         contains: le(function (t) {
//           return t = t.replace(te, ne), function (e) {
//             return -1 < (e.textContent || o(e)).indexOf(t);
//           };
//         }),
//         lang: le(function (n) {
//           return V.test(n || "") || se.error("unsupported lang: " + n), n = n.replace(te, ne).toLowerCase(), function (e) {
//             var t;

//             do {
//               if (t = E ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-");
//             } while ((e = e.parentNode) && 1 === e.nodeType);

//             return !1;
//           };
//         }),
//         target: function target(e) {
//           var t = n.location && n.location.hash;
//           return t && t.slice(1) === e.id;
//         },
//         root: function root(e) {
//           return e === a;
//         },
//         focus: function focus(e) {
//           return e === C.activeElement && (!C.hasFocus || C.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
//         },
//         enabled: ge(!1),
//         disabled: ge(!0),
//         checked: function checked(e) {
//           var t = e.nodeName.toLowerCase();
//           return "input" === t && !!e.checked || "option" === t && !!e.selected;
//         },
//         selected: function selected(e) {
//           return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected;
//         },
//         empty: function empty(e) {
//           for (e = e.firstChild; e; e = e.nextSibling) {
//             if (e.nodeType < 6) return !1;
//           }

//           return !0;
//         },
//         parent: function parent(e) {
//           return !b.pseudos.empty(e);
//         },
//         header: function header(e) {
//           return J.test(e.nodeName);
//         },
//         input: function input(e) {
//           return Q.test(e.nodeName);
//         },
//         button: function button(e) {
//           var t = e.nodeName.toLowerCase();
//           return "input" === t && "button" === e.type || "button" === t;
//         },
//         text: function text(e) {
//           var t;
//           return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase());
//         },
//         first: ve(function () {
//           return [0];
//         }),
//         last: ve(function (e, t) {
//           return [t - 1];
//         }),
//         eq: ve(function (e, t, n) {
//           return [n < 0 ? n + t : n];
//         }),
//         even: ve(function (e, t) {
//           for (var n = 0; n < t; n += 2) {
//             e.push(n);
//           }

//           return e;
//         }),
//         odd: ve(function (e, t) {
//           for (var n = 1; n < t; n += 2) {
//             e.push(n);
//           }

//           return e;
//         }),
//         lt: ve(function (e, t, n) {
//           for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r;) {
//             e.push(r);
//           }

//           return e;
//         }),
//         gt: ve(function (e, t, n) {
//           for (var r = n < 0 ? n + t : n; ++r < t;) {
//             e.push(r);
//           }

//           return e;
//         })
//       }
//     }).pseudos.nth = b.pseudos.eq, {
//       radio: !0,
//       checkbox: !0,
//       file: !0,
//       password: !0,
//       image: !0
//     }) {
//       b.pseudos[e] = de(e);
//     }

//     for (e in {
//       submit: !0,
//       reset: !0
//     }) {
//       b.pseudos[e] = he(e);
//     }

//     function me() {}

//     function xe(e) {
//       for (var t = 0, n = e.length, r = ""; t < n; t++) {
//         r += e[t].value;
//       }

//       return r;
//     }

//     function be(s, e, t) {
//       var u = e.dir,
//           l = e.next,
//           c = l || u,
//           f = t && "parentNode" === c,
//           p = r++;
//       return e.first ? function (e, t, n) {
//         while (e = e[u]) {
//           if (1 === e.nodeType || f) return s(e, t, n);
//         }

//         return !1;
//       } : function (e, t, n) {
//         var r,
//             i,
//             o,
//             a = [k, p];

//         if (n) {
//           while (e = e[u]) {
//             if ((1 === e.nodeType || f) && s(e, t, n)) return !0;
//           }
//         } else while (e = e[u]) {
//           if (1 === e.nodeType || f) if (i = (o = e[S] || (e[S] = {}))[e.uniqueID] || (o[e.uniqueID] = {}), l && l === e.nodeName.toLowerCase()) e = e[u] || e;else {
//             if ((r = i[c]) && r[0] === k && r[1] === p) return a[2] = r[2];
//             if ((i[c] = a)[2] = s(e, t, n)) return !0;
//           }
//         }

//         return !1;
//       };
//     }

//     function we(i) {
//       return 1 < i.length ? function (e, t, n) {
//         var r = i.length;

//         while (r--) {
//           if (!i[r](e, t, n)) return !1;
//         }

//         return !0;
//       } : i[0];
//     }

//     function Te(e, t, n, r, i) {
//       for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++) {
//         (o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
//       }

//       return a;
//     }

//     function Ce(d, h, g, v, y, e) {
//       return v && !v[S] && (v = Ce(v)), y && !y[S] && (y = Ce(y, e)), le(function (e, t, n, r) {
//         var i,
//             o,
//             a,
//             s = [],
//             u = [],
//             l = t.length,
//             c = e || function (e, t, n) {
//           for (var r = 0, i = t.length; r < i; r++) {
//             se(e, t[r], n);
//           }

//           return n;
//         }(h || "*", n.nodeType ? [n] : n, []),
//             f = !d || !e && h ? c : Te(c, s, d, n, r),
//             p = g ? y || (e ? d : l || v) ? [] : t : f;

//         if (g && g(f, p, n, r), v) {
//           i = Te(p, u), v(i, [], n, r), o = i.length;

//           while (o--) {
//             (a = i[o]) && (p[u[o]] = !(f[u[o]] = a));
//           }
//         }

//         if (e) {
//           if (y || d) {
//             if (y) {
//               i = [], o = p.length;

//               while (o--) {
//                 (a = p[o]) && i.push(f[o] = a);
//               }

//               y(null, p = [], i, r);
//             }

//             o = p.length;

//             while (o--) {
//               (a = p[o]) && -1 < (i = y ? P(e, a) : s[o]) && (e[i] = !(t[i] = a));
//             }
//           }
//         } else p = Te(p === t ? p.splice(l, p.length) : p), y ? y(null, t, p, r) : H.apply(t, p);
//       });
//     }

//     function Ee(e) {
//       for (var i, t, n, r = e.length, o = b.relative[e[0].type], a = o || b.relative[" "], s = o ? 1 : 0, u = be(function (e) {
//         return e === i;
//       }, a, !0), l = be(function (e) {
//         return -1 < P(i, e);
//       }, a, !0), c = [function (e, t, n) {
//         var r = !o && (n || t !== w) || ((i = t).nodeType ? u(e, t, n) : l(e, t, n));
//         return i = null, r;
//       }]; s < r; s++) {
//         if (t = b.relative[e[s].type]) c = [be(we(c), t)];else {
//           if ((t = b.filter[e[s].type].apply(null, e[s].matches))[S]) {
//             for (n = ++s; n < r; n++) {
//               if (b.relative[e[n].type]) break;
//             }

//             return Ce(1 < s && we(c), 1 < s && xe(e.slice(0, s - 1).concat({
//               value: " " === e[s - 2].type ? "*" : ""
//             })).replace($, "$1"), t, s < n && Ee(e.slice(s, n)), n < r && Ee(e = e.slice(n)), n < r && xe(e));
//           }

//           c.push(t);
//         }
//       }

//       return we(c);
//     }

//     return me.prototype = b.filters = b.pseudos, b.setFilters = new me(), h = se.tokenize = function (e, t) {
//       var n,
//           r,
//           i,
//           o,
//           a,
//           s,
//           u,
//           l = x[e + " "];
//       if (l) return t ? 0 : l.slice(0);
//       a = e, s = [], u = b.preFilter;

//       while (a) {
//         for (o in n && !(r = _.exec(a)) || (r && (a = a.slice(r[0].length) || a), s.push(i = [])), n = !1, (r = z.exec(a)) && (n = r.shift(), i.push({
//           value: n,
//           type: r[0].replace($, " ")
//         }), a = a.slice(n.length)), b.filter) {
//           !(r = G[o].exec(a)) || u[o] && !(r = u[o](r)) || (n = r.shift(), i.push({
//             value: n,
//             type: o,
//             matches: r
//           }), a = a.slice(n.length));
//         }

//         if (!n) break;
//       }

//       return t ? a.length : a ? se.error(e) : x(e, s).slice(0);
//     }, f = se.compile = function (e, t) {
//       var n,
//           v,
//           y,
//           m,
//           x,
//           r,
//           i = [],
//           o = [],
//           a = A[e + " "];

//       if (!a) {
//         t || (t = h(e)), n = t.length;

//         while (n--) {
//           (a = Ee(t[n]))[S] ? i.push(a) : o.push(a);
//         }

//         (a = A(e, (v = o, m = 0 < (y = i).length, x = 0 < v.length, r = function r(e, t, n, _r, i) {
//           var o,
//               a,
//               s,
//               u = 0,
//               l = "0",
//               c = e && [],
//               f = [],
//               p = w,
//               d = e || x && b.find.TAG("*", i),
//               h = k += null == p ? 1 : Math.random() || .1,
//               g = d.length;

//           for (i && (w = t == C || t || i); l !== g && null != (o = d[l]); l++) {
//             if (x && o) {
//               a = 0, t || o.ownerDocument == C || (T(o), n = !E);

//               while (s = v[a++]) {
//                 if (s(o, t || C, n)) {
//                   _r.push(o);

//                   break;
//                 }
//               }

//               i && (k = h);
//             }

//             m && ((o = !s && o) && u--, e && c.push(o));
//           }

//           if (u += l, m && l !== u) {
//             a = 0;

//             while (s = y[a++]) {
//               s(c, f, t, n);
//             }

//             if (e) {
//               if (0 < u) while (l--) {
//                 c[l] || f[l] || (f[l] = q.call(_r));
//               }
//               f = Te(f);
//             }

//             H.apply(_r, f), i && !e && 0 < f.length && 1 < u + y.length && se.uniqueSort(_r);
//           }

//           return i && (k = h, w = p), c;
//         }, m ? le(r) : r))).selector = e;
//       }

//       return a;
//     }, g = se.select = function (e, t, n, r) {
//       var i,
//           o,
//           a,
//           s,
//           u,
//           l = "function" == typeof e && e,
//           c = !r && h(e = l.selector || e);

//       if (n = n || [], 1 === c.length) {
//         if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (a = o[0]).type && 9 === t.nodeType && E && b.relative[o[1].type]) {
//           if (!(t = (b.find.ID(a.matches[0].replace(te, ne), t) || [])[0])) return n;
//           l && (t = t.parentNode), e = e.slice(o.shift().value.length);
//         }

//         i = G.needsContext.test(e) ? 0 : o.length;

//         while (i--) {
//           if (a = o[i], b.relative[s = a.type]) break;

//           if ((u = b.find[s]) && (r = u(a.matches[0].replace(te, ne), ee.test(o[0].type) && ye(t.parentNode) || t))) {
//             if (o.splice(i, 1), !(e = r.length && xe(o))) return H.apply(n, r), n;
//             break;
//           }
//         }
//       }

//       return (l || f(e, c))(r, t, !E, n, !t || ee.test(e) && ye(t.parentNode) || t), n;
//     }, d.sortStable = S.split("").sort(D).join("") === S, d.detectDuplicates = !!l, T(), d.sortDetached = ce(function (e) {
//       return 1 & e.compareDocumentPosition(C.createElement("fieldset"));
//     }), ce(function (e) {
//       return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href");
//     }) || fe("type|href|height|width", function (e, t, n) {
//       if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
//     }), d.attributes && ce(function (e) {
//       return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value");
//     }) || fe("value", function (e, t, n) {
//       if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue;
//     }), ce(function (e) {
//       return null == e.getAttribute("disabled");
//     }) || fe(R, function (e, t, n) {
//       var r;
//       if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null;
//     }), se;
//   }(C);

//   S.find = d, S.expr = d.selectors, S.expr[":"] = S.expr.pseudos, S.uniqueSort = S.unique = d.uniqueSort, S.text = d.getText, S.isXMLDoc = d.isXML, S.contains = d.contains, S.escapeSelector = d.escape;

//   var h = function h(e, t, n) {
//     var r = [],
//         i = void 0 !== n;

//     while ((e = e[t]) && 9 !== e.nodeType) {
//       if (1 === e.nodeType) {
//         if (i && S(e).is(n)) break;
//         r.push(e);
//       }
//     }

//     return r;
//   },
//       T = function T(e, t) {
//     for (var n = []; e; e = e.nextSibling) {
//       1 === e.nodeType && e !== t && n.push(e);
//     }

//     return n;
//   },
//       k = S.expr.match.needsContext;

//   function A(e, t) {
//     return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
//   }

//   var N = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

//   function D(e, n, r) {
//     return m(n) ? S.grep(e, function (e, t) {
//       return !!n.call(e, t, e) !== r;
//     }) : n.nodeType ? S.grep(e, function (e) {
//       return e === n !== r;
//     }) : "string" != typeof n ? S.grep(e, function (e) {
//       return -1 < i.call(n, e) !== r;
//     }) : S.filter(n, e, r);
//   }

//   S.filter = function (e, t, n) {
//     var r = t[0];
//     return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? S.find.matchesSelector(r, e) ? [r] : [] : S.find.matches(e, S.grep(t, function (e) {
//       return 1 === e.nodeType;
//     }));
//   }, S.fn.extend({
//     find: function find(e) {
//       var t,
//           n,
//           r = this.length,
//           i = this;
//       if ("string" != typeof e) return this.pushStack(S(e).filter(function () {
//         for (t = 0; t < r; t++) {
//           if (S.contains(i[t], this)) return !0;
//         }
//       }));

//       for (n = this.pushStack([]), t = 0; t < r; t++) {
//         S.find(e, i[t], n);
//       }

//       return 1 < r ? S.uniqueSort(n) : n;
//     },
//     filter: function filter(e) {
//       return this.pushStack(D(this, e || [], !1));
//     },
//     not: function not(e) {
//       return this.pushStack(D(this, e || [], !0));
//     },
//     is: function is(e) {
//       return !!D(this, "string" == typeof e && k.test(e) ? S(e) : e || [], !1).length;
//     }
//   });
//   var j,
//       q = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
//   (S.fn.init = function (e, t, n) {
//     var r, i;
//     if (!e) return this;

//     if (n = n || j, "string" == typeof e) {
//       if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : q.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);

//       if (r[1]) {
//         if (t = t instanceof S ? t[0] : t, S.merge(this, S.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : E, !0)), N.test(r[1]) && S.isPlainObject(t)) for (r in t) {
//           m(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
//         }
//         return this;
//       }

//       return (i = E.getElementById(r[2])) && (this[0] = i, this.length = 1), this;
//     }

//     return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== n.ready ? n.ready(e) : e(S) : S.makeArray(e, this);
//   }).prototype = S.fn, j = S(E);
//   var L = /^(?:parents|prev(?:Until|All))/,
//       H = {
//     children: !0,
//     contents: !0,
//     next: !0,
//     prev: !0
//   };

//   function O(e, t) {
//     while ((e = e[t]) && 1 !== e.nodeType) {
//       ;
//     }

//     return e;
//   }

//   S.fn.extend({
//     has: function has(e) {
//       var t = S(e, this),
//           n = t.length;
//       return this.filter(function () {
//         for (var e = 0; e < n; e++) {
//           if (S.contains(this, t[e])) return !0;
//         }
//       });
//     },
//     closest: function closest(e, t) {
//       var n,
//           r = 0,
//           i = this.length,
//           o = [],
//           a = "string" != typeof e && S(e);
//       if (!k.test(e)) for (; r < i; r++) {
//         for (n = this[r]; n && n !== t; n = n.parentNode) {
//           if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && S.find.matchesSelector(n, e))) {
//             o.push(n);
//             break;
//           }
//         }
//       }
//       return this.pushStack(1 < o.length ? S.uniqueSort(o) : o);
//     },
//     index: function index(e) {
//       return e ? "string" == typeof e ? i.call(S(e), this[0]) : i.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
//     },
//     add: function add(e, t) {
//       return this.pushStack(S.uniqueSort(S.merge(this.get(), S(e, t))));
//     },
//     addBack: function addBack(e) {
//       return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
//     }
//   }), S.each({
//     parent: function parent(e) {
//       var t = e.parentNode;
//       return t && 11 !== t.nodeType ? t : null;
//     },
//     parents: function parents(e) {
//       return h(e, "parentNode");
//     },
//     parentsUntil: function parentsUntil(e, t, n) {
//       return h(e, "parentNode", n);
//     },
//     next: function next(e) {
//       return O(e, "nextSibling");
//     },
//     prev: function prev(e) {
//       return O(e, "previousSibling");
//     },
//     nextAll: function nextAll(e) {
//       return h(e, "nextSibling");
//     },
//     prevAll: function prevAll(e) {
//       return h(e, "previousSibling");
//     },
//     nextUntil: function nextUntil(e, t, n) {
//       return h(e, "nextSibling", n);
//     },
//     prevUntil: function prevUntil(e, t, n) {
//       return h(e, "previousSibling", n);
//     },
//     siblings: function siblings(e) {
//       return T((e.parentNode || {}).firstChild, e);
//     },
//     children: function children(e) {
//       return T(e.firstChild);
//     },
//     contents: function contents(e) {
//       return null != e.contentDocument && r(e.contentDocument) ? e.contentDocument : (A(e, "template") && (e = e.content || e), S.merge([], e.childNodes));
//     }
//   }, function (r, i) {
//     S.fn[r] = function (e, t) {
//       var n = S.map(this, i, e);
//       return "Until" !== r.slice(-5) && (t = e), t && "string" == typeof t && (n = S.filter(t, n)), 1 < this.length && (H[r] || S.uniqueSort(n), L.test(r) && n.reverse()), this.pushStack(n);
//     };
//   });
//   var P = /[^\x20\t\r\n\f]+/g;

//   function R(e) {
//     return e;
//   }

//   function M(e) {
//     throw e;
//   }

//   function I(e, t, n, r) {
//     var i;

//     try {
//       e && m(i = e.promise) ? i.call(e).done(t).fail(n) : e && m(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r));
//     } catch (e) {
//       n.apply(void 0, [e]);
//     }
//   }

//   S.Callbacks = function (r) {
//     var e, n;
//     r = "string" == typeof r ? (e = r, n = {}, S.each(e.match(P) || [], function (e, t) {
//       n[t] = !0;
//     }), n) : S.extend({}, r);

//     var i,
//         t,
//         o,
//         a,
//         s = [],
//         u = [],
//         l = -1,
//         c = function c() {
//       for (a = a || r.once, o = i = !0; u.length; l = -1) {
//         t = u.shift();

//         while (++l < s.length) {
//           !1 === s[l].apply(t[0], t[1]) && r.stopOnFalse && (l = s.length, t = !1);
//         }
//       }

//       r.memory || (t = !1), i = !1, a && (s = t ? [] : "");
//     },
//         f = {
//       add: function add() {
//         return s && (t && !i && (l = s.length - 1, u.push(t)), function n(e) {
//           S.each(e, function (e, t) {
//             m(t) ? r.unique && f.has(t) || s.push(t) : t && t.length && "string" !== w(t) && n(t);
//           });
//         }(arguments), t && !i && c()), this;
//       },
//       remove: function remove() {
//         return S.each(arguments, function (e, t) {
//           var n;

//           while (-1 < (n = S.inArray(t, s, n))) {
//             s.splice(n, 1), n <= l && l--;
//           }
//         }), this;
//       },
//       has: function has(e) {
//         return e ? -1 < S.inArray(e, s) : 0 < s.length;
//       },
//       empty: function empty() {
//         return s && (s = []), this;
//       },
//       disable: function disable() {
//         return a = u = [], s = t = "", this;
//       },
//       disabled: function disabled() {
//         return !s;
//       },
//       lock: function lock() {
//         return a = u = [], t || i || (s = t = ""), this;
//       },
//       locked: function locked() {
//         return !!a;
//       },
//       fireWith: function fireWith(e, t) {
//         return a || (t = [e, (t = t || []).slice ? t.slice() : t], u.push(t), i || c()), this;
//       },
//       fire: function fire() {
//         return f.fireWith(this, arguments), this;
//       },
//       fired: function fired() {
//         return !!o;
//       }
//     };

//     return f;
//   }, S.extend({
//     Deferred: function Deferred(e) {
//       var o = [["notify", "progress", S.Callbacks("memory"), S.Callbacks("memory"), 2], ["resolve", "done", S.Callbacks("once memory"), S.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", S.Callbacks("once memory"), S.Callbacks("once memory"), 1, "rejected"]],
//           i = "pending",
//           a = {
//         state: function state() {
//           return i;
//         },
//         always: function always() {
//           return s.done(arguments).fail(arguments), this;
//         },
//         "catch": function _catch(e) {
//           return a.then(null, e);
//         },
//         pipe: function pipe() {
//           var i = arguments;
//           return S.Deferred(function (r) {
//             S.each(o, function (e, t) {
//               var n = m(i[t[4]]) && i[t[4]];
//               s[t[1]](function () {
//                 var e = n && n.apply(this, arguments);
//                 e && m(e.promise) ? e.promise().progress(r.notify).done(r.resolve).fail(r.reject) : r[t[0] + "With"](this, n ? [e] : arguments);
//               });
//             }), i = null;
//           }).promise();
//         },
//         then: function then(t, n, r) {
//           var u = 0;

//           function l(i, o, a, s) {
//             return function () {
//               var n = this,
//                   r = arguments,
//                   e = function e() {
//                 var e, t;

//                 if (!(i < u)) {
//                   if ((e = a.apply(n, r)) === o.promise()) throw new TypeError("Thenable self-resolution");
//                   t = e && ("object" == _typeof(e) || "function" == typeof e) && e.then, m(t) ? s ? t.call(e, l(u, o, R, s), l(u, o, M, s)) : (u++, t.call(e, l(u, o, R, s), l(u, o, M, s), l(u, o, R, o.notifyWith))) : (a !== R && (n = void 0, r = [e]), (s || o.resolveWith)(n, r));
//                 }
//               },
//                   t = s ? e : function () {
//                 try {
//                   e();
//                 } catch (e) {
//                   S.Deferred.exceptionHook && S.Deferred.exceptionHook(e, t.stackTrace), u <= i + 1 && (a !== M && (n = void 0, r = [e]), o.rejectWith(n, r));
//                 }
//               };

//               i ? t() : (S.Deferred.getStackHook && (t.stackTrace = S.Deferred.getStackHook()), C.setTimeout(t));
//             };
//           }

//           return S.Deferred(function (e) {
//             o[0][3].add(l(0, e, m(r) ? r : R, e.notifyWith)), o[1][3].add(l(0, e, m(t) ? t : R)), o[2][3].add(l(0, e, m(n) ? n : M));
//           }).promise();
//         },
//         promise: function promise(e) {
//           return null != e ? S.extend(e, a) : a;
//         }
//       },
//           s = {};
//       return S.each(o, function (e, t) {
//         var n = t[2],
//             r = t[5];
//         a[t[1]] = n.add, r && n.add(function () {
//           i = r;
//         }, o[3 - e][2].disable, o[3 - e][3].disable, o[0][2].lock, o[0][3].lock), n.add(t[3].fire), s[t[0]] = function () {
//           return s[t[0] + "With"](this === s ? void 0 : this, arguments), this;
//         }, s[t[0] + "With"] = n.fireWith;
//       }), a.promise(s), e && e.call(s, s), s;
//     },
//     when: function when(e) {
//       var n = arguments.length,
//           t = n,
//           r = Array(t),
//           i = s.call(arguments),
//           o = S.Deferred(),
//           a = function a(t) {
//         return function (e) {
//           r[t] = this, i[t] = 1 < arguments.length ? s.call(arguments) : e, --n || o.resolveWith(r, i);
//         };
//       };

//       if (n <= 1 && (I(e, o.done(a(t)).resolve, o.reject, !n), "pending" === o.state() || m(i[t] && i[t].then))) return o.then();

//       while (t--) {
//         I(i[t], a(t), o.reject);
//       }

//       return o.promise();
//     }
//   });
//   var W = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
//   S.Deferred.exceptionHook = function (e, t) {
//     C.console && C.console.warn && e && W.test(e.name) && C.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t);
//   }, S.readyException = function (e) {
//     C.setTimeout(function () {
//       throw e;
//     });
//   };
//   var F = S.Deferred();

//   function B() {
//     E.removeEventListener("DOMContentLoaded", B), C.removeEventListener("load", B), S.ready();
//   }

//   S.fn.ready = function (e) {
//     return F.then(e)["catch"](function (e) {
//       S.readyException(e);
//     }), this;
//   }, S.extend({
//     isReady: !1,
//     readyWait: 1,
//     ready: function ready(e) {
//       (!0 === e ? --S.readyWait : S.isReady) || (S.isReady = !0) !== e && 0 < --S.readyWait || F.resolveWith(E, [S]);
//     }
//   }), S.ready.then = F.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? C.setTimeout(S.ready) : (E.addEventListener("DOMContentLoaded", B), C.addEventListener("load", B));

//   var $ = function $(e, t, n, r, i, o, a) {
//     var s = 0,
//         u = e.length,
//         l = null == n;
//     if ("object" === w(n)) for (s in i = !0, n) {
//       $(e, t, s, n[s], !0, o, a);
//     } else if (void 0 !== r && (i = !0, m(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function t(e, _t2, n) {
//       return l.call(S(e), n);
//     })), t)) for (; s < u; s++) {
//       t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
//     }
//     return i ? e : l ? t.call(e) : u ? t(e[0], n) : o;
//   },
//       _ = /^-ms-/,
//       z = /-([a-z])/g;

//   function U(e, t) {
//     return t.toUpperCase();
//   }

//   function X(e) {
//     return e.replace(_, "ms-").replace(z, U);
//   }

//   var V = function V(e) {
//     return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
//   };

//   function G() {
//     this.expando = S.expando + G.uid++;
//   }

//   G.uid = 1, G.prototype = {
//     cache: function cache(e) {
//       var t = e[this.expando];
//       return t || (t = {}, V(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
//         value: t,
//         configurable: !0
//       }))), t;
//     },
//     set: function set(e, t, n) {
//       var r,
//           i = this.cache(e);
//       if ("string" == typeof t) i[X(t)] = n;else for (r in t) {
//         i[X(r)] = t[r];
//       }
//       return i;
//     },
//     get: function get(e, t) {
//       return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][X(t)];
//     },
//     access: function access(e, t, n) {
//       return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t);
//     },
//     remove: function remove(e, t) {
//       var n,
//           r = e[this.expando];

//       if (void 0 !== r) {
//         if (void 0 !== t) {
//           n = (t = Array.isArray(t) ? t.map(X) : (t = X(t)) in r ? [t] : t.match(P) || []).length;

//           while (n--) {
//             delete r[t[n]];
//           }
//         }

//         (void 0 === t || S.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando]);
//       }
//     },
//     hasData: function hasData(e) {
//       var t = e[this.expando];
//       return void 0 !== t && !S.isEmptyObject(t);
//     }
//   };
//   var Y = new G(),
//       Q = new G(),
//       J = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
//       K = /[A-Z]/g;

//   function Z(e, t, n) {
//     var r, i;
//     if (void 0 === n && 1 === e.nodeType) if (r = "data-" + t.replace(K, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(r))) {
//       try {
//         n = "true" === (i = n) || "false" !== i && ("null" === i ? null : i === +i + "" ? +i : J.test(i) ? JSON.parse(i) : i);
//       } catch (e) {}

//       Q.set(e, t, n);
//     } else n = void 0;
//     return n;
//   }

//   S.extend({
//     hasData: function hasData(e) {
//       return Q.hasData(e) || Y.hasData(e);
//     },
//     data: function data(e, t, n) {
//       return Q.access(e, t, n);
//     },
//     removeData: function removeData(e, t) {
//       Q.remove(e, t);
//     },
//     _data: function _data(e, t, n) {
//       return Y.access(e, t, n);
//     },
//     _removeData: function _removeData(e, t) {
//       Y.remove(e, t);
//     }
//   }), S.fn.extend({
//     data: function data(n, e) {
//       var t,
//           r,
//           i,
//           o = this[0],
//           a = o && o.attributes;

//       if (void 0 === n) {
//         if (this.length && (i = Q.get(o), 1 === o.nodeType && !Y.get(o, "hasDataAttrs"))) {
//           t = a.length;

//           while (t--) {
//             a[t] && 0 === (r = a[t].name).indexOf("data-") && (r = X(r.slice(5)), Z(o, r, i[r]));
//           }

//           Y.set(o, "hasDataAttrs", !0);
//         }

//         return i;
//       }

//       return "object" == _typeof(n) ? this.each(function () {
//         Q.set(this, n);
//       }) : $(this, function (e) {
//         var t;
//         if (o && void 0 === e) return void 0 !== (t = Q.get(o, n)) ? t : void 0 !== (t = Z(o, n)) ? t : void 0;
//         this.each(function () {
//           Q.set(this, n, e);
//         });
//       }, null, e, 1 < arguments.length, null, !0);
//     },
//     removeData: function removeData(e) {
//       return this.each(function () {
//         Q.remove(this, e);
//       });
//     }
//   }), S.extend({
//     queue: function queue(e, t, n) {
//       var r;
//       if (e) return t = (t || "fx") + "queue", r = Y.get(e, t), n && (!r || Array.isArray(n) ? r = Y.access(e, t, S.makeArray(n)) : r.push(n)), r || [];
//     },
//     dequeue: function dequeue(e, t) {
//       t = t || "fx";

//       var n = S.queue(e, t),
//           r = n.length,
//           i = n.shift(),
//           o = S._queueHooks(e, t);

//       "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, function () {
//         S.dequeue(e, t);
//       }, o)), !r && o && o.empty.fire();
//     },
//     _queueHooks: function _queueHooks(e, t) {
//       var n = t + "queueHooks";
//       return Y.get(e, n) || Y.access(e, n, {
//         empty: S.Callbacks("once memory").add(function () {
//           Y.remove(e, [t + "queue", n]);
//         })
//       });
//     }
//   }), S.fn.extend({
//     queue: function queue(t, n) {
//       var e = 2;
//       return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? S.queue(this[0], t) : void 0 === n ? this : this.each(function () {
//         var e = S.queue(this, t, n);
//         S._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && S.dequeue(this, t);
//       });
//     },
//     dequeue: function dequeue(e) {
//       return this.each(function () {
//         S.dequeue(this, e);
//       });
//     },
//     clearQueue: function clearQueue(e) {
//       return this.queue(e || "fx", []);
//     },
//     promise: function promise(e, t) {
//       var n,
//           r = 1,
//           i = S.Deferred(),
//           o = this,
//           a = this.length,
//           s = function s() {
//         --r || i.resolveWith(o, [o]);
//       };

//       "string" != typeof e && (t = e, e = void 0), e = e || "fx";

//       while (a--) {
//         (n = Y.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
//       }

//       return s(), i.promise(t);
//     }
//   });

//   var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
//       te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
//       ne = ["Top", "Right", "Bottom", "Left"],
//       re = E.documentElement,
//       ie = function ie(e) {
//     return S.contains(e.ownerDocument, e);
//   },
//       oe = {
//     composed: !0
//   };

//   re.getRootNode && (ie = function ie(e) {
//     return S.contains(e.ownerDocument, e) || e.getRootNode(oe) === e.ownerDocument;
//   });

//   var ae = function ae(e, t) {
//     return "none" === (e = t || e).style.display || "" === e.style.display && ie(e) && "none" === S.css(e, "display");
//   };

//   function se(e, t, n, r) {
//     var i,
//         o,
//         a = 20,
//         s = r ? function () {
//       return r.cur();
//     } : function () {
//       return S.css(e, t, "");
//     },
//         u = s(),
//         l = n && n[3] || (S.cssNumber[t] ? "" : "px"),
//         c = e.nodeType && (S.cssNumber[t] || "px" !== l && +u) && te.exec(S.css(e, t));

//     if (c && c[3] !== l) {
//       u /= 2, l = l || c[3], c = +u || 1;

//       while (a--) {
//         S.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
//       }

//       c *= 2, S.style(e, t, c + l), n = n || [];
//     }

//     return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i;
//   }

//   var ue = {};

//   function le(e, t) {
//     for (var n, r, i, o, a, s, u, l = [], c = 0, f = e.length; c < f; c++) {
//       (r = e[c]).style && (n = r.style.display, t ? ("none" === n && (l[c] = Y.get(r, "display") || null, l[c] || (r.style.display = "")), "" === r.style.display && ae(r) && (l[c] = (u = a = o = void 0, a = (i = r).ownerDocument, s = i.nodeName, (u = ue[s]) || (o = a.body.appendChild(a.createElement(s)), u = S.css(o, "display"), o.parentNode.removeChild(o), "none" === u && (u = "block"), ue[s] = u)))) : "none" !== n && (l[c] = "none", Y.set(r, "display", n)));
//     }

//     for (c = 0; c < f; c++) {
//       null != l[c] && (e[c].style.display = l[c]);
//     }

//     return e;
//   }

//   S.fn.extend({
//     show: function show() {
//       return le(this, !0);
//     },
//     hide: function hide() {
//       return le(this);
//     },
//     toggle: function toggle(e) {
//       return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
//         ae(this) ? S(this).show() : S(this).hide();
//       });
//     }
//   });
//   var ce,
//       fe,
//       pe = /^(?:checkbox|radio)$/i,
//       de = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
//       he = /^$|^module$|\/(?:java|ecma)script/i;
//   ce = E.createDocumentFragment().appendChild(E.createElement("div")), (fe = E.createElement("input")).setAttribute("type", "radio"), fe.setAttribute("checked", "checked"), fe.setAttribute("name", "t"), ce.appendChild(fe), y.checkClone = ce.cloneNode(!0).cloneNode(!0).lastChild.checked, ce.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!ce.cloneNode(!0).lastChild.defaultValue, ce.innerHTML = "<option></option>", y.option = !!ce.lastChild;
//   var ge = {
//     thead: [1, "<table>", "</table>"],
//     col: [2, "<table><colgroup>", "</colgroup></table>"],
//     tr: [2, "<table><tbody>", "</tbody></table>"],
//     td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
//     _default: [0, "", ""]
//   };

//   function ve(e, t) {
//     var n;
//     return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && A(e, t) ? S.merge([e], n) : n;
//   }

//   function ye(e, t) {
//     for (var n = 0, r = e.length; n < r; n++) {
//       Y.set(e[n], "globalEval", !t || Y.get(t[n], "globalEval"));
//     }
//   }

//   ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, ge.th = ge.td, y.option || (ge.optgroup = ge.option = [1, "<select multiple='multiple'>", "</select>"]);
//   var me = /<|&#?\w+;/;

//   function xe(e, t, n, r, i) {
//     for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++) {
//       if ((o = e[d]) || 0 === o) if ("object" === w(o)) S.merge(p, o.nodeType ? [o] : o);else if (me.test(o)) {
//         a = a || f.appendChild(t.createElement("div")), s = (de.exec(o) || ["", ""])[1].toLowerCase(), u = ge[s] || ge._default, a.innerHTML = u[1] + S.htmlPrefilter(o) + u[2], c = u[0];

//         while (c--) {
//           a = a.lastChild;
//         }

//         S.merge(p, a.childNodes), (a = f.firstChild).textContent = "";
//       } else p.push(t.createTextNode(o));
//     }

//     f.textContent = "", d = 0;

//     while (o = p[d++]) {
//       if (r && -1 < S.inArray(o, r)) i && i.push(o);else if (l = ie(o), a = ve(f.appendChild(o), "script"), l && ye(a), n) {
//         c = 0;

//         while (o = a[c++]) {
//           he.test(o.type || "") && n.push(o);
//         }
//       }
//     }

//     return f;
//   }

//   var be = /^key/,
//       we = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
//       Te = /^([^.]*)(?:\.(.+)|)/;

//   function Ce() {
//     return !0;
//   }

//   function Ee() {
//     return !1;
//   }

//   function Se(e, t) {
//     return e === function () {
//       try {
//         return E.activeElement;
//       } catch (e) {}
//     }() == ("focus" === t);
//   }

//   function ke(e, t, n, r, i, o) {
//     var a, s;

//     if ("object" == _typeof(t)) {
//       for (s in "string" != typeof n && (r = r || n, n = void 0), t) {
//         ke(e, s, n, r, t[s], o);
//       }

//       return e;
//     }

//     if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = Ee;else if (!i) return e;
//     return 1 === o && (a = i, (i = function i(e) {
//       return S().off(e), a.apply(this, arguments);
//     }).guid = a.guid || (a.guid = S.guid++)), e.each(function () {
//       S.event.add(this, t, i, r, n);
//     });
//   }

//   function Ae(e, i, o) {
//     o ? (Y.set(e, i, !1), S.event.add(e, i, {
//       namespace: !1,
//       handler: function handler(e) {
//         var t,
//             n,
//             r = Y.get(this, i);

//         if (1 & e.isTrigger && this[i]) {
//           if (r.length) (S.event.special[i] || {}).delegateType && e.stopPropagation();else if (r = s.call(arguments), Y.set(this, i, r), t = o(this, i), this[i](), r !== (n = Y.get(this, i)) || t ? Y.set(this, i, !1) : n = {}, r !== n) return e.stopImmediatePropagation(), e.preventDefault(), n.value;
//         } else r.length && (Y.set(this, i, {
//           value: S.event.trigger(S.extend(r[0], S.Event.prototype), r.slice(1), this)
//         }), e.stopImmediatePropagation());
//       }
//     })) : void 0 === Y.get(e, i) && S.event.add(e, i, Ce);
//   }

//   S.event = {
//     global: {},
//     add: function add(t, e, n, r, i) {
//       var o,
//           a,
//           s,
//           u,
//           l,
//           c,
//           f,
//           p,
//           d,
//           h,
//           g,
//           v = Y.get(t);

//       if (V(t)) {
//         n.handler && (n = (o = n).handler, i = o.selector), i && S.find.matchesSelector(re, i), n.guid || (n.guid = S.guid++), (u = v.events) || (u = v.events = Object.create(null)), (a = v.handle) || (a = v.handle = function (e) {
//           return "undefined" != typeof S && S.event.triggered !== e.type ? S.event.dispatch.apply(t, arguments) : void 0;
//         }), l = (e = (e || "").match(P) || [""]).length;

//         while (l--) {
//           d = g = (s = Te.exec(e[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = S.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = S.event.special[d] || {}, c = S.extend({
//             type: d,
//             origType: g,
//             data: r,
//             handler: n,
//             guid: n.guid,
//             selector: i,
//             needsContext: i && S.expr.match.needsContext.test(i),
//             namespace: h.join(".")
//           }, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(t, r, h, a) || t.addEventListener && t.addEventListener(d, a)), f.add && (f.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), S.event.global[d] = !0);
//         }
//       }
//     },
//     remove: function remove(e, t, n, r, i) {
//       var o,
//           a,
//           s,
//           u,
//           l,
//           c,
//           f,
//           p,
//           d,
//           h,
//           g,
//           v = Y.hasData(e) && Y.get(e);

//       if (v && (u = v.events)) {
//         l = (t = (t || "").match(P) || [""]).length;

//         while (l--) {
//           if (d = g = (s = Te.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d) {
//             f = S.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length;

//             while (o--) {
//               c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
//             }

//             a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, v.handle) || S.removeEvent(e, d, v.handle), delete u[d]);
//           } else for (d in u) {
//             S.event.remove(e, d + t[l], n, r, !0);
//           }
//         }

//         S.isEmptyObject(u) && Y.remove(e, "handle events");
//       }
//     },
//     dispatch: function dispatch(e) {
//       var t,
//           n,
//           r,
//           i,
//           o,
//           a,
//           s = new Array(arguments.length),
//           u = S.event.fix(e),
//           l = (Y.get(this, "events") || Object.create(null))[u.type] || [],
//           c = S.event.special[u.type] || {};

//       for (s[0] = u, t = 1; t < arguments.length; t++) {
//         s[t] = arguments[t];
//       }

//       if (u.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, u)) {
//         a = S.event.handlers.call(this, u, l), t = 0;

//         while ((i = a[t++]) && !u.isPropagationStopped()) {
//           u.currentTarget = i.elem, n = 0;

//           while ((o = i.handlers[n++]) && !u.isImmediatePropagationStopped()) {
//             u.rnamespace && !1 !== o.namespace && !u.rnamespace.test(o.namespace) || (u.handleObj = o, u.data = o.data, void 0 !== (r = ((S.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, s)) && !1 === (u.result = r) && (u.preventDefault(), u.stopPropagation()));
//           }
//         }

//         return c.postDispatch && c.postDispatch.call(this, u), u.result;
//       }
//     },
//     handlers: function handlers(e, t) {
//       var n,
//           r,
//           i,
//           o,
//           a,
//           s = [],
//           u = t.delegateCount,
//           l = e.target;
//       if (u && l.nodeType && !("click" === e.type && 1 <= e.button)) for (; l !== this; l = l.parentNode || this) {
//         if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
//           for (o = [], a = {}, n = 0; n < u; n++) {
//             void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? -1 < S(i, this).index(l) : S.find(i, this, null, [l]).length), a[i] && o.push(r);
//           }

//           o.length && s.push({
//             elem: l,
//             handlers: o
//           });
//         }
//       }
//       return l = this, u < t.length && s.push({
//         elem: l,
//         handlers: t.slice(u)
//       }), s;
//     },
//     addProp: function addProp(t, e) {
//       Object.defineProperty(S.Event.prototype, t, {
//         enumerable: !0,
//         configurable: !0,
//         get: m(e) ? function () {
//           if (this.originalEvent) return e(this.originalEvent);
//         } : function () {
//           if (this.originalEvent) return this.originalEvent[t];
//         },
//         set: function set(e) {
//           Object.defineProperty(this, t, {
//             enumerable: !0,
//             configurable: !0,
//             writable: !0,
//             value: e
//           });
//         }
//       });
//     },
//     fix: function fix(e) {
//       return e[S.expando] ? e : new S.Event(e);
//     },
//     special: {
//       load: {
//         noBubble: !0
//       },
//       click: {
//         setup: function setup(e) {
//           var t = this || e;
//           return pe.test(t.type) && t.click && A(t, "input") && Ae(t, "click", Ce), !1;
//         },
//         trigger: function trigger(e) {
//           var t = this || e;
//           return pe.test(t.type) && t.click && A(t, "input") && Ae(t, "click"), !0;
//         },
//         _default: function _default(e) {
//           var t = e.target;
//           return pe.test(t.type) && t.click && A(t, "input") && Y.get(t, "click") || A(t, "a");
//         }
//       },
//       beforeunload: {
//         postDispatch: function postDispatch(e) {
//           void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result);
//         }
//       }
//     }
//   }, S.removeEvent = function (e, t, n) {
//     e.removeEventListener && e.removeEventListener(t, n);
//   }, S.Event = function (e, t) {
//     if (!(this instanceof S.Event)) return new S.Event(e, t);
//     e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? Ce : Ee, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && S.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[S.expando] = !0;
//   }, S.Event.prototype = {
//     constructor: S.Event,
//     isDefaultPrevented: Ee,
//     isPropagationStopped: Ee,
//     isImmediatePropagationStopped: Ee,
//     isSimulated: !1,
//     preventDefault: function preventDefault() {
//       var e = this.originalEvent;
//       this.isDefaultPrevented = Ce, e && !this.isSimulated && e.preventDefault();
//     },
//     stopPropagation: function stopPropagation() {
//       var e = this.originalEvent;
//       this.isPropagationStopped = Ce, e && !this.isSimulated && e.stopPropagation();
//     },
//     stopImmediatePropagation: function stopImmediatePropagation() {
//       var e = this.originalEvent;
//       this.isImmediatePropagationStopped = Ce, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation();
//     }
//   }, S.each({
//     altKey: !0,
//     bubbles: !0,
//     cancelable: !0,
//     changedTouches: !0,
//     ctrlKey: !0,
//     detail: !0,
//     eventPhase: !0,
//     metaKey: !0,
//     pageX: !0,
//     pageY: !0,
//     shiftKey: !0,
//     view: !0,
//     "char": !0,
//     code: !0,
//     charCode: !0,
//     key: !0,
//     keyCode: !0,
//     button: !0,
//     buttons: !0,
//     clientX: !0,
//     clientY: !0,
//     offsetX: !0,
//     offsetY: !0,
//     pointerId: !0,
//     pointerType: !0,
//     screenX: !0,
//     screenY: !0,
//     targetTouches: !0,
//     toElement: !0,
//     touches: !0,
//     which: function which(e) {
//       var t = e.button;
//       return null == e.which && be.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && we.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which;
//     }
//   }, S.event.addProp), S.each({
//     focus: "focusin",
//     blur: "focusout"
//   }, function (e, t) {
//     S.event.special[e] = {
//       setup: function setup() {
//         return Ae(this, e, Se), !1;
//       },
//       trigger: function trigger() {
//         return Ae(this, e), !0;
//       },
//       delegateType: t
//     };
//   }), S.each({
//     mouseenter: "mouseover",
//     mouseleave: "mouseout",
//     pointerenter: "pointerover",
//     pointerleave: "pointerout"
//   }, function (e, i) {
//     S.event.special[e] = {
//       delegateType: i,
//       bindType: i,
//       handle: function handle(e) {
//         var t,
//             n = e.relatedTarget,
//             r = e.handleObj;
//         return n && (n === this || S.contains(this, n)) || (e.type = r.origType, t = r.handler.apply(this, arguments), e.type = i), t;
//       }
//     };
//   }), S.fn.extend({
//     on: function on(e, t, n, r) {
//       return ke(this, e, t, n, r);
//     },
//     one: function one(e, t, n, r) {
//       return ke(this, e, t, n, r, 1);
//     },
//     off: function off(e, t, n) {
//       var r, i;
//       if (e && e.preventDefault && e.handleObj) return r = e.handleObj, S(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;

//       if ("object" == _typeof(e)) {
//         for (i in e) {
//           this.off(i, t, e[i]);
//         }

//         return this;
//       }

//       return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = Ee), this.each(function () {
//         S.event.remove(this, e, n, t);
//       });
//     }
//   });
//   var Ne = /<script|<style|<link/i,
//       De = /checked\s*(?:[^=]|=\s*.checked.)/i,
//       je = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

//   function qe(e, t) {
//     return A(e, "table") && A(11 !== t.nodeType ? t : t.firstChild, "tr") && S(e).children("tbody")[0] || e;
//   }

//   function Le(e) {
//     return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e;
//   }

//   function He(e) {
//     return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e;
//   }

//   function Oe(e, t) {
//     var n, r, i, o, a, s;

//     if (1 === t.nodeType) {
//       if (Y.hasData(e) && (s = Y.get(e).events)) for (i in Y.remove(t, "handle events"), s) {
//         for (n = 0, r = s[i].length; n < r; n++) {
//           S.event.add(t, i, s[i][n]);
//         }
//       }
//       Q.hasData(e) && (o = Q.access(e), a = S.extend({}, o), Q.set(t, a));
//     }
//   }

//   function Pe(n, r, i, o) {
//     r = g(r);
//     var e,
//         t,
//         a,
//         s,
//         u,
//         l,
//         c = 0,
//         f = n.length,
//         p = f - 1,
//         d = r[0],
//         h = m(d);
//     if (h || 1 < f && "string" == typeof d && !y.checkClone && De.test(d)) return n.each(function (e) {
//       var t = n.eq(e);
//       h && (r[0] = d.call(this, e, t.html())), Pe(t, r, i, o);
//     });

//     if (f && (t = (e = xe(r, n[0].ownerDocument, !1, n, o)).firstChild, 1 === e.childNodes.length && (e = t), t || o)) {
//       for (s = (a = S.map(ve(e, "script"), Le)).length; c < f; c++) {
//         u = e, c !== p && (u = S.clone(u, !0, !0), s && S.merge(a, ve(u, "script"))), i.call(n[c], u, c);
//       }

//       if (s) for (l = a[a.length - 1].ownerDocument, S.map(a, He), c = 0; c < s; c++) {
//         u = a[c], he.test(u.type || "") && !Y.access(u, "globalEval") && S.contains(l, u) && (u.src && "module" !== (u.type || "").toLowerCase() ? S._evalUrl && !u.noModule && S._evalUrl(u.src, {
//           nonce: u.nonce || u.getAttribute("nonce")
//         }, l) : b(u.textContent.replace(je, ""), u, l));
//       }
//     }

//     return n;
//   }

//   function Re(e, t, n) {
//     for (var r, i = t ? S.filter(t, e) : e, o = 0; null != (r = i[o]); o++) {
//       n || 1 !== r.nodeType || S.cleanData(ve(r)), r.parentNode && (n && ie(r) && ye(ve(r, "script")), r.parentNode.removeChild(r));
//     }

//     return e;
//   }

//   S.extend({
//     htmlPrefilter: function htmlPrefilter(e) {
//       return e;
//     },
//     clone: function clone(e, t, n) {
//       var r,
//           i,
//           o,
//           a,
//           s,
//           u,
//           l,
//           c = e.cloneNode(!0),
//           f = ie(e);
//       if (!(y.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || S.isXMLDoc(e))) for (a = ve(c), r = 0, i = (o = ve(e)).length; r < i; r++) {
//         s = o[r], u = a[r], void 0, "input" === (l = u.nodeName.toLowerCase()) && pe.test(s.type) ? u.checked = s.checked : "input" !== l && "textarea" !== l || (u.defaultValue = s.defaultValue);
//       }
//       if (t) if (n) for (o = o || ve(e), a = a || ve(c), r = 0, i = o.length; r < i; r++) {
//         Oe(o[r], a[r]);
//       } else Oe(e, c);
//       return 0 < (a = ve(c, "script")).length && ye(a, !f && ve(e, "script")), c;
//     },
//     cleanData: function cleanData(e) {
//       for (var t, n, r, i = S.event.special, o = 0; void 0 !== (n = e[o]); o++) {
//         if (V(n)) {
//           if (t = n[Y.expando]) {
//             if (t.events) for (r in t.events) {
//               i[r] ? S.event.remove(n, r) : S.removeEvent(n, r, t.handle);
//             }
//             n[Y.expando] = void 0;
//           }

//           n[Q.expando] && (n[Q.expando] = void 0);
//         }
//       }
//     }
//   }), S.fn.extend({
//     detach: function detach(e) {
//       return Re(this, e, !0);
//     },
//     remove: function remove(e) {
//       return Re(this, e);
//     },
//     text: function text(e) {
//       return $(this, function (e) {
//         return void 0 === e ? S.text(this) : this.empty().each(function () {
//           1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e);
//         });
//       }, null, e, arguments.length);
//     },
//     append: function append() {
//       return Pe(this, arguments, function (e) {
//         1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || qe(this, e).appendChild(e);
//       });
//     },
//     prepend: function prepend() {
//       return Pe(this, arguments, function (e) {
//         if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
//           var t = qe(this, e);
//           t.insertBefore(e, t.firstChild);
//         }
//       });
//     },
//     before: function before() {
//       return Pe(this, arguments, function (e) {
//         this.parentNode && this.parentNode.insertBefore(e, this);
//       });
//     },
//     after: function after() {
//       return Pe(this, arguments, function (e) {
//         this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
//       });
//     },
//     empty: function empty() {
//       for (var e, t = 0; null != (e = this[t]); t++) {
//         1 === e.nodeType && (S.cleanData(ve(e, !1)), e.textContent = "");
//       }

//       return this;
//     },
//     clone: function clone(e, t) {
//       return e = null != e && e, t = null == t ? e : t, this.map(function () {
//         return S.clone(this, e, t);
//       });
//     },
//     html: function html(e) {
//       return $(this, function (e) {
//         var t = this[0] || {},
//             n = 0,
//             r = this.length;
//         if (void 0 === e && 1 === t.nodeType) return t.innerHTML;

//         if ("string" == typeof e && !Ne.test(e) && !ge[(de.exec(e) || ["", ""])[1].toLowerCase()]) {
//           e = S.htmlPrefilter(e);

//           try {
//             for (; n < r; n++) {
//               1 === (t = this[n] || {}).nodeType && (S.cleanData(ve(t, !1)), t.innerHTML = e);
//             }

//             t = 0;
//           } catch (e) {}
//         }

//         t && this.empty().append(e);
//       }, null, e, arguments.length);
//     },
//     replaceWith: function replaceWith() {
//       var n = [];
//       return Pe(this, arguments, function (e) {
//         var t = this.parentNode;
//         S.inArray(this, n) < 0 && (S.cleanData(ve(this)), t && t.replaceChild(e, this));
//       }, n);
//     }
//   }), S.each({
//     appendTo: "append",
//     prependTo: "prepend",
//     insertBefore: "before",
//     insertAfter: "after",
//     replaceAll: "replaceWith"
//   }, function (e, a) {
//     S.fn[e] = function (e) {
//       for (var t, n = [], r = S(e), i = r.length - 1, o = 0; o <= i; o++) {
//         t = o === i ? this : this.clone(!0), S(r[o])[a](t), u.apply(n, t.get());
//       }

//       return this.pushStack(n);
//     };
//   });

//   var Me = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
//       Ie = function Ie(e) {
//     var t = e.ownerDocument.defaultView;
//     return t && t.opener || (t = C), t.getComputedStyle(e);
//   },
//       We = function We(e, t, n) {
//     var r,
//         i,
//         o = {};

//     for (i in t) {
//       o[i] = e.style[i], e.style[i] = t[i];
//     }

//     for (i in r = n.call(e), t) {
//       e.style[i] = o[i];
//     }

//     return r;
//   },
//       Fe = new RegExp(ne.join("|"), "i");

//   function Be(e, t, n) {
//     var r,
//         i,
//         o,
//         a,
//         s = e.style;
//     return (n = n || Ie(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || ie(e) || (a = S.style(e, t)), !y.pixelBoxStyles() && Me.test(a) && Fe.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a;
//   }

//   function $e(e, t) {
//     return {
//       get: function get() {
//         if (!e()) return (this.get = t).apply(this, arguments);
//         delete this.get;
//       }
//     };
//   }

//   !function () {
//     function e() {
//       if (l) {
//         u.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", re.appendChild(u).appendChild(l);
//         var e = C.getComputedStyle(l);
//         n = "1%" !== e.top, s = 12 === t(e.marginLeft), l.style.right = "60%", o = 36 === t(e.right), r = 36 === t(e.width), l.style.position = "absolute", i = 12 === t(l.offsetWidth / 3), re.removeChild(u), l = null;
//       }
//     }

//     function t(e) {
//       return Math.round(parseFloat(e));
//     }

//     var n,
//         r,
//         i,
//         o,
//         a,
//         s,
//         u = E.createElement("div"),
//         l = E.createElement("div");
//     l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", y.clearCloneStyle = "content-box" === l.style.backgroundClip, S.extend(y, {
//       boxSizingReliable: function boxSizingReliable() {
//         return e(), r;
//       },
//       pixelBoxStyles: function pixelBoxStyles() {
//         return e(), o;
//       },
//       pixelPosition: function pixelPosition() {
//         return e(), n;
//       },
//       reliableMarginLeft: function reliableMarginLeft() {
//         return e(), s;
//       },
//       scrollboxSize: function scrollboxSize() {
//         return e(), i;
//       },
//       reliableTrDimensions: function reliableTrDimensions() {
//         var e, t, n, r;
//         return null == a && (e = E.createElement("table"), t = E.createElement("tr"), n = E.createElement("div"), e.style.cssText = "position:absolute;left:-11111px", t.style.height = "1px", n.style.height = "9px", re.appendChild(e).appendChild(t).appendChild(n), r = C.getComputedStyle(t), a = 3 < parseInt(r.height), re.removeChild(e)), a;
//       }
//     }));
//   }();
//   var _e = ["Webkit", "Moz", "ms"],
//       ze = E.createElement("div").style,
//       Ue = {};

//   function Xe(e) {
//     var t = S.cssProps[e] || Ue[e];
//     return t || (e in ze ? e : Ue[e] = function (e) {
//       var t = e[0].toUpperCase() + e.slice(1),
//           n = _e.length;

//       while (n--) {
//         if ((e = _e[n] + t) in ze) return e;
//       }
//     }(e) || e);
//   }

//   var Ve = /^(none|table(?!-c[ea]).+)/,
//       Ge = /^--/,
//       Ye = {
//     position: "absolute",
//     visibility: "hidden",
//     display: "block"
//   },
//       Qe = {
//     letterSpacing: "0",
//     fontWeight: "400"
//   };

//   function Je(e, t, n) {
//     var r = te.exec(t);
//     return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t;
//   }

//   function Ke(e, t, n, r, i, o) {
//     var a = "width" === t ? 1 : 0,
//         s = 0,
//         u = 0;
//     if (n === (r ? "border" : "content")) return 0;

//     for (; a < 4; a += 2) {
//       "margin" === n && (u += S.css(e, n + ne[a], !0, i)), r ? ("content" === n && (u -= S.css(e, "padding" + ne[a], !0, i)), "margin" !== n && (u -= S.css(e, "border" + ne[a] + "Width", !0, i))) : (u += S.css(e, "padding" + ne[a], !0, i), "padding" !== n ? u += S.css(e, "border" + ne[a] + "Width", !0, i) : s += S.css(e, "border" + ne[a] + "Width", !0, i));
//     }

//     return !r && 0 <= o && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5)) || 0), u;
//   }

//   function Ze(e, t, n) {
//     var r = Ie(e),
//         i = (!y.boxSizingReliable() || n) && "border-box" === S.css(e, "boxSizing", !1, r),
//         o = i,
//         a = Be(e, t, r),
//         s = "offset" + t[0].toUpperCase() + t.slice(1);

//     if (Me.test(a)) {
//       if (!n) return a;
//       a = "auto";
//     }

//     return (!y.boxSizingReliable() && i || !y.reliableTrDimensions() && A(e, "tr") || "auto" === a || !parseFloat(a) && "inline" === S.css(e, "display", !1, r)) && e.getClientRects().length && (i = "border-box" === S.css(e, "boxSizing", !1, r), (o = s in e) && (a = e[s])), (a = parseFloat(a) || 0) + Ke(e, t, n || (i ? "border" : "content"), o, r, a) + "px";
//   }

//   function et(e, t, n, r, i) {
//     return new et.prototype.init(e, t, n, r, i);
//   }

//   S.extend({
//     cssHooks: {
//       opacity: {
//         get: function get(e, t) {
//           if (t) {
//             var n = Be(e, "opacity");
//             return "" === n ? "1" : n;
//           }
//         }
//       }
//     },
//     cssNumber: {
//       animationIterationCount: !0,
//       columnCount: !0,
//       fillOpacity: !0,
//       flexGrow: !0,
//       flexShrink: !0,
//       fontWeight: !0,
//       gridArea: !0,
//       gridColumn: !0,
//       gridColumnEnd: !0,
//       gridColumnStart: !0,
//       gridRow: !0,
//       gridRowEnd: !0,
//       gridRowStart: !0,
//       lineHeight: !0,
//       opacity: !0,
//       order: !0,
//       orphans: !0,
//       widows: !0,
//       zIndex: !0,
//       zoom: !0
//     },
//     cssProps: {},
//     style: function style(e, t, n, r) {
//       if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
//         var i,
//             o,
//             a,
//             s = X(t),
//             u = Ge.test(t),
//             l = e.style;
//         if (u || (t = Xe(s)), a = S.cssHooks[t] || S.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
//         "string" === (o = _typeof(n)) && (i = te.exec(n)) && i[1] && (n = se(e, t, i), o = "number"), null != n && n == n && ("number" !== o || u || (n += i && i[3] || (S.cssNumber[s] ? "" : "px")), y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n));
//       }
//     },
//     css: function css(e, t, n, r) {
//       var i,
//           o,
//           a,
//           s = X(t);
//       return Ge.test(t) || (t = Xe(s)), (a = S.cssHooks[t] || S.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = Be(e, t, r)), "normal" === i && t in Qe && (i = Qe[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i;
//     }
//   }), S.each(["height", "width"], function (e, u) {
//     S.cssHooks[u] = {
//       get: function get(e, t, n) {
//         if (t) return !Ve.test(S.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Ze(e, u, n) : We(e, Ye, function () {
//           return Ze(e, u, n);
//         });
//       },
//       set: function set(e, t, n) {
//         var r,
//             i = Ie(e),
//             o = !y.scrollboxSize() && "absolute" === i.position,
//             a = (o || n) && "border-box" === S.css(e, "boxSizing", !1, i),
//             s = n ? Ke(e, u, n, a, i) : 0;
//         return a && o && (s -= Math.ceil(e["offset" + u[0].toUpperCase() + u.slice(1)] - parseFloat(i[u]) - Ke(e, u, "border", !1, i) - .5)), s && (r = te.exec(t)) && "px" !== (r[3] || "px") && (e.style[u] = t, t = S.css(e, u)), Je(0, t, s);
//       }
//     };
//   }), S.cssHooks.marginLeft = $e(y.reliableMarginLeft, function (e, t) {
//     if (t) return (parseFloat(Be(e, "marginLeft")) || e.getBoundingClientRect().left - We(e, {
//       marginLeft: 0
//     }, function () {
//       return e.getBoundingClientRect().left;
//     })) + "px";
//   }), S.each({
//     margin: "",
//     padding: "",
//     border: "Width"
//   }, function (i, o) {
//     S.cssHooks[i + o] = {
//       expand: function expand(e) {
//         for (var t = 0, n = {}, r = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) {
//           n[i + ne[t] + o] = r[t] || r[t - 2] || r[0];
//         }

//         return n;
//       }
//     }, "margin" !== i && (S.cssHooks[i + o].set = Je);
//   }), S.fn.extend({
//     css: function css(e, t) {
//       return $(this, function (e, t, n) {
//         var r,
//             i,
//             o = {},
//             a = 0;

//         if (Array.isArray(t)) {
//           for (r = Ie(e), i = t.length; a < i; a++) {
//             o[t[a]] = S.css(e, t[a], !1, r);
//           }

//           return o;
//         }

//         return void 0 !== n ? S.style(e, t, n) : S.css(e, t);
//       }, e, t, 1 < arguments.length);
//     }
//   }), ((S.Tween = et).prototype = {
//     constructor: et,
//     init: function init(e, t, n, r, i, o) {
//       this.elem = e, this.prop = n, this.easing = i || S.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (S.cssNumber[n] ? "" : "px");
//     },
//     cur: function cur() {
//       var e = et.propHooks[this.prop];
//       return e && e.get ? e.get(this) : et.propHooks._default.get(this);
//     },
//     run: function run(e) {
//       var t,
//           n = et.propHooks[this.prop];
//       return this.options.duration ? this.pos = t = S.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : et.propHooks._default.set(this), this;
//     }
//   }).init.prototype = et.prototype, (et.propHooks = {
//     _default: {
//       get: function get(e) {
//         var t;
//         return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = S.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0;
//       },
//       set: function set(e) {
//         S.fx.step[e.prop] ? S.fx.step[e.prop](e) : 1 !== e.elem.nodeType || !S.cssHooks[e.prop] && null == e.elem.style[Xe(e.prop)] ? e.elem[e.prop] = e.now : S.style(e.elem, e.prop, e.now + e.unit);
//       }
//     }
//   }).scrollTop = et.propHooks.scrollLeft = {
//     set: function set(e) {
//       e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
//     }
//   }, S.easing = {
//     linear: function linear(e) {
//       return e;
//     },
//     swing: function swing(e) {
//       return .5 - Math.cos(e * Math.PI) / 2;
//     },
//     _default: "swing"
//   }, S.fx = et.prototype.init, S.fx.step = {};
//   var tt,
//       nt,
//       rt,
//       it,
//       ot = /^(?:toggle|show|hide)$/,
//       at = /queueHooks$/;

//   function st() {
//     nt && (!1 === E.hidden && C.requestAnimationFrame ? C.requestAnimationFrame(st) : C.setTimeout(st, S.fx.interval), S.fx.tick());
//   }

//   function ut() {
//     return C.setTimeout(function () {
//       tt = void 0;
//     }), tt = Date.now();
//   }

//   function lt(e, t) {
//     var n,
//         r = 0,
//         i = {
//       height: e
//     };

//     for (t = t ? 1 : 0; r < 4; r += 2 - t) {
//       i["margin" + (n = ne[r])] = i["padding" + n] = e;
//     }

//     return t && (i.opacity = i.width = e), i;
//   }

//   function ct(e, t, n) {
//     for (var r, i = (ft.tweeners[t] || []).concat(ft.tweeners["*"]), o = 0, a = i.length; o < a; o++) {
//       if (r = i[o].call(n, t, e)) return r;
//     }
//   }

//   function ft(o, e, t) {
//     var n,
//         a,
//         r = 0,
//         i = ft.prefilters.length,
//         s = S.Deferred().always(function () {
//       delete u.elem;
//     }),
//         u = function u() {
//       if (a) return !1;

//       for (var e = tt || ut(), t = Math.max(0, l.startTime + l.duration - e), n = 1 - (t / l.duration || 0), r = 0, i = l.tweens.length; r < i; r++) {
//         l.tweens[r].run(n);
//       }

//       return s.notifyWith(o, [l, n, t]), n < 1 && i ? t : (i || s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l]), !1);
//     },
//         l = s.promise({
//       elem: o,
//       props: S.extend({}, e),
//       opts: S.extend(!0, {
//         specialEasing: {},
//         easing: S.easing._default
//       }, t),
//       originalProperties: e,
//       originalOptions: t,
//       startTime: tt || ut(),
//       duration: t.duration,
//       tweens: [],
//       createTween: function createTween(e, t) {
//         var n = S.Tween(o, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
//         return l.tweens.push(n), n;
//       },
//       stop: function stop(e) {
//         var t = 0,
//             n = e ? l.tweens.length : 0;
//         if (a) return this;

//         for (a = !0; t < n; t++) {
//           l.tweens[t].run(1);
//         }

//         return e ? (s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l, e])) : s.rejectWith(o, [l, e]), this;
//       }
//     }),
//         c = l.props;

//     for (!function (e, t) {
//       var n, r, i, o, a;

//       for (n in e) {
//         if (i = t[r = X(n)], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = S.cssHooks[r]) && ("expand" in a)) for (n in o = a.expand(o), delete e[r], o) {
//           (n in e) || (e[n] = o[n], t[n] = i);
//         } else t[r] = i;
//       }
//     }(c, l.opts.specialEasing); r < i; r++) {
//       if (n = ft.prefilters[r].call(l, o, c, l.opts)) return m(n.stop) && (S._queueHooks(l.elem, l.opts.queue).stop = n.stop.bind(n)), n;
//     }

//     return S.map(c, ct, l), m(l.opts.start) && l.opts.start.call(o, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), S.fx.timer(S.extend(u, {
//       elem: o,
//       anim: l,
//       queue: l.opts.queue
//     })), l;
//   }

//   S.Animation = S.extend(ft, {
//     tweeners: {
//       "*": [function (e, t) {
//         var n = this.createTween(e, t);
//         return se(n.elem, e, te.exec(t), n), n;
//       }]
//     },
//     tweener: function tweener(e, t) {
//       m(e) ? (t = e, e = ["*"]) : e = e.match(P);

//       for (var n, r = 0, i = e.length; r < i; r++) {
//         n = e[r], ft.tweeners[n] = ft.tweeners[n] || [], ft.tweeners[n].unshift(t);
//       }
//     },
//     prefilters: [function (e, t, n) {
//       var r,
//           i,
//           o,
//           a,
//           s,
//           u,
//           l,
//           c,
//           f = "width" in t || "height" in t,
//           p = this,
//           d = {},
//           h = e.style,
//           g = e.nodeType && ae(e),
//           v = Y.get(e, "fxshow");

//       for (r in n.queue || (null == (a = S._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function () {
//         a.unqueued || s();
//       }), a.unqueued++, p.always(function () {
//         p.always(function () {
//           a.unqueued--, S.queue(e, "fx").length || a.empty.fire();
//         });
//       })), t) {
//         if (i = t[r], ot.test(i)) {
//           if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
//             if ("show" !== i || !v || void 0 === v[r]) continue;
//             g = !0;
//           }

//           d[r] = v && v[r] || S.style(e, r);
//         }
//       }

//       if ((u = !S.isEmptyObject(t)) || !S.isEmptyObject(d)) for (r in f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = v && v.display) && (l = Y.get(e, "display")), "none" === (c = S.css(e, "display")) && (l ? c = l : (le([e], !0), l = e.style.display || l, c = S.css(e, "display"), le([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === S.css(e, "float") && (u || (p.done(function () {
//         h.display = l;
//       }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
//         h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2];
//       })), u = !1, d) {
//         u || (v ? "hidden" in v && (g = v.hidden) : v = Y.access(e, "fxshow", {
//           display: l
//         }), o && (v.hidden = !g), g && le([e], !0), p.done(function () {
//           for (r in g || le([e]), Y.remove(e, "fxshow"), d) {
//             S.style(e, r, d[r]);
//           }
//         })), u = ct(g ? v[r] : 0, r, p), r in v || (v[r] = u.start, g && (u.end = u.start, u.start = 0));
//       }
//     }],
//     prefilter: function prefilter(e, t) {
//       t ? ft.prefilters.unshift(e) : ft.prefilters.push(e);
//     }
//   }), S.speed = function (e, t, n) {
//     var r = e && "object" == _typeof(e) ? S.extend({}, e) : {
//       complete: n || !n && t || m(e) && e,
//       duration: e,
//       easing: n && t || t && !m(t) && t
//     };
//     return S.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in S.fx.speeds ? r.duration = S.fx.speeds[r.duration] : r.duration = S.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function () {
//       m(r.old) && r.old.call(this), r.queue && S.dequeue(this, r.queue);
//     }, r;
//   }, S.fn.extend({
//     fadeTo: function fadeTo(e, t, n, r) {
//       return this.filter(ae).css("opacity", 0).show().end().animate({
//         opacity: t
//       }, e, n, r);
//     },
//     animate: function animate(t, e, n, r) {
//       var i = S.isEmptyObject(t),
//           o = S.speed(e, n, r),
//           a = function a() {
//         var e = ft(this, S.extend({}, t), o);
//         (i || Y.get(this, "finish")) && e.stop(!0);
//       };

//       return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a);
//     },
//     stop: function stop(i, e, o) {
//       var a = function a(e) {
//         var t = e.stop;
//         delete e.stop, t(o);
//       };

//       return "string" != typeof i && (o = e, e = i, i = void 0), e && this.queue(i || "fx", []), this.each(function () {
//         var e = !0,
//             t = null != i && i + "queueHooks",
//             n = S.timers,
//             r = Y.get(this);
//         if (t) r[t] && r[t].stop && a(r[t]);else for (t in r) {
//           r[t] && r[t].stop && at.test(t) && a(r[t]);
//         }

//         for (t = n.length; t--;) {
//           n[t].elem !== this || null != i && n[t].queue !== i || (n[t].anim.stop(o), e = !1, n.splice(t, 1));
//         }

//         !e && o || S.dequeue(this, i);
//       });
//     },
//     finish: function finish(a) {
//       return !1 !== a && (a = a || "fx"), this.each(function () {
//         var e,
//             t = Y.get(this),
//             n = t[a + "queue"],
//             r = t[a + "queueHooks"],
//             i = S.timers,
//             o = n ? n.length : 0;

//         for (t.finish = !0, S.queue(this, a, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--;) {
//           i[e].elem === this && i[e].queue === a && (i[e].anim.stop(!0), i.splice(e, 1));
//         }

//         for (e = 0; e < o; e++) {
//           n[e] && n[e].finish && n[e].finish.call(this);
//         }

//         delete t.finish;
//       });
//     }
//   }), S.each(["toggle", "show", "hide"], function (e, r) {
//     var i = S.fn[r];

//     S.fn[r] = function (e, t, n) {
//       return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(lt(r, !0), e, t, n);
//     };
//   }), S.each({
//     slideDown: lt("show"),
//     slideUp: lt("hide"),
//     slideToggle: lt("toggle"),
//     fadeIn: {
//       opacity: "show"
//     },
//     fadeOut: {
//       opacity: "hide"
//     },
//     fadeToggle: {
//       opacity: "toggle"
//     }
//   }, function (e, r) {
//     S.fn[e] = function (e, t, n) {
//       return this.animate(r, e, t, n);
//     };
//   }), S.timers = [], S.fx.tick = function () {
//     var e,
//         t = 0,
//         n = S.timers;

//     for (tt = Date.now(); t < n.length; t++) {
//       (e = n[t])() || n[t] !== e || n.splice(t--, 1);
//     }

//     n.length || S.fx.stop(), tt = void 0;
//   }, S.fx.timer = function (e) {
//     S.timers.push(e), S.fx.start();
//   }, S.fx.interval = 13, S.fx.start = function () {
//     nt || (nt = !0, st());
//   }, S.fx.stop = function () {
//     nt = null;
//   }, S.fx.speeds = {
//     slow: 600,
//     fast: 200,
//     _default: 400
//   }, S.fn.delay = function (r, e) {
//     return r = S.fx && S.fx.speeds[r] || r, e = e || "fx", this.queue(e, function (e, t) {
//       var n = C.setTimeout(e, r);

//       t.stop = function () {
//         C.clearTimeout(n);
//       };
//     });
//   }, rt = E.createElement("input"), it = E.createElement("select").appendChild(E.createElement("option")), rt.type = "checkbox", y.checkOn = "" !== rt.value, y.optSelected = it.selected, (rt = E.createElement("input")).value = "t", rt.type = "radio", y.radioValue = "t" === rt.value;
//   var pt,
//       dt = S.expr.attrHandle;
//   S.fn.extend({
//     attr: function attr(e, t) {
//       return $(this, S.attr, e, t, 1 < arguments.length);
//     },
//     removeAttr: function removeAttr(e) {
//       return this.each(function () {
//         S.removeAttr(this, e);
//       });
//     }
//   }), S.extend({
//     attr: function attr(e, t, n) {
//       var r,
//           i,
//           o = e.nodeType;
//       if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? S.prop(e, t, n) : (1 === o && S.isXMLDoc(e) || (i = S.attrHooks[t.toLowerCase()] || (S.expr.match.bool.test(t) ? pt : void 0)), void 0 !== n ? null === n ? void S.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = S.find.attr(e, t)) ? void 0 : r);
//     },
//     attrHooks: {
//       type: {
//         set: function set(e, t) {
//           if (!y.radioValue && "radio" === t && A(e, "input")) {
//             var n = e.value;
//             return e.setAttribute("type", t), n && (e.value = n), t;
//           }
//         }
//       }
//     },
//     removeAttr: function removeAttr(e, t) {
//       var n,
//           r = 0,
//           i = t && t.match(P);
//       if (i && 1 === e.nodeType) while (n = i[r++]) {
//         e.removeAttribute(n);
//       }
//     }
//   }), pt = {
//     set: function set(e, t, n) {
//       return !1 === t ? S.removeAttr(e, n) : e.setAttribute(n, n), n;
//     }
//   }, S.each(S.expr.match.bool.source.match(/\w+/g), function (e, t) {
//     var a = dt[t] || S.find.attr;

//     dt[t] = function (e, t, n) {
//       var r,
//           i,
//           o = t.toLowerCase();
//       return n || (i = dt[o], dt[o] = r, r = null != a(e, t, n) ? o : null, dt[o] = i), r;
//     };
//   });
//   var ht = /^(?:input|select|textarea|button)$/i,
//       gt = /^(?:a|area)$/i;

//   function vt(e) {
//     return (e.match(P) || []).join(" ");
//   }

//   function yt(e) {
//     return e.getAttribute && e.getAttribute("class") || "";
//   }

//   function mt(e) {
//     return Array.isArray(e) ? e : "string" == typeof e && e.match(P) || [];
//   }

//   S.fn.extend({
//     prop: function prop(e, t) {
//       return $(this, S.prop, e, t, 1 < arguments.length);
//     },
//     removeProp: function removeProp(e) {
//       return this.each(function () {
//         delete this[S.propFix[e] || e];
//       });
//     }
//   }), S.extend({
//     prop: function prop(e, t, n) {
//       var r,
//           i,
//           o = e.nodeType;
//       if (3 !== o && 8 !== o && 2 !== o) return 1 === o && S.isXMLDoc(e) || (t = S.propFix[t] || t, i = S.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t];
//     },
//     propHooks: {
//       tabIndex: {
//         get: function get(e) {
//           var t = S.find.attr(e, "tabindex");
//           return t ? parseInt(t, 10) : ht.test(e.nodeName) || gt.test(e.nodeName) && e.href ? 0 : -1;
//         }
//       }
//     },
//     propFix: {
//       "for": "htmlFor",
//       "class": "className"
//     }
//   }), y.optSelected || (S.propHooks.selected = {
//     get: function get(e) {
//       var t = e.parentNode;
//       return t && t.parentNode && t.parentNode.selectedIndex, null;
//     },
//     set: function set(e) {
//       var t = e.parentNode;
//       t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex);
//     }
//   }), S.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
//     S.propFix[this.toLowerCase()] = this;
//   }), S.fn.extend({
//     addClass: function addClass(t) {
//       var e,
//           n,
//           r,
//           i,
//           o,
//           a,
//           s,
//           u = 0;
//       if (m(t)) return this.each(function (e) {
//         S(this).addClass(t.call(this, e, yt(this)));
//       });
//       if ((e = mt(t)).length) while (n = this[u++]) {
//         if (i = yt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
//           a = 0;

//           while (o = e[a++]) {
//             r.indexOf(" " + o + " ") < 0 && (r += o + " ");
//           }

//           i !== (s = vt(r)) && n.setAttribute("class", s);
//         }
//       }
//       return this;
//     },
//     removeClass: function removeClass(t) {
//       var e,
//           n,
//           r,
//           i,
//           o,
//           a,
//           s,
//           u = 0;
//       if (m(t)) return this.each(function (e) {
//         S(this).removeClass(t.call(this, e, yt(this)));
//       });
//       if (!arguments.length) return this.attr("class", "");
//       if ((e = mt(t)).length) while (n = this[u++]) {
//         if (i = yt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
//           a = 0;

//           while (o = e[a++]) {
//             while (-1 < r.indexOf(" " + o + " ")) {
//               r = r.replace(" " + o + " ", " ");
//             }
//           }

//           i !== (s = vt(r)) && n.setAttribute("class", s);
//         }
//       }
//       return this;
//     },
//     toggleClass: function toggleClass(i, t) {
//       var o = _typeof(i),
//           a = "string" === o || Array.isArray(i);

//       return "boolean" == typeof t && a ? t ? this.addClass(i) : this.removeClass(i) : m(i) ? this.each(function (e) {
//         S(this).toggleClass(i.call(this, e, yt(this), t), t);
//       }) : this.each(function () {
//         var e, t, n, r;

//         if (a) {
//           t = 0, n = S(this), r = mt(i);

//           while (e = r[t++]) {
//             n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
//           }
//         } else void 0 !== i && "boolean" !== o || ((e = yt(this)) && Y.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === i ? "" : Y.get(this, "__className__") || ""));
//       });
//     },
//     hasClass: function hasClass(e) {
//       var t,
//           n,
//           r = 0;
//       t = " " + e + " ";

//       while (n = this[r++]) {
//         if (1 === n.nodeType && -1 < (" " + vt(yt(n)) + " ").indexOf(t)) return !0;
//       }

//       return !1;
//     }
//   });
//   var xt = /\r/g;
//   S.fn.extend({
//     val: function val(n) {
//       var r,
//           e,
//           i,
//           t = this[0];
//       return arguments.length ? (i = m(n), this.each(function (e) {
//         var t;
//         1 === this.nodeType && (null == (t = i ? n.call(this, e, S(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = S.map(t, function (e) {
//           return null == e ? "" : e + "";
//         })), (r = S.valHooks[this.type] || S.valHooks[this.nodeName.toLowerCase()]) && "set" in r && void 0 !== r.set(this, t, "value") || (this.value = t));
//       })) : t ? (r = S.valHooks[t.type] || S.valHooks[t.nodeName.toLowerCase()]) && "get" in r && void 0 !== (e = r.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(xt, "") : null == e ? "" : e : void 0;
//     }
//   }), S.extend({
//     valHooks: {
//       option: {
//         get: function get(e) {
//           var t = S.find.attr(e, "value");
//           return null != t ? t : vt(S.text(e));
//         }
//       },
//       select: {
//         get: function get(e) {
//           var t,
//               n,
//               r,
//               i = e.options,
//               o = e.selectedIndex,
//               a = "select-one" === e.type,
//               s = a ? null : [],
//               u = a ? o + 1 : i.length;

//           for (r = o < 0 ? u : a ? o : 0; r < u; r++) {
//             if (((n = i[r]).selected || r === o) && !n.disabled && (!n.parentNode.disabled || !A(n.parentNode, "optgroup"))) {
//               if (t = S(n).val(), a) return t;
//               s.push(t);
//             }
//           }

//           return s;
//         },
//         set: function set(e, t) {
//           var n,
//               r,
//               i = e.options,
//               o = S.makeArray(t),
//               a = i.length;

//           while (a--) {
//             ((r = i[a]).selected = -1 < S.inArray(S.valHooks.option.get(r), o)) && (n = !0);
//           }

//           return n || (e.selectedIndex = -1), o;
//         }
//       }
//     }
//   }), S.each(["radio", "checkbox"], function () {
//     S.valHooks[this] = {
//       set: function set(e, t) {
//         if (Array.isArray(t)) return e.checked = -1 < S.inArray(S(e).val(), t);
//       }
//     }, y.checkOn || (S.valHooks[this].get = function (e) {
//       return null === e.getAttribute("value") ? "on" : e.value;
//     });
//   }), y.focusin = "onfocusin" in C;

//   var bt = /^(?:focusinfocus|focusoutblur)$/,
//       wt = function wt(e) {
//     e.stopPropagation();
//   };

//   S.extend(S.event, {
//     trigger: function trigger(e, t, n, r) {
//       var i,
//           o,
//           a,
//           s,
//           u,
//           l,
//           c,
//           f,
//           p = [n || E],
//           d = v.call(e, "type") ? e.type : e,
//           h = v.call(e, "namespace") ? e.namespace.split(".") : [];

//       if (o = f = a = n = n || E, 3 !== n.nodeType && 8 !== n.nodeType && !bt.test(d + S.event.triggered) && (-1 < d.indexOf(".") && (d = (h = d.split(".")).shift(), h.sort()), u = d.indexOf(":") < 0 && "on" + d, (e = e[S.expando] ? e : new S.Event(d, "object" == _typeof(e) && e)).isTrigger = r ? 2 : 3, e.namespace = h.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : S.makeArray(t, [e]), c = S.event.special[d] || {}, r || !c.trigger || !1 !== c.trigger.apply(n, t))) {
//         if (!r && !c.noBubble && !x(n)) {
//           for (s = c.delegateType || d, bt.test(s + d) || (o = o.parentNode); o; o = o.parentNode) {
//             p.push(o), a = o;
//           }

//           a === (n.ownerDocument || E) && p.push(a.defaultView || a.parentWindow || C);
//         }

//         i = 0;

//         while ((o = p[i++]) && !e.isPropagationStopped()) {
//           f = o, e.type = 1 < i ? s : c.bindType || d, (l = (Y.get(o, "events") || Object.create(null))[e.type] && Y.get(o, "handle")) && l.apply(o, t), (l = u && o[u]) && l.apply && V(o) && (e.result = l.apply(o, t), !1 === e.result && e.preventDefault());
//         }

//         return e.type = d, r || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(p.pop(), t) || !V(n) || u && m(n[d]) && !x(n) && ((a = n[u]) && (n[u] = null), S.event.triggered = d, e.isPropagationStopped() && f.addEventListener(d, wt), n[d](), e.isPropagationStopped() && f.removeEventListener(d, wt), S.event.triggered = void 0, a && (n[u] = a)), e.result;
//       }
//     },
//     simulate: function simulate(e, t, n) {
//       var r = S.extend(new S.Event(), n, {
//         type: e,
//         isSimulated: !0
//       });
//       S.event.trigger(r, null, t);
//     }
//   }), S.fn.extend({
//     trigger: function trigger(e, t) {
//       return this.each(function () {
//         S.event.trigger(e, t, this);
//       });
//     },
//     triggerHandler: function triggerHandler(e, t) {
//       var n = this[0];
//       if (n) return S.event.trigger(e, t, n, !0);
//     }
//   }), y.focusin || S.each({
//     focus: "focusin",
//     blur: "focusout"
//   }, function (n, r) {
//     var i = function i(e) {
//       S.event.simulate(r, e.target, S.event.fix(e));
//     };

//     S.event.special[r] = {
//       setup: function setup() {
//         var e = this.ownerDocument || this.document || this,
//             t = Y.access(e, r);
//         t || e.addEventListener(n, i, !0), Y.access(e, r, (t || 0) + 1);
//       },
//       teardown: function teardown() {
//         var e = this.ownerDocument || this.document || this,
//             t = Y.access(e, r) - 1;
//         t ? Y.access(e, r, t) : (e.removeEventListener(n, i, !0), Y.remove(e, r));
//       }
//     };
//   });
//   var Tt = C.location,
//       Ct = {
//     guid: Date.now()
//   },
//       Et = /\?/;

//   S.parseXML = function (e) {
//     var t;
//     if (!e || "string" != typeof e) return null;

//     try {
//       t = new C.DOMParser().parseFromString(e, "text/xml");
//     } catch (e) {
//       t = void 0;
//     }

//     return t && !t.getElementsByTagName("parsererror").length || S.error("Invalid XML: " + e), t;
//   };

//   var St = /\[\]$/,
//       kt = /\r?\n/g,
//       At = /^(?:submit|button|image|reset|file)$/i,
//       Nt = /^(?:input|select|textarea|keygen)/i;

//   function Dt(n, e, r, i) {
//     var t;
//     if (Array.isArray(e)) S.each(e, function (e, t) {
//       r || St.test(n) ? i(n, t) : Dt(n + "[" + ("object" == _typeof(t) && null != t ? e : "") + "]", t, r, i);
//     });else if (r || "object" !== w(e)) i(n, e);else for (t in e) {
//       Dt(n + "[" + t + "]", e[t], r, i);
//     }
//   }

//   S.param = function (e, t) {
//     var n,
//         r = [],
//         i = function i(e, t) {
//       var n = m(t) ? t() : t;
//       r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n);
//     };

//     if (null == e) return "";
//     if (Array.isArray(e) || e.jquery && !S.isPlainObject(e)) S.each(e, function () {
//       i(this.name, this.value);
//     });else for (n in e) {
//       Dt(n, e[n], t, i);
//     }
//     return r.join("&");
//   }, S.fn.extend({
//     serialize: function serialize() {
//       return S.param(this.serializeArray());
//     },
//     serializeArray: function serializeArray() {
//       return this.map(function () {
//         var e = S.prop(this, "elements");
//         return e ? S.makeArray(e) : this;
//       }).filter(function () {
//         var e = this.type;
//         return this.name && !S(this).is(":disabled") && Nt.test(this.nodeName) && !At.test(e) && (this.checked || !pe.test(e));
//       }).map(function (e, t) {
//         var n = S(this).val();
//         return null == n ? null : Array.isArray(n) ? S.map(n, function (e) {
//           return {
//             name: t.name,
//             value: e.replace(kt, "\r\n")
//           };
//         }) : {
//           name: t.name,
//           value: n.replace(kt, "\r\n")
//         };
//       }).get();
//     }
//   });
//   var jt = /%20/g,
//       qt = /#.*$/,
//       Lt = /([?&])_=[^&]*/,
//       Ht = /^(.*?):[ \t]*([^\r\n]*)$/gm,
//       Ot = /^(?:GET|HEAD)$/,
//       Pt = /^\/\//,
//       Rt = {},
//       Mt = {},
//       It = "*/".concat("*"),
//       Wt = E.createElement("a");

//   function Ft(o) {
//     return function (e, t) {
//       "string" != typeof e && (t = e, e = "*");
//       var n,
//           r = 0,
//           i = e.toLowerCase().match(P) || [];
//       if (m(t)) while (n = i[r++]) {
//         "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t);
//       }
//     };
//   }

//   function Bt(t, i, o, a) {
//     var s = {},
//         u = t === Mt;

//     function l(e) {
//       var r;
//       return s[e] = !0, S.each(t[e] || [], function (e, t) {
//         var n = t(i, o, a);
//         return "string" != typeof n || u || s[n] ? u ? !(r = n) : void 0 : (i.dataTypes.unshift(n), l(n), !1);
//       }), r;
//     }

//     return l(i.dataTypes[0]) || !s["*"] && l("*");
//   }

//   function $t(e, t) {
//     var n,
//         r,
//         i = S.ajaxSettings.flatOptions || {};

//     for (n in t) {
//       void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
//     }

//     return r && S.extend(!0, e, r), e;
//   }

//   Wt.href = Tt.href, S.extend({
//     active: 0,
//     lastModified: {},
//     etag: {},
//     ajaxSettings: {
//       url: Tt.href,
//       type: "GET",
//       isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Tt.protocol),
//       global: !0,
//       processData: !0,
//       async: !0,
//       contentType: "application/x-www-form-urlencoded; charset=UTF-8",
//       accepts: {
//         "*": It,
//         text: "text/plain",
//         html: "text/html",
//         xml: "application/xml, text/xml",
//         json: "application/json, text/javascript"
//       },
//       contents: {
//         xml: /\bxml\b/,
//         html: /\bhtml/,
//         json: /\bjson\b/
//       },
//       responseFields: {
//         xml: "responseXML",
//         text: "responseText",
//         json: "responseJSON"
//       },
//       converters: {
//         "* text": String,
//         "text html": !0,
//         "text json": JSON.parse,
//         "text xml": S.parseXML
//       },
//       flatOptions: {
//         url: !0,
//         context: !0
//       }
//     },
//     ajaxSetup: function ajaxSetup(e, t) {
//       return t ? $t($t(e, S.ajaxSettings), t) : $t(S.ajaxSettings, e);
//     },
//     ajaxPrefilter: Ft(Rt),
//     ajaxTransport: Ft(Mt),
//     ajax: function ajax(e, t) {
//       "object" == _typeof(e) && (t = e, e = void 0), t = t || {};
//       var c,
//           f,
//           p,
//           n,
//           d,
//           r,
//           h,
//           g,
//           i,
//           o,
//           v = S.ajaxSetup({}, t),
//           y = v.context || v,
//           m = v.context && (y.nodeType || y.jquery) ? S(y) : S.event,
//           x = S.Deferred(),
//           b = S.Callbacks("once memory"),
//           w = v.statusCode || {},
//           a = {},
//           s = {},
//           u = "canceled",
//           T = {
//         readyState: 0,
//         getResponseHeader: function getResponseHeader(e) {
//           var t;

//           if (h) {
//             if (!n) {
//               n = {};

//               while (t = Ht.exec(p)) {
//                 n[t[1].toLowerCase() + " "] = (n[t[1].toLowerCase() + " "] || []).concat(t[2]);
//               }
//             }

//             t = n[e.toLowerCase() + " "];
//           }

//           return null == t ? null : t.join(", ");
//         },
//         getAllResponseHeaders: function getAllResponseHeaders() {
//           return h ? p : null;
//         },
//         setRequestHeader: function setRequestHeader(e, t) {
//           return null == h && (e = s[e.toLowerCase()] = s[e.toLowerCase()] || e, a[e] = t), this;
//         },
//         overrideMimeType: function overrideMimeType(e) {
//           return null == h && (v.mimeType = e), this;
//         },
//         statusCode: function statusCode(e) {
//           var t;
//           if (e) if (h) T.always(e[T.status]);else for (t in e) {
//             w[t] = [w[t], e[t]];
//           }
//           return this;
//         },
//         abort: function abort(e) {
//           var t = e || u;
//           return c && c.abort(t), l(0, t), this;
//         }
//       };

//       if (x.promise(T), v.url = ((e || v.url || Tt.href) + "").replace(Pt, Tt.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(P) || [""], null == v.crossDomain) {
//         r = E.createElement("a");

//         try {
//           r.href = v.url, r.href = r.href, v.crossDomain = Wt.protocol + "//" + Wt.host != r.protocol + "//" + r.host;
//         } catch (e) {
//           v.crossDomain = !0;
//         }
//       }

//       if (v.data && v.processData && "string" != typeof v.data && (v.data = S.param(v.data, v.traditional)), Bt(Rt, v, t, T), h) return T;

//       for (i in (g = S.event && v.global) && 0 == S.active++ && S.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !Ot.test(v.type), f = v.url.replace(qt, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(jt, "+")) : (o = v.url.slice(f.length), v.data && (v.processData || "string" == typeof v.data) && (f += (Et.test(f) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (f = f.replace(Lt, "$1"), o = (Et.test(f) ? "&" : "?") + "_=" + Ct.guid++ + o), v.url = f + o), v.ifModified && (S.lastModified[f] && T.setRequestHeader("If-Modified-Since", S.lastModified[f]), S.etag[f] && T.setRequestHeader("If-None-Match", S.etag[f])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && T.setRequestHeader("Content-Type", v.contentType), T.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + It + "; q=0.01" : "") : v.accepts["*"]), v.headers) {
//         T.setRequestHeader(i, v.headers[i]);
//       }

//       if (v.beforeSend && (!1 === v.beforeSend.call(y, T, v) || h)) return T.abort();

//       if (u = "abort", b.add(v.complete), T.done(v.success), T.fail(v.error), c = Bt(Mt, v, t, T)) {
//         if (T.readyState = 1, g && m.trigger("ajaxSend", [T, v]), h) return T;
//         v.async && 0 < v.timeout && (d = C.setTimeout(function () {
//           T.abort("timeout");
//         }, v.timeout));

//         try {
//           h = !1, c.send(a, l);
//         } catch (e) {
//           if (h) throw e;
//           l(-1, e);
//         }
//       } else l(-1, "No Transport");

//       function l(e, t, n, r) {
//         var i,
//             o,
//             a,
//             s,
//             u,
//             l = t;
//         h || (h = !0, d && C.clearTimeout(d), c = void 0, p = r || "", T.readyState = 0 < e ? 4 : 0, i = 200 <= e && e < 300 || 304 === e, n && (s = function (e, t, n) {
//           var r,
//               i,
//               o,
//               a,
//               s = e.contents,
//               u = e.dataTypes;

//           while ("*" === u[0]) {
//             u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
//           }

//           if (r) for (i in s) {
//             if (s[i] && s[i].test(r)) {
//               u.unshift(i);
//               break;
//             }
//           }
//           if (u[0] in n) o = u[0];else {
//             for (i in n) {
//               if (!u[0] || e.converters[i + " " + u[0]]) {
//                 o = i;
//                 break;
//               }

//               a || (a = i);
//             }

//             o = o || a;
//           }
//           if (o) return o !== u[0] && u.unshift(o), n[o];
//         }(v, T, n)), !i && -1 < S.inArray("script", v.dataTypes) && (v.converters["text script"] = function () {}), s = function (e, t, n, r) {
//           var i,
//               o,
//               a,
//               s,
//               u,
//               l = {},
//               c = e.dataTypes.slice();
//           if (c[1]) for (a in e.converters) {
//             l[a.toLowerCase()] = e.converters[a];
//           }
//           o = c.shift();

//           while (o) {
//             if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift()) if ("*" === o) o = u;else if ("*" !== u && u !== o) {
//               if (!(a = l[u + " " + o] || l["* " + o])) for (i in l) {
//                 if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
//                   !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
//                   break;
//                 }
//               }
//               if (!0 !== a) if (a && e["throws"]) t = a(t);else try {
//                 t = a(t);
//               } catch (e) {
//                 return {
//                   state: "parsererror",
//                   error: a ? e : "No conversion from " + u + " to " + o
//                 };
//               }
//             }
//           }

//           return {
//             state: "success",
//             data: t
//           };
//         }(v, s, T, i), i ? (v.ifModified && ((u = T.getResponseHeader("Last-Modified")) && (S.lastModified[f] = u), (u = T.getResponseHeader("etag")) && (S.etag[f] = u)), 204 === e || "HEAD" === v.type ? l = "nocontent" : 304 === e ? l = "notmodified" : (l = s.state, o = s.data, i = !(a = s.error))) : (a = l, !e && l || (l = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || l) + "", i ? x.resolveWith(y, [o, l, T]) : x.rejectWith(y, [T, l, a]), T.statusCode(w), w = void 0, g && m.trigger(i ? "ajaxSuccess" : "ajaxError", [T, v, i ? o : a]), b.fireWith(y, [T, l]), g && (m.trigger("ajaxComplete", [T, v]), --S.active || S.event.trigger("ajaxStop")));
//       }

//       return T;
//     },
//     getJSON: function getJSON(e, t, n) {
//       return S.get(e, t, n, "json");
//     },
//     getScript: function getScript(e, t) {
//       return S.get(e, void 0, t, "script");
//     }
//   }), S.each(["get", "post"], function (e, i) {
//     S[i] = function (e, t, n, r) {
//       return m(t) && (r = r || n, n = t, t = void 0), S.ajax(S.extend({
//         url: e,
//         type: i,
//         dataType: r,
//         data: t,
//         success: n
//       }, S.isPlainObject(e) && e));
//     };
//   }), S.ajaxPrefilter(function (e) {
//     var t;

//     for (t in e.headers) {
//       "content-type" === t.toLowerCase() && (e.contentType = e.headers[t] || "");
//     }
//   }), S._evalUrl = function (e, t, n) {
//     return S.ajax({
//       url: e,
//       type: "GET",
//       dataType: "script",
//       cache: !0,
//       async: !1,
//       global: !1,
//       converters: {
//         "text script": function textScript() {}
//       },
//       dataFilter: function dataFilter(e) {
//         S.globalEval(e, t, n);
//       }
//     });
//   }, S.fn.extend({
//     wrapAll: function wrapAll(e) {
//       var t;
//       return this[0] && (m(e) && (e = e.call(this[0])), t = S(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
//         var e = this;

//         while (e.firstElementChild) {
//           e = e.firstElementChild;
//         }

//         return e;
//       }).append(this)), this;
//     },
//     wrapInner: function wrapInner(n) {
//       return m(n) ? this.each(function (e) {
//         S(this).wrapInner(n.call(this, e));
//       }) : this.each(function () {
//         var e = S(this),
//             t = e.contents();
//         t.length ? t.wrapAll(n) : e.append(n);
//       });
//     },
//     wrap: function wrap(t) {
//       var n = m(t);
//       return this.each(function (e) {
//         S(this).wrapAll(n ? t.call(this, e) : t);
//       });
//     },
//     unwrap: function unwrap(e) {
//       return this.parent(e).not("body").each(function () {
//         S(this).replaceWith(this.childNodes);
//       }), this;
//     }
//   }), S.expr.pseudos.hidden = function (e) {
//     return !S.expr.pseudos.visible(e);
//   }, S.expr.pseudos.visible = function (e) {
//     return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length);
//   }, S.ajaxSettings.xhr = function () {
//     try {
//       return new C.XMLHttpRequest();
//     } catch (e) {}
//   };
//   var _t = {
//     0: 200,
//     1223: 204
//   },
//       zt = S.ajaxSettings.xhr();
//   y.cors = !!zt && "withCredentials" in zt, y.ajax = zt = !!zt, S.ajaxTransport(function (i) {
//     var _o, a;

//     if (y.cors || zt && !i.crossDomain) return {
//       send: function send(e, t) {
//         var n,
//             r = i.xhr();
//         if (r.open(i.type, i.url, i.async, i.username, i.password), i.xhrFields) for (n in i.xhrFields) {
//           r[n] = i.xhrFields[n];
//         }

//         for (n in i.mimeType && r.overrideMimeType && r.overrideMimeType(i.mimeType), i.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) {
//           r.setRequestHeader(n, e[n]);
//         }

//         _o = function o(e) {
//           return function () {
//             _o && (_o = a = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null, "abort" === e ? r.abort() : "error" === e ? "number" != typeof r.status ? t(0, "error") : t(r.status, r.statusText) : t(_t[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? {
//               binary: r.response
//             } : {
//               text: r.responseText
//             }, r.getAllResponseHeaders()));
//           };
//         }, r.onload = _o(), a = r.onerror = r.ontimeout = _o("error"), void 0 !== r.onabort ? r.onabort = a : r.onreadystatechange = function () {
//           4 === r.readyState && C.setTimeout(function () {
//             _o && a();
//           });
//         }, _o = _o("abort");

//         try {
//           r.send(i.hasContent && i.data || null);
//         } catch (e) {
//           if (_o) throw e;
//         }
//       },
//       abort: function abort() {
//         _o && _o();
//       }
//     };
//   }), S.ajaxPrefilter(function (e) {
//     e.crossDomain && (e.contents.script = !1);
//   }), S.ajaxSetup({
//     accepts: {
//       script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
//     },
//     contents: {
//       script: /\b(?:java|ecma)script\b/
//     },
//     converters: {
//       "text script": function textScript(e) {
//         return S.globalEval(e), e;
//       }
//     }
//   }), S.ajaxPrefilter("script", function (e) {
//     void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
//   }), S.ajaxTransport("script", function (n) {
//     var r, _i;

//     if (n.crossDomain || n.scriptAttrs) return {
//       send: function send(e, t) {
//         r = S("<script>").attr(n.scriptAttrs || {}).prop({
//           charset: n.scriptCharset,
//           src: n.url
//         }).on("load error", _i = function i(e) {
//           r.remove(), _i = null, e && t("error" === e.type ? 404 : 200, e.type);
//         }), E.head.appendChild(r[0]);
//       },
//       abort: function abort() {
//         _i && _i();
//       }
//     };
//   });
//   var Ut,
//       Xt = [],
//       Vt = /(=)\?(?=&|$)|\?\?/;
//   S.ajaxSetup({
//     jsonp: "callback",
//     jsonpCallback: function jsonpCallback() {
//       var e = Xt.pop() || S.expando + "_" + Ct.guid++;
//       return this[e] = !0, e;
//     }
//   }), S.ajaxPrefilter("json jsonp", function (e, t, n) {
//     var r,
//         i,
//         o,
//         a = !1 !== e.jsonp && (Vt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Vt.test(e.data) && "data");
//     if (a || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = m(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Vt, "$1" + r) : !1 !== e.jsonp && (e.url += (Et.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function () {
//       return o || S.error(r + " was not called"), o[0];
//     }, e.dataTypes[0] = "json", i = C[r], C[r] = function () {
//       o = arguments;
//     }, n.always(function () {
//       void 0 === i ? S(C).removeProp(r) : C[r] = i, e[r] && (e.jsonpCallback = t.jsonpCallback, Xt.push(r)), o && m(i) && i(o[0]), o = i = void 0;
//     }), "script";
//   }), y.createHTMLDocument = ((Ut = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Ut.childNodes.length), S.parseHTML = function (e, t, n) {
//     return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (y.createHTMLDocument ? ((r = (t = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, t.head.appendChild(r)) : t = E), o = !n && [], (i = N.exec(e)) ? [t.createElement(i[1])] : (i = xe([e], t, o), o && o.length && S(o).remove(), S.merge([], i.childNodes)));
//     var r, i, o;
//   }, S.fn.load = function (e, t, n) {
//     var r,
//         i,
//         o,
//         a = this,
//         s = e.indexOf(" ");
//     return -1 < s && (r = vt(e.slice(s)), e = e.slice(0, s)), m(t) ? (n = t, t = void 0) : t && "object" == _typeof(t) && (i = "POST"), 0 < a.length && S.ajax({
//       url: e,
//       type: i || "GET",
//       dataType: "html",
//       data: t
//     }).done(function (e) {
//       o = arguments, a.html(r ? S("<div>").append(S.parseHTML(e)).find(r) : e);
//     }).always(n && function (e, t) {
//       a.each(function () {
//         n.apply(this, o || [e.responseText, t, e]);
//       });
//     }), this;
//   }, S.expr.pseudos.animated = function (t) {
//     return S.grep(S.timers, function (e) {
//       return t === e.elem;
//     }).length;
//   }, S.offset = {
//     setOffset: function setOffset(e, t, n) {
//       var r,
//           i,
//           o,
//           a,
//           s,
//           u,
//           l = S.css(e, "position"),
//           c = S(e),
//           f = {};
//       "static" === l && (e.style.position = "relative"), s = c.offset(), o = S.css(e, "top"), u = S.css(e, "left"), ("absolute" === l || "fixed" === l) && -1 < (o + u).indexOf("auto") ? (a = (r = c.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), m(t) && (t = t.call(e, n, S.extend({}, s))), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : ("number" == typeof f.top && (f.top += "px"), "number" == typeof f.left && (f.left += "px"), c.css(f));
//     }
//   }, S.fn.extend({
//     offset: function offset(t) {
//       if (arguments.length) return void 0 === t ? this : this.each(function (e) {
//         S.offset.setOffset(this, t, e);
//       });
//       var e,
//           n,
//           r = this[0];
//       return r ? r.getClientRects().length ? (e = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
//         top: e.top + n.pageYOffset,
//         left: e.left + n.pageXOffset
//       }) : {
//         top: 0,
//         left: 0
//       } : void 0;
//     },
//     position: function position() {
//       if (this[0]) {
//         var e,
//             t,
//             n,
//             r = this[0],
//             i = {
//           top: 0,
//           left: 0
//         };
//         if ("fixed" === S.css(r, "position")) t = r.getBoundingClientRect();else {
//           t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement;

//           while (e && (e === n.body || e === n.documentElement) && "static" === S.css(e, "position")) {
//             e = e.parentNode;
//           }

//           e && e !== r && 1 === e.nodeType && ((i = S(e).offset()).top += S.css(e, "borderTopWidth", !0), i.left += S.css(e, "borderLeftWidth", !0));
//         }
//         return {
//           top: t.top - i.top - S.css(r, "marginTop", !0),
//           left: t.left - i.left - S.css(r, "marginLeft", !0)
//         };
//       }
//     },
//     offsetParent: function offsetParent() {
//       return this.map(function () {
//         var e = this.offsetParent;

//         while (e && "static" === S.css(e, "position")) {
//           e = e.offsetParent;
//         }

//         return e || re;
//       });
//     }
//   }), S.each({
//     scrollLeft: "pageXOffset",
//     scrollTop: "pageYOffset"
//   }, function (t, i) {
//     var o = "pageYOffset" === i;

//     S.fn[t] = function (e) {
//       return $(this, function (e, t, n) {
//         var r;
//         if (x(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === n) return r ? r[i] : e[t];
//         r ? r.scrollTo(o ? r.pageXOffset : n, o ? n : r.pageYOffset) : e[t] = n;
//       }, t, e, arguments.length);
//     };
//   }), S.each(["top", "left"], function (e, n) {
//     S.cssHooks[n] = $e(y.pixelPosition, function (e, t) {
//       if (t) return t = Be(e, n), Me.test(t) ? S(e).position()[n] + "px" : t;
//     });
//   }), S.each({
//     Height: "height",
//     Width: "width"
//   }, function (a, s) {
//     S.each({
//       padding: "inner" + a,
//       content: s,
//       "": "outer" + a
//     }, function (r, o) {
//       S.fn[o] = function (e, t) {
//         var n = arguments.length && (r || "boolean" != typeof e),
//             i = r || (!0 === e || !0 === t ? "margin" : "border");
//         return $(this, function (e, t, n) {
//           var r;
//           return x(e) ? 0 === o.indexOf("outer") ? e["inner" + a] : e.document.documentElement["client" + a] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + a], r["scroll" + a], e.body["offset" + a], r["offset" + a], r["client" + a])) : void 0 === n ? S.css(e, t, i) : S.style(e, t, n, i);
//         }, s, n ? e : void 0, n);
//       };
//     });
//   }), S.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
//     S.fn[t] = function (e) {
//       return this.on(t, e);
//     };
//   }), S.fn.extend({
//     bind: function bind(e, t, n) {
//       return this.on(e, null, t, n);
//     },
//     unbind: function unbind(e, t) {
//       return this.off(e, null, t);
//     },
//     delegate: function delegate(e, t, n, r) {
//       return this.on(t, e, n, r);
//     },
//     undelegate: function undelegate(e, t, n) {
//       return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n);
//     },
//     hover: function hover(e, t) {
//       return this.mouseenter(e).mouseleave(t || e);
//     }
//   }), S.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
//     S.fn[n] = function (e, t) {
//       return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n);
//     };
//   });
//   var Gt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
//   S.proxy = function (e, t) {
//     var n, r, i;
//     if ("string" == typeof t && (n = e[t], t = e, e = n), m(e)) return r = s.call(arguments, 2), (i = function i() {
//       return e.apply(t || this, r.concat(s.call(arguments)));
//     }).guid = e.guid = e.guid || S.guid++, i;
//   }, S.holdReady = function (e) {
//     e ? S.readyWait++ : S.ready(!0);
//   }, S.isArray = Array.isArray, S.parseJSON = JSON.parse, S.nodeName = A, S.isFunction = m, S.isWindow = x, S.camelCase = X, S.type = w, S.now = Date.now, S.isNumeric = function (e) {
//     var t = S.type(e);
//     return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e));
//   }, S.trim = function (e) {
//     return null == e ? "" : (e + "").replace(Gt, "");
//   }, "function" == typeof define && define.amd && define("jquery", [], function () {
//     return S;
//   });
//   var Yt = C.jQuery,
//       Qt = C.$;
//   return S.noConflict = function (e) {
//     return C.$ === S && (C.$ = Qt), e && C.jQuery === S && (C.jQuery = Yt), S;
//   }, "undefined" == typeof e && (C.jQuery = C.$ = S), S;
// });
// /*! Magnific Popup - v1.1.0 - 2016-02-20
// * http://dimsemenov.com/plugins/magnific-popup/
// * Copyright (c) 2016 Dmitry Semenov; */

// !function (a) {
//   "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? require("jquery") : window.jQuery || window.Zepto);
// }(function (a) {
//   var b,
//       c,
//       d,
//       e,
//       f,
//       g,
//       h = "Close",
//       i = "BeforeClose",
//       j = "AfterClose",
//       k = "BeforeAppend",
//       l = "MarkupParse",
//       m = "Open",
//       n = "Change",
//       o = "mfp",
//       p = "." + o,
//       q = "mfp-ready",
//       r = "mfp-removing",
//       s = "mfp-prevent-close",
//       t = function t() {},
//       u = !!window.jQuery,
//       v = a(window),
//       w = function w(a, c) {
//     b.ev.on(o + a + p, c);
//   },
//       x = function x(b, c, d, e) {
//     var f = document.createElement("div");
//     return f.className = "mfp-" + b, d && (f.innerHTML = d), e ? c && c.appendChild(f) : (f = a(f), c && f.appendTo(c)), f;
//   },
//       y = function y(c, d) {
//     b.ev.triggerHandler(o + c, d), b.st.callbacks && (c = c.charAt(0).toLowerCase() + c.slice(1), b.st.callbacks[c] && b.st.callbacks[c].apply(b, a.isArray(d) ? d : [d]));
//   },
//       z = function z(c) {
//     return c === g && b.currTemplate.closeBtn || (b.currTemplate.closeBtn = a(b.st.closeMarkup.replace("%title%", b.st.tClose)), g = c), b.currTemplate.closeBtn;
//   },
//       A = function A() {
//     a.magnificPopup.instance || (b = new t(), b.init(), a.magnificPopup.instance = b);
//   },
//       B = function B() {
//     var a = document.createElement("p").style,
//         b = ["ms", "O", "Moz", "Webkit"];
//     if (void 0 !== a.transition) return !0;

//     for (; b.length;) {
//       if (b.pop() + "Transition" in a) return !0;
//     }

//     return !1;
//   };

//   t.prototype = {
//     constructor: t,
//     init: function init() {
//       var c = navigator.appVersion;
//       b.isLowIE = b.isIE8 = document.all && !document.addEventListener, b.isAndroid = /android/gi.test(c), b.isIOS = /iphone|ipad|ipod/gi.test(c), b.supportsTransition = B(), b.probablyMobile = b.isAndroid || b.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), d = a(document), b.popupsCache = {};
//     },
//     open: function open(c) {
//       var e;

//       if (c.isObj === !1) {
//         b.items = c.items.toArray(), b.index = 0;
//         var g,
//             h = c.items;

//         for (e = 0; e < h.length; e++) {
//           if (g = h[e], g.parsed && (g = g.el[0]), g === c.el[0]) {
//             b.index = e;
//             break;
//           }
//         }
//       } else b.items = a.isArray(c.items) ? c.items : [c.items], b.index = c.index || 0;

//       if (b.isOpen) return void b.updateItemHTML();
//       b.types = [], f = "", c.mainEl && c.mainEl.length ? b.ev = c.mainEl.eq(0) : b.ev = d, c.key ? (b.popupsCache[c.key] || (b.popupsCache[c.key] = {}), b.currTemplate = b.popupsCache[c.key]) : b.currTemplate = {}, b.st = a.extend(!0, {}, a.magnificPopup.defaults, c), b.fixedContentPos = "auto" === b.st.fixedContentPos ? !b.probablyMobile : b.st.fixedContentPos, b.st.modal && (b.st.closeOnContentClick = !1, b.st.closeOnBgClick = !1, b.st.showCloseBtn = !1, b.st.enableEscapeKey = !1), b.bgOverlay || (b.bgOverlay = x("bg").on("click" + p, function () {
//         b.close();
//       }), b.wrap = x("wrap").attr("tabindex", -1).on("click" + p, function (a) {
//         b._checkIfClose(a.target) && b.close();
//       }), b.container = x("container", b.wrap)), b.contentContainer = x("content"), b.st.preloader && (b.preloader = x("preloader", b.container, b.st.tLoading));
//       var i = a.magnificPopup.modules;

//       for (e = 0; e < i.length; e++) {
//         var j = i[e];
//         j = j.charAt(0).toUpperCase() + j.slice(1), b["init" + j].call(b);
//       }

//       y("BeforeOpen"), b.st.showCloseBtn && (b.st.closeBtnInside ? (w(l, function (a, b, c, d) {
//         c.close_replaceWith = z(d.type);
//       }), f += " mfp-close-btn-in") : b.wrap.append(z())), b.st.alignTop && (f += " mfp-align-top"), b.fixedContentPos ? b.wrap.css({
//         overflow: b.st.overflowY,
//         overflowX: "hidden",
//         overflowY: b.st.overflowY
//       }) : b.wrap.css({
//         top: v.scrollTop(),
//         position: "absolute"
//       }), (b.st.fixedBgPos === !1 || "auto" === b.st.fixedBgPos && !b.fixedContentPos) && b.bgOverlay.css({
//         height: d.height(),
//         position: "absolute"
//       }), b.st.enableEscapeKey && d.on("keyup" + p, function (a) {
//         27 === a.keyCode && b.close();
//       }), v.on("resize" + p, function () {
//         b.updateSize();
//       }), b.st.closeOnContentClick || (f += " mfp-auto-cursor"), f && b.wrap.addClass(f);
//       var k = b.wH = v.height(),
//           n = {};

//       if (b.fixedContentPos && b._hasScrollBar(k)) {
//         var o = b._getScrollbarSize();

//         o && (n.marginRight = o);
//       }

//       b.fixedContentPos && (b.isIE7 ? a("body, html").css("overflow", "hidden") : n.overflow = "hidden");
//       var r = b.st.mainClass;
//       return b.isIE7 && (r += " mfp-ie7"), r && b._addClassToMFP(r), b.updateItemHTML(), y("BuildControls"), a("html").css(n), b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo || a(document.body)), b._lastFocusedEl = document.activeElement, setTimeout(function () {
//         b.content ? (b._addClassToMFP(q), b._setFocus()) : b.bgOverlay.addClass(q), d.on("focusin" + p, b._onFocusIn);
//       }, 16), b.isOpen = !0, b.updateSize(k), y(m), c;
//     },
//     close: function close() {
//       b.isOpen && (y(i), b.isOpen = !1, b.st.removalDelay && !b.isLowIE && b.supportsTransition ? (b._addClassToMFP(r), setTimeout(function () {
//         b._close();
//       }, b.st.removalDelay)) : b._close());
//     },
//     _close: function _close() {
//       y(h);
//       var c = r + " " + q + " ";

//       if (b.bgOverlay.detach(), b.wrap.detach(), b.container.empty(), b.st.mainClass && (c += b.st.mainClass + " "), b._removeClassFromMFP(c), b.fixedContentPos) {
//         var e = {
//           marginRight: ""
//         };
//         b.isIE7 ? a("body, html").css("overflow", "") : e.overflow = "", a("html").css(e);
//       }

//       d.off("keyup" + p + " focusin" + p), b.ev.off(p), b.wrap.attr("class", "mfp-wrap").removeAttr("style"), b.bgOverlay.attr("class", "mfp-bg"), b.container.attr("class", "mfp-container"), !b.st.showCloseBtn || b.st.closeBtnInside && b.currTemplate[b.currItem.type] !== !0 || b.currTemplate.closeBtn && b.currTemplate.closeBtn.detach(), b.st.autoFocusLast && b._lastFocusedEl && a(b._lastFocusedEl).focus(), b.currItem = null, b.content = null, b.currTemplate = null, b.prevHeight = 0, y(j);
//     },
//     updateSize: function updateSize(a) {
//       if (b.isIOS) {
//         var c = document.documentElement.clientWidth / window.innerWidth,
//             d = window.innerHeight * c;
//         b.wrap.css("height", d), b.wH = d;
//       } else b.wH = a || v.height();

//       b.fixedContentPos || b.wrap.css("height", b.wH), y("Resize");
//     },
//     updateItemHTML: function updateItemHTML() {
//       var c = b.items[b.index];
//       b.contentContainer.detach(), b.content && b.content.detach(), c.parsed || (c = b.parseEl(b.index));
//       var d = c.type;

//       if (y("BeforeChange", [b.currItem ? b.currItem.type : "", d]), b.currItem = c, !b.currTemplate[d]) {
//         var f = b.st[d] ? b.st[d].markup : !1;
//         y("FirstMarkupParse", f), f ? b.currTemplate[d] = a(f) : b.currTemplate[d] = !0;
//       }

//       e && e !== c.type && b.container.removeClass("mfp-" + e + "-holder");
//       var g = b["get" + d.charAt(0).toUpperCase() + d.slice(1)](c, b.currTemplate[d]);
//       b.appendContent(g, d), c.preloaded = !0, y(n, c), e = c.type, b.container.prepend(b.contentContainer), y("AfterChange");
//     },
//     appendContent: function appendContent(a, c) {
//       b.content = a, a ? b.st.showCloseBtn && b.st.closeBtnInside && b.currTemplate[c] === !0 ? b.content.find(".mfp-close").length || b.content.append(z()) : b.content = a : b.content = "", y(k), b.container.addClass("mfp-" + c + "-holder"), b.contentContainer.append(b.content);
//     },
//     parseEl: function parseEl(c) {
//       var d,
//           e = b.items[c];

//       if (e.tagName ? e = {
//         el: a(e)
//       } : (d = e.type, e = {
//         data: e,
//         src: e.src
//       }), e.el) {
//         for (var f = b.types, g = 0; g < f.length; g++) {
//           if (e.el.hasClass("mfp-" + f[g])) {
//             d = f[g];
//             break;
//           }
//         }

//         e.src = e.el.attr("data-mfp-src"), e.src || (e.src = e.el.attr("href"));
//       }

//       return e.type = d || b.st.type || "inline", e.index = c, e.parsed = !0, b.items[c] = e, y("ElementParse", e), b.items[c];
//     },
//     addGroup: function addGroup(a, c) {
//       var d = function d(_d) {
//         _d.mfpEl = this, b._openClick(_d, a, c);
//       };

//       c || (c = {});
//       var e = "click.magnificPopup";
//       c.mainEl = a, c.items ? (c.isObj = !0, a.off(e).on(e, d)) : (c.isObj = !1, c.delegate ? a.off(e).on(e, c.delegate, d) : (c.items = a, a.off(e).on(e, d)));
//     },
//     _openClick: function _openClick(c, d, e) {
//       var f = void 0 !== e.midClick ? e.midClick : a.magnificPopup.defaults.midClick;

//       if (f || !(2 === c.which || c.ctrlKey || c.metaKey || c.altKey || c.shiftKey)) {
//         var g = void 0 !== e.disableOn ? e.disableOn : a.magnificPopup.defaults.disableOn;
//         if (g) if (a.isFunction(g)) {
//           if (!g.call(b)) return !0;
//         } else if (v.width() < g) return !0;
//         c.type && (c.preventDefault(), b.isOpen && c.stopPropagation()), e.el = a(c.mfpEl), e.delegate && (e.items = d.find(e.delegate)), b.open(e);
//       }
//     },
//     updateStatus: function updateStatus(a, d) {
//       if (b.preloader) {
//         c !== a && b.container.removeClass("mfp-s-" + c), d || "loading" !== a || (d = b.st.tLoading);
//         var e = {
//           status: a,
//           text: d
//         };
//         y("UpdateStatus", e), a = e.status, d = e.text, b.preloader.html(d), b.preloader.find("a").on("click", function (a) {
//           a.stopImmediatePropagation();
//         }), b.container.addClass("mfp-s-" + a), c = a;
//       }
//     },
//     _checkIfClose: function _checkIfClose(c) {
//       if (!a(c).hasClass(s)) {
//         var d = b.st.closeOnContentClick,
//             e = b.st.closeOnBgClick;
//         if (d && e) return !0;
//         if (!b.content || a(c).hasClass("mfp-close") || b.preloader && c === b.preloader[0]) return !0;

//         if (c === b.content[0] || a.contains(b.content[0], c)) {
//           if (d) return !0;
//         } else if (e && a.contains(document, c)) return !0;

//         return !1;
//       }
//     },
//     _addClassToMFP: function _addClassToMFP(a) {
//       b.bgOverlay.addClass(a), b.wrap.addClass(a);
//     },
//     _removeClassFromMFP: function _removeClassFromMFP(a) {
//       this.bgOverlay.removeClass(a), b.wrap.removeClass(a);
//     },
//     _hasScrollBar: function _hasScrollBar(a) {
//       return (b.isIE7 ? d.height() : document.body.scrollHeight) > (a || v.height());
//     },
//     _setFocus: function _setFocus() {
//       (b.st.focus ? b.content.find(b.st.focus).eq(0) : b.wrap).focus();
//     },
//     _onFocusIn: function _onFocusIn(c) {
//       return c.target === b.wrap[0] || a.contains(b.wrap[0], c.target) ? void 0 : (b._setFocus(), !1);
//     },
//     _parseMarkup: function _parseMarkup(b, c, d) {
//       var e;
//       d.data && (c = a.extend(d.data, c)), y(l, [b, c, d]), a.each(c, function (c, d) {
//         if (void 0 === d || d === !1) return !0;

//         if (e = c.split("_"), e.length > 1) {
//           var f = b.find(p + "-" + e[0]);

//           if (f.length > 0) {
//             var g = e[1];
//             "replaceWith" === g ? f[0] !== d[0] && f.replaceWith(d) : "img" === g ? f.is("img") ? f.attr("src", d) : f.replaceWith(a("<img>").attr("src", d).attr("class", f.attr("class"))) : f.attr(e[1], d);
//           }
//         } else b.find(p + "-" + c).html(d);
//       });
//     },
//     _getScrollbarSize: function _getScrollbarSize() {
//       if (void 0 === b.scrollbarSize) {
//         var a = document.createElement("div");
//         a.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(a), b.scrollbarSize = a.offsetWidth - a.clientWidth, document.body.removeChild(a);
//       }

//       return b.scrollbarSize;
//     }
//   }, a.magnificPopup = {
//     instance: null,
//     proto: t.prototype,
//     modules: [],
//     open: function open(b, c) {
//       return A(), b = b ? a.extend(!0, {}, b) : {}, b.isObj = !0, b.index = c || 0, this.instance.open(b);
//     },
//     close: function close() {
//       return a.magnificPopup.instance && a.magnificPopup.instance.close();
//     },
//     registerModule: function registerModule(b, c) {
//       c.options && (a.magnificPopup.defaults[b] = c.options), a.extend(this.proto, c.proto), this.modules.push(b);
//     },
//     defaults: {
//       disableOn: 0,
//       key: null,
//       midClick: !1,
//       mainClass: "",
//       preloader: !0,
//       focus: "",
//       closeOnContentClick: !1,
//       closeOnBgClick: !0,
//       closeBtnInside: !0,
//       showCloseBtn: !0,
//       enableEscapeKey: !0,
//       modal: !1,
//       alignTop: !1,
//       removalDelay: 0,
//       prependTo: null,
//       fixedContentPos: "auto",
//       fixedBgPos: "auto",
//       overflowY: "auto",
//       closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
//       tClose: "Close (Esc)",
//       tLoading: "Loading...",
//       autoFocusLast: !0
//     }
//   }, a.fn.magnificPopup = function (c) {
//     A();
//     var d = a(this);
//     if ("string" == typeof c) {
//       if ("open" === c) {
//         var e,
//             f = u ? d.data("magnificPopup") : d[0].magnificPopup,
//             g = parseInt(arguments[1], 10) || 0;
//         f.items ? e = f.items[g] : (e = d, f.delegate && (e = e.find(f.delegate)), e = e.eq(g)), b._openClick({
//           mfpEl: e
//         }, d, f);
//       } else b.isOpen && b[c].apply(b, Array.prototype.slice.call(arguments, 1));
//     } else c = a.extend(!0, {}, c), u ? d.data("magnificPopup", c) : d[0].magnificPopup = c, b.addGroup(d, c);
//     return d;
//   };

//   var C,
//       D,
//       E,
//       F = "inline",
//       G = function G() {
//     E && (D.after(E.addClass(C)).detach(), E = null);
//   };

//   a.magnificPopup.registerModule(F, {
//     options: {
//       hiddenClass: "hide",
//       markup: "",
//       tNotFound: "Content not found"
//     },
//     proto: {
//       initInline: function initInline() {
//         b.types.push(F), w(h + "." + F, function () {
//           G();
//         });
//       },
//       getInline: function getInline(c, d) {
//         if (G(), c.src) {
//           var e = b.st.inline,
//               f = a(c.src);

//           if (f.length) {
//             var g = f[0].parentNode;
//             g && g.tagName && (D || (C = e.hiddenClass, D = x(C), C = "mfp-" + C), E = f.after(D).detach().removeClass(C)), b.updateStatus("ready");
//           } else b.updateStatus("error", e.tNotFound), f = a("<div>");

//           return c.inlineElement = f, f;
//         }

//         return b.updateStatus("ready"), b._parseMarkup(d, {}, c), d;
//       }
//     }
//   });

//   var H,
//       I = "ajax",
//       J = function J() {
//     H && a(document.body).removeClass(H);
//   },
//       K = function K() {
//     J(), b.req && b.req.abort();
//   };

//   a.magnificPopup.registerModule(I, {
//     options: {
//       settings: null,
//       cursor: "mfp-ajax-cur",
//       tError: '<a href="%url%">The content</a> could not be loaded.'
//     },
//     proto: {
//       initAjax: function initAjax() {
//         b.types.push(I), H = b.st.ajax.cursor, w(h + "." + I, K), w("BeforeChange." + I, K);
//       },
//       getAjax: function getAjax(c) {
//         H && a(document.body).addClass(H), b.updateStatus("loading");
//         var d = a.extend({
//           url: c.src,
//           success: function success(d, e, f) {
//             var g = {
//               data: d,
//               xhr: f
//             };
//             y("ParseAjax", g), b.appendContent(a(g.data), I), c.finished = !0, J(), b._setFocus(), setTimeout(function () {
//               b.wrap.addClass(q);
//             }, 16), b.updateStatus("ready"), y("AjaxContentAdded");
//           },
//           error: function error() {
//             J(), c.finished = c.loadError = !0, b.updateStatus("error", b.st.ajax.tError.replace("%url%", c.src));
//           }
//         }, b.st.ajax.settings);
//         return b.req = a.ajax(d), "";
//       }
//     }
//   });

//   var L,
//       M = function M(c) {
//     if (c.data && void 0 !== c.data.title) return c.data.title;
//     var d = b.st.image.titleSrc;

//     if (d) {
//       if (a.isFunction(d)) return d.call(b, c);
//       if (c.el) return c.el.attr(d) || "";
//     }

//     return "";
//   };

//   a.magnificPopup.registerModule("image", {
//     options: {
//       markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
//       cursor: "mfp-zoom-out-cur",
//       titleSrc: "title",
//       verticalFit: !0,
//       tError: '<a href="%url%">The image</a> could not be loaded.'
//     },
//     proto: {
//       initImage: function initImage() {
//         var c = b.st.image,
//             d = ".image";
//         b.types.push("image"), w(m + d, function () {
//           "image" === b.currItem.type && c.cursor && a(document.body).addClass(c.cursor);
//         }), w(h + d, function () {
//           c.cursor && a(document.body).removeClass(c.cursor), v.off("resize" + p);
//         }), w("Resize" + d, b.resizeImage), b.isLowIE && w("AfterChange", b.resizeImage);
//       },
//       resizeImage: function resizeImage() {
//         var a = b.currItem;

//         if (a && a.img && b.st.image.verticalFit) {
//           var c = 0;
//           b.isLowIE && (c = parseInt(a.img.css("padding-top"), 10) + parseInt(a.img.css("padding-bottom"), 10)), a.img.css("max-height", b.wH - c);
//         }
//       },
//       _onImageHasSize: function _onImageHasSize(a) {
//         a.img && (a.hasSize = !0, L && clearInterval(L), a.isCheckingImgSize = !1, y("ImageHasSize", a), a.imgHidden && (b.content && b.content.removeClass("mfp-loading"), a.imgHidden = !1));
//       },
//       findImageSize: function findImageSize(a) {
//         var c = 0,
//             d = a.img[0],
//             e = function e(f) {
//           L && clearInterval(L), L = setInterval(function () {
//             return d.naturalWidth > 0 ? void b._onImageHasSize(a) : (c > 200 && clearInterval(L), c++, void (3 === c ? e(10) : 40 === c ? e(50) : 100 === c && e(500)));
//           }, f);
//         };

//         e(1);
//       },
//       getImage: function getImage(c, d) {
//         var e = 0,
//             f = function f() {
//           c && (c.img[0].complete ? (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("ready")), c.hasSize = !0, c.loaded = !0, y("ImageLoadComplete")) : (e++, 200 > e ? setTimeout(f, 100) : g()));
//         },
//             g = function g() {
//           c && (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("error", h.tError.replace("%url%", c.src))), c.hasSize = !0, c.loaded = !0, c.loadError = !0);
//         },
//             h = b.st.image,
//             i = d.find(".mfp-img");

//         if (i.length) {
//           var j = document.createElement("img");
//           j.className = "mfp-img", c.el && c.el.find("img").length && (j.alt = c.el.find("img").attr("alt")), c.img = a(j).on("load.mfploader", f).on("error.mfploader", g), j.src = c.src, i.is("img") && (c.img = c.img.clone()), j = c.img[0], j.naturalWidth > 0 ? c.hasSize = !0 : j.width || (c.hasSize = !1);
//         }

//         return b._parseMarkup(d, {
//           title: M(c),
//           img_replaceWith: c.img
//         }, c), b.resizeImage(), c.hasSize ? (L && clearInterval(L), c.loadError ? (d.addClass("mfp-loading"), b.updateStatus("error", h.tError.replace("%url%", c.src))) : (d.removeClass("mfp-loading"), b.updateStatus("ready")), d) : (b.updateStatus("loading"), c.loading = !0, c.hasSize || (c.imgHidden = !0, d.addClass("mfp-loading"), b.findImageSize(c)), d);
//       }
//     }
//   });

//   var N,
//       O = function O() {
//     return void 0 === N && (N = void 0 !== document.createElement("p").style.MozTransform), N;
//   };

//   a.magnificPopup.registerModule("zoom", {
//     options: {
//       enabled: !1,
//       easing: "ease-in-out",
//       duration: 300,
//       opener: function opener(a) {
//         return a.is("img") ? a : a.find("img");
//       }
//     },
//     proto: {
//       initZoom: function initZoom() {
//         var a,
//             c = b.st.zoom,
//             d = ".zoom";

//         if (c.enabled && b.supportsTransition) {
//           var e,
//               f,
//               g = c.duration,
//               j = function j(a) {
//             var b = a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
//                 d = "all " + c.duration / 1e3 + "s " + c.easing,
//                 e = {
//               position: "fixed",
//               zIndex: 9999,
//               left: 0,
//               top: 0,
//               "-webkit-backface-visibility": "hidden"
//             },
//                 f = "transition";
//             return e["-webkit-" + f] = e["-moz-" + f] = e["-o-" + f] = e[f] = d, b.css(e), b;
//           },
//               k = function k() {
//             b.content.css("visibility", "visible");
//           };

//           w("BuildControls" + d, function () {
//             if (b._allowZoom()) {
//               if (clearTimeout(e), b.content.css("visibility", "hidden"), a = b._getItemToZoom(), !a) return void k();
//               f = j(a), f.css(b._getOffset()), b.wrap.append(f), e = setTimeout(function () {
//                 f.css(b._getOffset(!0)), e = setTimeout(function () {
//                   k(), setTimeout(function () {
//                     f.remove(), a = f = null, y("ZoomAnimationEnded");
//                   }, 16);
//                 }, g);
//               }, 16);
//             }
//           }), w(i + d, function () {
//             if (b._allowZoom()) {
//               if (clearTimeout(e), b.st.removalDelay = g, !a) {
//                 if (a = b._getItemToZoom(), !a) return;
//                 f = j(a);
//               }

//               f.css(b._getOffset(!0)), b.wrap.append(f), b.content.css("visibility", "hidden"), setTimeout(function () {
//                 f.css(b._getOffset());
//               }, 16);
//             }
//           }), w(h + d, function () {
//             b._allowZoom() && (k(), f && f.remove(), a = null);
//           });
//         }
//       },
//       _allowZoom: function _allowZoom() {
//         return "image" === b.currItem.type;
//       },
//       _getItemToZoom: function _getItemToZoom() {
//         return b.currItem.hasSize ? b.currItem.img : !1;
//       },
//       _getOffset: function _getOffset(c) {
//         var d;
//         d = c ? b.currItem.img : b.st.zoom.opener(b.currItem.el || b.currItem);
//         var e = d.offset(),
//             f = parseInt(d.css("padding-top"), 10),
//             g = parseInt(d.css("padding-bottom"), 10);
//         e.top -= a(window).scrollTop() - f;
//         var h = {
//           width: d.width(),
//           height: (u ? d.innerHeight() : d[0].offsetHeight) - g - f
//         };
//         return O() ? h["-moz-transform"] = h.transform = "translate(" + e.left + "px," + e.top + "px)" : (h.left = e.left, h.top = e.top), h;
//       }
//     }
//   });

//   var P = "iframe",
//       Q = "//about:blank",
//       R = function R(a) {
//     if (b.currTemplate[P]) {
//       var c = b.currTemplate[P].find("iframe");
//       c.length && (a || (c[0].src = Q), b.isIE8 && c.css("display", a ? "block" : "none"));
//     }
//   };

//   a.magnificPopup.registerModule(P, {
//     options: {
//       markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
//       srcAction: "iframe_src",
//       patterns: {
//         youtube: {
//           index: "youtube.com",
//           id: "v=",
//           src: "//www.youtube.com/embed/%id%?autoplay=1"
//         },
//         vimeo: {
//           index: "vimeo.com/",
//           id: "/",
//           src: "//player.vimeo.com/video/%id%?autoplay=1"
//         },
//         gmaps: {
//           index: "//maps.google.",
//           src: "%id%&output=embed"
//         }
//       }
//     },
//     proto: {
//       initIframe: function initIframe() {
//         b.types.push(P), w("BeforeChange", function (a, b, c) {
//           b !== c && (b === P ? R() : c === P && R(!0));
//         }), w(h + "." + P, function () {
//           R();
//         });
//       },
//       getIframe: function getIframe(c, d) {
//         var e = c.src,
//             f = b.st.iframe;
//         a.each(f.patterns, function () {
//           return e.indexOf(this.index) > -1 ? (this.id && (e = "string" == typeof this.id ? e.substr(e.lastIndexOf(this.id) + this.id.length, e.length) : this.id.call(this, e)), e = this.src.replace("%id%", e), !1) : void 0;
//         });
//         var g = {};
//         return f.srcAction && (g[f.srcAction] = e), b._parseMarkup(d, g, c), b.updateStatus("ready"), d;
//       }
//     }
//   });

//   var S = function S(a) {
//     var c = b.items.length;
//     return a > c - 1 ? a - c : 0 > a ? c + a : a;
//   },
//       T = function T(a, b, c) {
//     return a.replace(/%curr%/gi, b + 1).replace(/%total%/gi, c);
//   };

//   a.magnificPopup.registerModule("gallery", {
//     options: {
//       enabled: !1,
//       arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
//       preload: [0, 2],
//       navigateByImgClick: !0,
//       arrows: !0,
//       tPrev: "Previous (Left arrow key)",
//       tNext: "Next (Right arrow key)",
//       tCounter: "%curr% of %total%"
//     },
//     proto: {
//       initGallery: function initGallery() {
//         var c = b.st.gallery,
//             e = ".mfp-gallery";
//         return b.direction = !0, c && c.enabled ? (f += " mfp-gallery", w(m + e, function () {
//           c.navigateByImgClick && b.wrap.on("click" + e, ".mfp-img", function () {
//             return b.items.length > 1 ? (b.next(), !1) : void 0;
//           }), d.on("keydown" + e, function (a) {
//             37 === a.keyCode ? b.prev() : 39 === a.keyCode && b.next();
//           });
//         }), w("UpdateStatus" + e, function (a, c) {
//           c.text && (c.text = T(c.text, b.currItem.index, b.items.length));
//         }), w(l + e, function (a, d, e, f) {
//           var g = b.items.length;
//           e.counter = g > 1 ? T(c.tCounter, f.index, g) : "";
//         }), w("BuildControls" + e, function () {
//           if (b.items.length > 1 && c.arrows && !b.arrowLeft) {
//             var d = c.arrowMarkup,
//                 e = b.arrowLeft = a(d.replace(/%title%/gi, c.tPrev).replace(/%dir%/gi, "left")).addClass(s),
//                 f = b.arrowRight = a(d.replace(/%title%/gi, c.tNext).replace(/%dir%/gi, "right")).addClass(s);
//             e.click(function () {
//               b.prev();
//             }), f.click(function () {
//               b.next();
//             }), b.container.append(e.add(f));
//           }
//         }), w(n + e, function () {
//           b._preloadTimeout && clearTimeout(b._preloadTimeout), b._preloadTimeout = setTimeout(function () {
//             b.preloadNearbyImages(), b._preloadTimeout = null;
//           }, 16);
//         }), void w(h + e, function () {
//           d.off(e), b.wrap.off("click" + e), b.arrowRight = b.arrowLeft = null;
//         })) : !1;
//       },
//       next: function next() {
//         b.direction = !0, b.index = S(b.index + 1), b.updateItemHTML();
//       },
//       prev: function prev() {
//         b.direction = !1, b.index = S(b.index - 1), b.updateItemHTML();
//       },
//       goTo: function goTo(a) {
//         b.direction = a >= b.index, b.index = a, b.updateItemHTML();
//       },
//       preloadNearbyImages: function preloadNearbyImages() {
//         var a,
//             c = b.st.gallery.preload,
//             d = Math.min(c[0], b.items.length),
//             e = Math.min(c[1], b.items.length);

//         for (a = 1; a <= (b.direction ? e : d); a++) {
//           b._preloadItem(b.index + a);
//         }

//         for (a = 1; a <= (b.direction ? d : e); a++) {
//           b._preloadItem(b.index - a);
//         }
//       },
//       _preloadItem: function _preloadItem(c) {
//         if (c = S(c), !b.items[c].preloaded) {
//           var d = b.items[c];
//           d.parsed || (d = b.parseEl(c)), y("LazyLoad", d), "image" === d.type && (d.img = a('<img class="mfp-img" />').on("load.mfploader", function () {
//             d.hasSize = !0;
//           }).on("error.mfploader", function () {
//             d.hasSize = !0, d.loadError = !0, y("LazyLoadError", d);
//           }).attr("src", d.src)), d.preloaded = !0;
//         }
//       }
//     }
//   });
//   var U = "retina";
//   a.magnificPopup.registerModule(U, {
//     options: {
//       replaceSrc: function replaceSrc(a) {
//         return a.src.replace(/\.\w+$/, function (a) {
//           return "@2x" + a;
//         });
//       },
//       ratio: 1
//     },
//     proto: {
//       initRetina: function initRetina() {
//         if (window.devicePixelRatio > 1) {
//           var a = b.st.retina,
//               c = a.ratio;
//           c = isNaN(c) ? c() : c, c > 1 && (w("ImageHasSize." + U, function (a, b) {
//             b.img.css({
//               "max-width": b.img[0].naturalWidth / c,
//               width: "100%"
//             });
//           }), w("ElementParse." + U, function (b, d) {
//             d.src = a.replaceSrc(d, c);
//           }));
//         }
//       }
//     }
//   }), A();
// });
// !function (i) {
//   "use strict";

//   "function" == typeof define && define.amd ? define(["jquery"], i) : "undefined" != typeof exports ? module.exports = i(require("jquery")) : i(jQuery);
// }(function (i) {
//   "use strict";

//   var e = window.Slick || {};
//   (e = function () {
//     var e = 0;
//     return function (t, o) {
//       var s,
//           n = this;
//       n.defaults = {
//         accessibility: !0,
//         adaptiveHeight: !1,
//         appendArrows: i(t),
//         appendDots: i(t),
//         arrows: !0,
//         asNavFor: null,
//         prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
//         nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
//         autoplay: !1,
//         autoplaySpeed: 3e3,
//         centerMode: !1,
//         centerPadding: "50px",
//         cssEase: "ease",
//         customPaging: function customPaging(e, t) {
//           return i('<button type="button" />').text(t + 1);
//         },
//         dots: !1,
//         dotsClass: "slick-dots",
//         draggable: !0,
//         easing: "linear",
//         edgeFriction: .35,
//         fade: !1,
//         focusOnSelect: !1,
//         focusOnChange: !1,
//         infinite: !0,
//         initialSlide: 0,
//         lazyLoad: "ondemand",
//         mobileFirst: !1,
//         pauseOnHover: !0,
//         pauseOnFocus: !0,
//         pauseOnDotsHover: !1,
//         respondTo: "window",
//         responsive: null,
//         rows: 1,
//         rtl: !1,
//         slide: "",
//         slidesPerRow: 1,
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         speed: 500,
//         swipe: !0,
//         swipeToSlide: !1,
//         touchMove: !0,
//         touchThreshold: 5,
//         useCSS: !0,
//         useTransform: !0,
//         variableWidth: !1,
//         vertical: !1,
//         verticalSwiping: !1,
//         waitForAnimate: !0,
//         zIndex: 1e3
//       }, n.initials = {
//         animating: !1,
//         dragging: !1,
//         autoPlayTimer: null,
//         currentDirection: 0,
//         currentLeft: null,
//         currentSlide: 0,
//         direction: 1,
//         $dots: null,
//         listWidth: null,
//         listHeight: null,
//         loadIndex: 0,
//         $nextArrow: null,
//         $prevArrow: null,
//         scrolling: !1,
//         slideCount: null,
//         slideWidth: null,
//         $slideTrack: null,
//         $slides: null,
//         sliding: !1,
//         slideOffset: 0,
//         swipeLeft: null,
//         swiping: !1,
//         $list: null,
//         touchObject: {},
//         transformsEnabled: !1,
//         unslicked: !1
//       }, i.extend(n, n.initials), n.activeBreakpoint = null, n.animType = null, n.animProp = null, n.breakpoints = [], n.breakpointSettings = [], n.cssTransitions = !1, n.focussed = !1, n.interrupted = !1, n.hidden = "hidden", n.paused = !0, n.positionProp = null, n.respondTo = null, n.rowCount = 1, n.shouldClick = !0, n.$slider = i(t), n.$slidesCache = null, n.transformType = null, n.transitionType = null, n.visibilityChange = "visibilitychange", n.windowWidth = 0, n.windowTimer = null, s = i(t).data("slick") || {}, n.options = i.extend({}, n.defaults, o, s), n.currentSlide = n.options.initialSlide, n.originalSettings = n.options, void 0 !== document.mozHidden ? (n.hidden = "mozHidden", n.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (n.hidden = "webkitHidden", n.visibilityChange = "webkitvisibilitychange"), n.autoPlay = i.proxy(n.autoPlay, n), n.autoPlayClear = i.proxy(n.autoPlayClear, n), n.autoPlayIterator = i.proxy(n.autoPlayIterator, n), n.changeSlide = i.proxy(n.changeSlide, n), n.clickHandler = i.proxy(n.clickHandler, n), n.selectHandler = i.proxy(n.selectHandler, n), n.setPosition = i.proxy(n.setPosition, n), n.swipeHandler = i.proxy(n.swipeHandler, n), n.dragHandler = i.proxy(n.dragHandler, n), n.keyHandler = i.proxy(n.keyHandler, n), n.instanceUid = e++, n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, n.registerBreakpoints(), n.init(!0);
//     };
//   }()).prototype.activateADA = function () {
//     this.$slideTrack.find(".slick-active").attr({
//       "aria-hidden": "false"
//     }).find("a, input, button, select").attr({
//       tabindex: "0"
//     });
//   }, e.prototype.addSlide = e.prototype.slickAdd = function (e, t, o) {
//     var s = this;
//     if ("boolean" == typeof t) o = t, t = null;else if (t < 0 || t >= s.slideCount) return !1;
//     s.unload(), "number" == typeof t ? 0 === t && 0 === s.$slides.length ? i(e).appendTo(s.$slideTrack) : o ? i(e).insertBefore(s.$slides.eq(t)) : i(e).insertAfter(s.$slides.eq(t)) : !0 === o ? i(e).prependTo(s.$slideTrack) : i(e).appendTo(s.$slideTrack), s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(), s.$slideTrack.append(s.$slides), s.$slides.each(function (e, t) {
//       i(t).attr("data-slick-index", e);
//     }), s.$slidesCache = s.$slides, s.reinit();
//   }, e.prototype.animateHeight = function () {
//     var i = this;

//     if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
//       var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
//       i.$list.animate({
//         height: e
//       }, i.options.speed);
//     }
//   }, e.prototype.animateSlide = function (e, t) {
//     var o = {},
//         s = this;
//     s.animateHeight(), !0 === s.options.rtl && !1 === s.options.vertical && (e = -e), !1 === s.transformsEnabled ? !1 === s.options.vertical ? s.$slideTrack.animate({
//       left: e
//     }, s.options.speed, s.options.easing, t) : s.$slideTrack.animate({
//       top: e
//     }, s.options.speed, s.options.easing, t) : !1 === s.cssTransitions ? (!0 === s.options.rtl && (s.currentLeft = -s.currentLeft), i({
//       animStart: s.currentLeft
//     }).animate({
//       animStart: e
//     }, {
//       duration: s.options.speed,
//       easing: s.options.easing,
//       step: function step(i) {
//         i = Math.ceil(i), !1 === s.options.vertical ? (o[s.animType] = "translate(" + i + "px, 0px)", s.$slideTrack.css(o)) : (o[s.animType] = "translate(0px," + i + "px)", s.$slideTrack.css(o));
//       },
//       complete: function complete() {
//         t && t.call();
//       }
//     })) : (s.applyTransition(), e = Math.ceil(e), !1 === s.options.vertical ? o[s.animType] = "translate3d(" + e + "px, 0px, 0px)" : o[s.animType] = "translate3d(0px," + e + "px, 0px)", s.$slideTrack.css(o), t && setTimeout(function () {
//       s.disableTransition(), t.call();
//     }, s.options.speed));
//   }, e.prototype.getNavTarget = function () {
//     var e = this,
//         t = e.options.asNavFor;
//     return t && null !== t && (t = i(t).not(e.$slider)), t;
//   }, e.prototype.asNavFor = function (e) {
//     var t = this.getNavTarget();
//     null !== t && "object" == _typeof(t) && t.each(function () {
//       var t = i(this).slick("getSlick");
//       t.unslicked || t.slideHandler(e, !0);
//     });
//   }, e.prototype.applyTransition = function (i) {
//     var e = this,
//         t = {};
//     !1 === e.options.fade ? t[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : t[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
//   }, e.prototype.autoPlay = function () {
//     var i = this;
//     i.autoPlayClear(), i.slideCount > i.options.slidesToShow && (i.autoPlayTimer = setInterval(i.autoPlayIterator, i.options.autoplaySpeed));
//   }, e.prototype.autoPlayClear = function () {
//     var i = this;
//     i.autoPlayTimer && clearInterval(i.autoPlayTimer);
//   }, e.prototype.autoPlayIterator = function () {
//     var i = this,
//         e = i.currentSlide + i.options.slidesToScroll;
//     i.paused || i.interrupted || i.focussed || (!1 === i.options.infinite && (1 === i.direction && i.currentSlide + 1 === i.slideCount - 1 ? i.direction = 0 : 0 === i.direction && (e = i.currentSlide - i.options.slidesToScroll, i.currentSlide - 1 == 0 && (i.direction = 1))), i.slideHandler(e));
//   }, e.prototype.buildArrows = function () {
//     var e = this;
//     !0 === e.options.arrows && (e.$prevArrow = i(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = i(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
//       "aria-disabled": "true",
//       tabindex: "-1"
//     }));
//   }, e.prototype.buildDots = function () {
//     var e,
//         t,
//         o = this;

//     if (!0 === o.options.dots) {
//       for (o.$slider.addClass("slick-dotted"), t = i("<ul />").addClass(o.options.dotsClass), e = 0; e <= o.getDotCount(); e += 1) {
//         t.append(i("<li />").append(o.options.customPaging.call(this, o, e)));
//       }

//       o.$dots = t.appendTo(o.options.appendDots), o.$dots.find("li").first().addClass("slick-active");
//     }
//   }, e.prototype.buildOut = function () {
//     var e = this;
//     e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
//       i(t).attr("data-slick-index", e).data("originalStyling", i(t).attr("style") || "");
//     }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? i('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), i("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable");
//   }, e.prototype.buildRows = function () {
//     var i,
//         e,
//         t,
//         o,
//         s,
//         n,
//         r,
//         l = this;

//     if (o = document.createDocumentFragment(), n = l.$slider.children(), l.options.rows > 1) {
//       for (r = l.options.slidesPerRow * l.options.rows, s = Math.ceil(n.length / r), i = 0; i < s; i++) {
//         var d = document.createElement("div");

//         for (e = 0; e < l.options.rows; e++) {
//           var a = document.createElement("div");

//           for (t = 0; t < l.options.slidesPerRow; t++) {
//             var c = i * r + (e * l.options.slidesPerRow + t);
//             n.get(c) && a.appendChild(n.get(c));
//           }

//           d.appendChild(a);
//         }

//         o.appendChild(d);
//       }

//       l.$slider.empty().append(o), l.$slider.children().children().children().css({
//         width: 100 / l.options.slidesPerRow + "%",
//         display: "inline-block"
//       });
//     }
//   }, e.prototype.checkResponsive = function (e, t) {
//     var o,
//         s,
//         n,
//         r = this,
//         l = !1,
//         d = r.$slider.width(),
//         a = window.innerWidth || i(window).width();

//     if ("window" === r.respondTo ? n = a : "slider" === r.respondTo ? n = d : "min" === r.respondTo && (n = Math.min(a, d)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
//       s = null;

//       for (o in r.breakpoints) {
//         r.breakpoints.hasOwnProperty(o) && (!1 === r.originalSettings.mobileFirst ? n < r.breakpoints[o] && (s = r.breakpoints[o]) : n > r.breakpoints[o] && (s = r.breakpoints[o]));
//       }

//       null !== s ? null !== r.activeBreakpoint ? (s !== r.activeBreakpoint || t) && (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), l = s), e || !1 === l || r.$slider.trigger("breakpoint", [r, l]);
//     }
//   }, e.prototype.changeSlide = function (e, t) {
//     var o,
//         s,
//         n,
//         r = this,
//         l = i(e.currentTarget);

//     switch (l.is("a") && e.preventDefault(), l.is("li") || (l = l.closest("li")), n = r.slideCount % r.options.slidesToScroll != 0, o = n ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, e.data.message) {
//       case "previous":
//         s = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - s, !1, t);
//         break;

//       case "next":
//         s = 0 === o ? r.options.slidesToScroll : o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + s, !1, t);
//         break;

//       case "index":
//         var d = 0 === e.data.index ? 0 : e.data.index || l.index() * r.options.slidesToScroll;
//         r.slideHandler(r.checkNavigable(d), !1, t), l.children().trigger("focus");
//         break;

//       default:
//         return;
//     }
//   }, e.prototype.checkNavigable = function (i) {
//     var e, t;
//     if (e = this.getNavigableIndexes(), t = 0, i > e[e.length - 1]) i = e[e.length - 1];else for (var o in e) {
//       if (i < e[o]) {
//         i = t;
//         break;
//       }

//       t = e[o];
//     }
//     return i;
//   }, e.prototype.cleanUpEvents = function () {
//     var e = this;
//     e.options.dots && null !== e.$dots && (i("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", i.proxy(e.interrupt, e, !0)).off("mouseleave.slick", i.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), i(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().off("click.slick", e.selectHandler), i(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), i(window).off("resize.slick.slick-" + e.instanceUid, e.resize), i("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), i(window).off("load.slick.slick-" + e.instanceUid, e.setPosition);
//   }, e.prototype.cleanUpSlideEvents = function () {
//     var e = this;
//     e.$list.off("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", i.proxy(e.interrupt, e, !1));
//   }, e.prototype.cleanUpRows = function () {
//     var i,
//         e = this;
//     e.options.rows > 1 && ((i = e.$slides.children().children()).removeAttr("style"), e.$slider.empty().append(i));
//   }, e.prototype.clickHandler = function (i) {
//     !1 === this.shouldClick && (i.stopImmediatePropagation(), i.stopPropagation(), i.preventDefault());
//   }, e.prototype.destroy = function (e) {
//     var t = this;
//     t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), i(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
//       i(this).attr("style", i(this).data("originalStyling"));
//     }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t]);
//   }, e.prototype.disableTransition = function (i) {
//     var e = this,
//         t = {};
//     t[e.transitionType] = "", !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
//   }, e.prototype.fadeSlide = function (i, e) {
//     var t = this;
//     !1 === t.cssTransitions ? (t.$slides.eq(i).css({
//       zIndex: t.options.zIndex
//     }), t.$slides.eq(i).animate({
//       opacity: 1
//     }, t.options.speed, t.options.easing, e)) : (t.applyTransition(i), t.$slides.eq(i).css({
//       opacity: 1,
//       zIndex: t.options.zIndex
//     }), e && setTimeout(function () {
//       t.disableTransition(i), e.call();
//     }, t.options.speed));
//   }, e.prototype.fadeSlideOut = function (i) {
//     var e = this;
//     !1 === e.cssTransitions ? e.$slides.eq(i).animate({
//       opacity: 0,
//       zIndex: e.options.zIndex - 2
//     }, e.options.speed, e.options.easing) : (e.applyTransition(i), e.$slides.eq(i).css({
//       opacity: 0,
//       zIndex: e.options.zIndex - 2
//     }));
//   }, e.prototype.filterSlides = e.prototype.slickFilter = function (i) {
//     var e = this;
//     null !== i && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(i).appendTo(e.$slideTrack), e.reinit());
//   }, e.prototype.focusHandler = function () {
//     var e = this;
//     e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (t) {
//       t.stopImmediatePropagation();
//       var o = i(this);
//       setTimeout(function () {
//         e.options.pauseOnFocus && (e.focussed = o.is(":focus"), e.autoPlay());
//       }, 0);
//     });
//   }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
//     return this.currentSlide;
//   }, e.prototype.getDotCount = function () {
//     var i = this,
//         e = 0,
//         t = 0,
//         o = 0;
//     if (!0 === i.options.infinite) {
//       if (i.slideCount <= i.options.slidesToShow) ++o;else for (; e < i.slideCount;) {
//         ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
//       }
//     } else if (!0 === i.options.centerMode) o = i.slideCount;else if (i.options.asNavFor) for (; e < i.slideCount;) {
//       ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
//     } else o = 1 + Math.ceil((i.slideCount - i.options.slidesToShow) / i.options.slidesToScroll);
//     return o - 1;
//   }, e.prototype.getLeft = function (i) {
//     var e,
//         t,
//         o,
//         s,
//         n = this,
//         r = 0;
//     return n.slideOffset = 0, t = n.$slides.first().outerHeight(!0), !0 === n.options.infinite ? (n.slideCount > n.options.slidesToShow && (n.slideOffset = n.slideWidth * n.options.slidesToShow * -1, s = -1, !0 === n.options.vertical && !0 === n.options.centerMode && (2 === n.options.slidesToShow ? s = -1.5 : 1 === n.options.slidesToShow && (s = -2)), r = t * n.options.slidesToShow * s), n.slideCount % n.options.slidesToScroll != 0 && i + n.options.slidesToScroll > n.slideCount && n.slideCount > n.options.slidesToShow && (i > n.slideCount ? (n.slideOffset = (n.options.slidesToShow - (i - n.slideCount)) * n.slideWidth * -1, r = (n.options.slidesToShow - (i - n.slideCount)) * t * -1) : (n.slideOffset = n.slideCount % n.options.slidesToScroll * n.slideWidth * -1, r = n.slideCount % n.options.slidesToScroll * t * -1))) : i + n.options.slidesToShow > n.slideCount && (n.slideOffset = (i + n.options.slidesToShow - n.slideCount) * n.slideWidth, r = (i + n.options.slidesToShow - n.slideCount) * t), n.slideCount <= n.options.slidesToShow && (n.slideOffset = 0, r = 0), !0 === n.options.centerMode && n.slideCount <= n.options.slidesToShow ? n.slideOffset = n.slideWidth * Math.floor(n.options.slidesToShow) / 2 - n.slideWidth * n.slideCount / 2 : !0 === n.options.centerMode && !0 === n.options.infinite ? n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2) - n.slideWidth : !0 === n.options.centerMode && (n.slideOffset = 0, n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2)), e = !1 === n.options.vertical ? i * n.slideWidth * -1 + n.slideOffset : i * t * -1 + r, !0 === n.options.variableWidth && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, !0 === n.options.centerMode && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow + 1), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, e += (n.$list.width() - o.outerWidth()) / 2)), e;
//   }, e.prototype.getOption = e.prototype.slickGetOption = function (i) {
//     return this.options[i];
//   }, e.prototype.getNavigableIndexes = function () {
//     var i,
//         e = this,
//         t = 0,
//         o = 0,
//         s = [];

//     for (!1 === e.options.infinite ? i = e.slideCount : (t = -1 * e.options.slidesToScroll, o = -1 * e.options.slidesToScroll, i = 2 * e.slideCount); t < i;) {
//       s.push(t), t = o + e.options.slidesToScroll, o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
//     }

//     return s;
//   }, e.prototype.getSlick = function () {
//     return this;
//   }, e.prototype.getSlideCount = function () {
//     var e,
//         t,
//         o = this;
//     return t = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (s, n) {
//       if (n.offsetLeft - t + i(n).outerWidth() / 2 > -1 * o.swipeLeft) return e = n, !1;
//     }), Math.abs(i(e).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll;
//   }, e.prototype.goTo = e.prototype.slickGoTo = function (i, e) {
//     this.changeSlide({
//       data: {
//         message: "index",
//         index: parseInt(i)
//       }
//     }, e);
//   }, e.prototype.init = function (e) {
//     var t = this;
//     i(t.$slider).hasClass("slick-initialized") || (i(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay());
//   }, e.prototype.initADA = function () {
//     var e = this,
//         t = Math.ceil(e.slideCount / e.options.slidesToShow),
//         o = e.getNavigableIndexes().filter(function (i) {
//       return i >= 0 && i < e.slideCount;
//     });
//     e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
//       "aria-hidden": "true",
//       tabindex: "-1"
//     }).find("a, input, button, select").attr({
//       tabindex: "-1"
//     }), null !== e.$dots && (e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function (t) {
//       var s = o.indexOf(t);
//       i(this).attr({
//         role: "tabpanel",
//         id: "slick-slide" + e.instanceUid + t,
//         tabindex: -1
//       }), -1 !== s && i(this).attr({
//         "aria-describedby": "slick-slide-control" + e.instanceUid + s
//       });
//     }), e.$dots.attr("role", "tablist").find("li").each(function (s) {
//       var n = o[s];
//       i(this).attr({
//         role: "presentation"
//       }), i(this).find("button").first().attr({
//         role: "tab",
//         id: "slick-slide-control" + e.instanceUid + s,
//         "aria-controls": "slick-slide" + e.instanceUid + n,
//         "aria-label": s + 1 + " of " + t,
//         "aria-selected": null,
//         tabindex: "-1"
//       });
//     }).eq(e.currentSlide).find("button").attr({
//       "aria-selected": "true",
//       tabindex: "0"
//     }).end());

//     for (var s = e.currentSlide, n = s + e.options.slidesToShow; s < n; s++) {
//       e.$slides.eq(s).attr("tabindex", 0);
//     }

//     e.activateADA();
//   }, e.prototype.initArrowEvents = function () {
//     var i = this;
//     !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.off("click.slick").on("click.slick", {
//       message: "previous"
//     }, i.changeSlide), i.$nextArrow.off("click.slick").on("click.slick", {
//       message: "next"
//     }, i.changeSlide), !0 === i.options.accessibility && (i.$prevArrow.on("keydown.slick", i.keyHandler), i.$nextArrow.on("keydown.slick", i.keyHandler)));
//   }, e.prototype.initDotEvents = function () {
//     var e = this;
//     !0 === e.options.dots && (i("li", e.$dots).on("click.slick", {
//       message: "index"
//     }, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && i("li", e.$dots).on("mouseenter.slick", i.proxy(e.interrupt, e, !0)).on("mouseleave.slick", i.proxy(e.interrupt, e, !1));
//   }, e.prototype.initSlideEvents = function () {
//     var e = this;
//     e.options.pauseOnHover && (e.$list.on("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", i.proxy(e.interrupt, e, !1)));
//   }, e.prototype.initializeEvents = function () {
//     var e = this;
//     e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
//       action: "start"
//     }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
//       action: "move"
//     }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
//       action: "end"
//     }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
//       action: "end"
//     }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), i(document).on(e.visibilityChange, i.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), i(window).on("orientationchange.slick.slick-" + e.instanceUid, i.proxy(e.orientationChange, e)), i(window).on("resize.slick.slick-" + e.instanceUid, i.proxy(e.resize, e)), i("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), i(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), i(e.setPosition);
//   }, e.prototype.initUI = function () {
//     var i = this;
//     !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.show(), i.$nextArrow.show()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.show();
//   }, e.prototype.keyHandler = function (i) {
//     var e = this;
//     i.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === i.keyCode && !0 === e.options.accessibility ? e.changeSlide({
//       data: {
//         message: !0 === e.options.rtl ? "next" : "previous"
//       }
//     }) : 39 === i.keyCode && !0 === e.options.accessibility && e.changeSlide({
//       data: {
//         message: !0 === e.options.rtl ? "previous" : "next"
//       }
//     }));
//   }, e.prototype.lazyLoad = function () {
//     function e(e) {
//       i("img[data-lazy]", e).each(function () {
//         var e = i(this),
//             t = i(this).attr("data-lazy"),
//             o = i(this).attr("data-srcset"),
//             s = i(this).attr("data-sizes") || n.$slider.attr("data-sizes"),
//             r = document.createElement("img");
//         r.onload = function () {
//           e.animate({
//             opacity: 0
//           }, 100, function () {
//             o && (e.attr("srcset", o), s && e.attr("sizes", s)), e.attr("src", t).animate({
//               opacity: 1
//             }, 200, function () {
//               e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading");
//             }), n.$slider.trigger("lazyLoaded", [n, e, t]);
//           });
//         }, r.onerror = function () {
//           e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), n.$slider.trigger("lazyLoadError", [n, e, t]);
//         }, r.src = t;
//       });
//     }

//     var t,
//         o,
//         s,
//         n = this;
//     if (!0 === n.options.centerMode ? !0 === n.options.infinite ? s = (o = n.currentSlide + (n.options.slidesToShow / 2 + 1)) + n.options.slidesToShow + 2 : (o = Math.max(0, n.currentSlide - (n.options.slidesToShow / 2 + 1)), s = n.options.slidesToShow / 2 + 1 + 2 + n.currentSlide) : (o = n.options.infinite ? n.options.slidesToShow + n.currentSlide : n.currentSlide, s = Math.ceil(o + n.options.slidesToShow), !0 === n.options.fade && (o > 0 && o--, s <= n.slideCount && s++)), t = n.$slider.find(".slick-slide").slice(o, s), "anticipated" === n.options.lazyLoad) for (var r = o - 1, l = s, d = n.$slider.find(".slick-slide"), a = 0; a < n.options.slidesToScroll; a++) {
//       r < 0 && (r = n.slideCount - 1), t = (t = t.add(d.eq(r))).add(d.eq(l)), r--, l++;
//     }
//     e(t), n.slideCount <= n.options.slidesToShow ? e(n.$slider.find(".slick-slide")) : n.currentSlide >= n.slideCount - n.options.slidesToShow ? e(n.$slider.find(".slick-cloned").slice(0, n.options.slidesToShow)) : 0 === n.currentSlide && e(n.$slider.find(".slick-cloned").slice(-1 * n.options.slidesToShow));
//   }, e.prototype.loadSlider = function () {
//     var i = this;
//     i.setPosition(), i.$slideTrack.css({
//       opacity: 1
//     }), i.$slider.removeClass("slick-loading"), i.initUI(), "progressive" === i.options.lazyLoad && i.progressiveLazyLoad();
//   }, e.prototype.next = e.prototype.slickNext = function () {
//     this.changeSlide({
//       data: {
//         message: "next"
//       }
//     });
//   }, e.prototype.orientationChange = function () {
//     var i = this;
//     i.checkResponsive(), i.setPosition();
//   }, e.prototype.pause = e.prototype.slickPause = function () {
//     var i = this;
//     i.autoPlayClear(), i.paused = !0;
//   }, e.prototype.play = e.prototype.slickPlay = function () {
//     var i = this;
//     i.autoPlay(), i.options.autoplay = !0, i.paused = !1, i.focussed = !1, i.interrupted = !1;
//   }, e.prototype.postSlide = function (e) {
//     var t = this;
//     t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && i(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()));
//   }, e.prototype.prev = e.prototype.slickPrev = function () {
//     this.changeSlide({
//       data: {
//         message: "previous"
//       }
//     });
//   }, e.prototype.preventDefault = function (i) {
//     i.preventDefault();
//   }, e.prototype.progressiveLazyLoad = function (e) {
//     e = e || 1;
//     var t,
//         o,
//         s,
//         n,
//         r,
//         l = this,
//         d = i("img[data-lazy]", l.$slider);
//     d.length ? (t = d.first(), o = t.attr("data-lazy"), s = t.attr("data-srcset"), n = t.attr("data-sizes") || l.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function () {
//       s && (t.attr("srcset", s), n && t.attr("sizes", n)), t.attr("src", o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === l.options.adaptiveHeight && l.setPosition(), l.$slider.trigger("lazyLoaded", [l, t, o]), l.progressiveLazyLoad();
//     }, r.onerror = function () {
//       e < 3 ? setTimeout(function () {
//         l.progressiveLazyLoad(e + 1);
//       }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), l.$slider.trigger("lazyLoadError", [l, t, o]), l.progressiveLazyLoad());
//     }, r.src = o) : l.$slider.trigger("allImagesLoaded", [l]);
//   }, e.prototype.refresh = function (e) {
//     var t,
//         o,
//         s = this;
//     o = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > o && (s.currentSlide = o), s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), t = s.currentSlide, s.destroy(!0), i.extend(s, s.initials, {
//       currentSlide: t
//     }), s.init(), e || s.changeSlide({
//       data: {
//         message: "index",
//         index: t
//       }
//     }, !1);
//   }, e.prototype.registerBreakpoints = function () {
//     var e,
//         t,
//         o,
//         s = this,
//         n = s.options.responsive || null;

//     if ("array" === i.type(n) && n.length) {
//       s.respondTo = s.options.respondTo || "window";

//       for (e in n) {
//         if (o = s.breakpoints.length - 1, n.hasOwnProperty(e)) {
//           for (t = n[e].breakpoint; o >= 0;) {
//             s.breakpoints[o] && s.breakpoints[o] === t && s.breakpoints.splice(o, 1), o--;
//           }

//           s.breakpoints.push(t), s.breakpointSettings[t] = n[e].settings;
//         }
//       }

//       s.breakpoints.sort(function (i, e) {
//         return s.options.mobileFirst ? i - e : e - i;
//       });
//     }
//   }, e.prototype.reinit = function () {
//     var e = this;
//     e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e]);
//   }, e.prototype.resize = function () {
//     var e = this;
//     i(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
//       e.windowWidth = i(window).width(), e.checkResponsive(), e.unslicked || e.setPosition();
//     }, 50));
//   }, e.prototype.removeSlide = e.prototype.slickRemove = function (i, e, t) {
//     var o = this;
//     if (i = "boolean" == typeof i ? !0 === (e = i) ? 0 : o.slideCount - 1 : !0 === e ? --i : i, o.slideCount < 1 || i < 0 || i > o.slideCount - 1) return !1;
//     o.unload(), !0 === t ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(i).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, o.reinit();
//   }, e.prototype.setCSS = function (i) {
//     var e,
//         t,
//         o = this,
//         s = {};
//     !0 === o.options.rtl && (i = -i), e = "left" == o.positionProp ? Math.ceil(i) + "px" : "0px", t = "top" == o.positionProp ? Math.ceil(i) + "px" : "0px", s[o.positionProp] = i, !1 === o.transformsEnabled ? o.$slideTrack.css(s) : (s = {}, !1 === o.cssTransitions ? (s[o.animType] = "translate(" + e + ", " + t + ")", o.$slideTrack.css(s)) : (s[o.animType] = "translate3d(" + e + ", " + t + ", 0px)", o.$slideTrack.css(s)));
//   }, e.prototype.setDimensions = function () {
//     var i = this;
//     !1 === i.options.vertical ? !0 === i.options.centerMode && i.$list.css({
//       padding: "0px " + i.options.centerPadding
//     }) : (i.$list.height(i.$slides.first().outerHeight(!0) * i.options.slidesToShow), !0 === i.options.centerMode && i.$list.css({
//       padding: i.options.centerPadding + " 0px"
//     })), i.listWidth = i.$list.width(), i.listHeight = i.$list.height(), !1 === i.options.vertical && !1 === i.options.variableWidth ? (i.slideWidth = Math.ceil(i.listWidth / i.options.slidesToShow), i.$slideTrack.width(Math.ceil(i.slideWidth * i.$slideTrack.children(".slick-slide").length))) : !0 === i.options.variableWidth ? i.$slideTrack.width(5e3 * i.slideCount) : (i.slideWidth = Math.ceil(i.listWidth), i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0) * i.$slideTrack.children(".slick-slide").length)));
//     var e = i.$slides.first().outerWidth(!0) - i.$slides.first().width();
//     !1 === i.options.variableWidth && i.$slideTrack.children(".slick-slide").width(i.slideWidth - e);
//   }, e.prototype.setFade = function () {
//     var e,
//         t = this;
//     t.$slides.each(function (o, s) {
//       e = t.slideWidth * o * -1, !0 === t.options.rtl ? i(s).css({
//         position: "relative",
//         right: e,
//         top: 0,
//         zIndex: t.options.zIndex - 2,
//         opacity: 0
//       }) : i(s).css({
//         position: "relative",
//         left: e,
//         top: 0,
//         zIndex: t.options.zIndex - 2,
//         opacity: 0
//       });
//     }), t.$slides.eq(t.currentSlide).css({
//       zIndex: t.options.zIndex - 1,
//       opacity: 1
//     });
//   }, e.prototype.setHeight = function () {
//     var i = this;

//     if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
//       var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
//       i.$list.css("height", e);
//     }
//   }, e.prototype.setOption = e.prototype.slickSetOption = function () {
//     var e,
//         t,
//         o,
//         s,
//         n,
//         r = this,
//         l = !1;
//     if ("object" === i.type(arguments[0]) ? (o = arguments[0], l = arguments[1], n = "multiple") : "string" === i.type(arguments[0]) && (o = arguments[0], s = arguments[1], l = arguments[2], "responsive" === arguments[0] && "array" === i.type(arguments[1]) ? n = "responsive" : void 0 !== arguments[1] && (n = "single")), "single" === n) r.options[o] = s;else if ("multiple" === n) i.each(o, function (i, e) {
//       r.options[i] = e;
//     });else if ("responsive" === n) for (t in s) {
//       if ("array" !== i.type(r.options.responsive)) r.options.responsive = [s[t]];else {
//         for (e = r.options.responsive.length - 1; e >= 0;) {
//           r.options.responsive[e].breakpoint === s[t].breakpoint && r.options.responsive.splice(e, 1), e--;
//         }

//         r.options.responsive.push(s[t]);
//       }
//     }
//     l && (r.unload(), r.reinit());
//   }, e.prototype.setPosition = function () {
//     var i = this;
//     i.setDimensions(), i.setHeight(), !1 === i.options.fade ? i.setCSS(i.getLeft(i.currentSlide)) : i.setFade(), i.$slider.trigger("setPosition", [i]);
//   }, e.prototype.setProps = function () {
//     var i = this,
//         e = document.body.style;
//     i.positionProp = !0 === i.options.vertical ? "top" : "left", "top" === i.positionProp ? i.$slider.addClass("slick-vertical") : i.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === i.options.useCSS && (i.cssTransitions = !0), i.options.fade && ("number" == typeof i.options.zIndex ? i.options.zIndex < 3 && (i.options.zIndex = 3) : i.options.zIndex = i.defaults.zIndex), void 0 !== e.OTransform && (i.animType = "OTransform", i.transformType = "-o-transform", i.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.MozTransform && (i.animType = "MozTransform", i.transformType = "-moz-transform", i.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (i.animType = !1)), void 0 !== e.webkitTransform && (i.animType = "webkitTransform", i.transformType = "-webkit-transform", i.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.msTransform && (i.animType = "msTransform", i.transformType = "-ms-transform", i.transitionType = "msTransition", void 0 === e.msTransform && (i.animType = !1)), void 0 !== e.transform && !1 !== i.animType && (i.animType = "transform", i.transformType = "transform", i.transitionType = "transition"), i.transformsEnabled = i.options.useTransform && null !== i.animType && !1 !== i.animType;
//   }, e.prototype.setSlideClasses = function (i) {
//     var e,
//         t,
//         o,
//         s,
//         n = this;

//     if (t = n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), n.$slides.eq(i).addClass("slick-current"), !0 === n.options.centerMode) {
//       var r = n.options.slidesToShow % 2 == 0 ? 1 : 0;
//       e = Math.floor(n.options.slidesToShow / 2), !0 === n.options.infinite && (i >= e && i <= n.slideCount - 1 - e ? n.$slides.slice(i - e + r, i + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = n.options.slidesToShow + i, t.slice(o - e + 1 + r, o + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === i ? t.eq(t.length - 1 - n.options.slidesToShow).addClass("slick-center") : i === n.slideCount - 1 && t.eq(n.options.slidesToShow).addClass("slick-center")), n.$slides.eq(i).addClass("slick-center");
//     } else i >= 0 && i <= n.slideCount - n.options.slidesToShow ? n.$slides.slice(i, i + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : t.length <= n.options.slidesToShow ? t.addClass("slick-active").attr("aria-hidden", "false") : (s = n.slideCount % n.options.slidesToShow, o = !0 === n.options.infinite ? n.options.slidesToShow + i : i, n.options.slidesToShow == n.options.slidesToScroll && n.slideCount - i < n.options.slidesToShow ? t.slice(o - (n.options.slidesToShow - s), o + s).addClass("slick-active").attr("aria-hidden", "false") : t.slice(o, o + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));

//     "ondemand" !== n.options.lazyLoad && "anticipated" !== n.options.lazyLoad || n.lazyLoad();
//   }, e.prototype.setupInfinite = function () {
//     var e,
//         t,
//         o,
//         s = this;

//     if (!0 === s.options.fade && (s.options.centerMode = !1), !0 === s.options.infinite && !1 === s.options.fade && (t = null, s.slideCount > s.options.slidesToShow)) {
//       for (o = !0 === s.options.centerMode ? s.options.slidesToShow + 1 : s.options.slidesToShow, e = s.slideCount; e > s.slideCount - o; e -= 1) {
//         t = e - 1, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");
//       }

//       for (e = 0; e < o + s.slideCount; e += 1) {
//         t = e, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");
//       }

//       s.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
//         i(this).attr("id", "");
//       });
//     }
//   }, e.prototype.interrupt = function (i) {
//     var e = this;
//     i || e.autoPlay(), e.interrupted = i;
//   }, e.prototype.selectHandler = function (e) {
//     var t = this,
//         o = i(e.target).is(".slick-slide") ? i(e.target) : i(e.target).parents(".slick-slide"),
//         s = parseInt(o.attr("data-slick-index"));
//     s || (s = 0), t.slideCount <= t.options.slidesToShow ? t.slideHandler(s, !1, !0) : t.slideHandler(s);
//   }, e.prototype.slideHandler = function (i, e, t) {
//     var o,
//         s,
//         n,
//         r,
//         l,
//         d = null,
//         a = this;
//     if (e = e || !1, !(!0 === a.animating && !0 === a.options.waitForAnimate || !0 === a.options.fade && a.currentSlide === i)) if (!1 === e && a.asNavFor(i), o = i, d = a.getLeft(o), r = a.getLeft(a.currentSlide), a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft, !1 === a.options.infinite && !1 === a.options.centerMode && (i < 0 || i > a.getDotCount() * a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
//       a.postSlide(o);
//     }) : a.postSlide(o));else if (!1 === a.options.infinite && !0 === a.options.centerMode && (i < 0 || i > a.slideCount - a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
//       a.postSlide(o);
//     }) : a.postSlide(o));else {
//       if (a.options.autoplay && clearInterval(a.autoPlayTimer), s = o < 0 ? a.slideCount % a.options.slidesToScroll != 0 ? a.slideCount - a.slideCount % a.options.slidesToScroll : a.slideCount + o : o >= a.slideCount ? a.slideCount % a.options.slidesToScroll != 0 ? 0 : o - a.slideCount : o, a.animating = !0, a.$slider.trigger("beforeChange", [a, a.currentSlide, s]), n = a.currentSlide, a.currentSlide = s, a.setSlideClasses(a.currentSlide), a.options.asNavFor && (l = (l = a.getNavTarget()).slick("getSlick")).slideCount <= l.options.slidesToShow && l.setSlideClasses(a.currentSlide), a.updateDots(), a.updateArrows(), !0 === a.options.fade) return !0 !== t ? (a.fadeSlideOut(n), a.fadeSlide(s, function () {
//         a.postSlide(s);
//       })) : a.postSlide(s), void a.animateHeight();
//       !0 !== t ? a.animateSlide(d, function () {
//         a.postSlide(s);
//       }) : a.postSlide(s);
//     }
//   }, e.prototype.startLoad = function () {
//     var i = this;
//     !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.hide(), i.$nextArrow.hide()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.hide(), i.$slider.addClass("slick-loading");
//   }, e.prototype.swipeDirection = function () {
//     var i,
//         e,
//         t,
//         o,
//         s = this;
//     return i = s.touchObject.startX - s.touchObject.curX, e = s.touchObject.startY - s.touchObject.curY, t = Math.atan2(e, i), (o = Math.round(180 * t / Math.PI)) < 0 && (o = 360 - Math.abs(o)), o <= 45 && o >= 0 ? !1 === s.options.rtl ? "left" : "right" : o <= 360 && o >= 315 ? !1 === s.options.rtl ? "left" : "right" : o >= 135 && o <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? o >= 35 && o <= 135 ? "down" : "up" : "vertical";
//   }, e.prototype.swipeEnd = function (i) {
//     var e,
//         t,
//         o = this;
//     if (o.dragging = !1, o.swiping = !1, o.scrolling) return o.scrolling = !1, !1;
//     if (o.interrupted = !1, o.shouldClick = !(o.touchObject.swipeLength > 10), void 0 === o.touchObject.curX) return !1;

//     if (!0 === o.touchObject.edgeHit && o.$slider.trigger("edge", [o, o.swipeDirection()]), o.touchObject.swipeLength >= o.touchObject.minSwipe) {
//       switch (t = o.swipeDirection()) {
//         case "left":
//         case "down":
//           e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), o.currentDirection = 0;
//           break;

//         case "right":
//         case "up":
//           e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), o.currentDirection = 1;
//       }

//       "vertical" != t && (o.slideHandler(e), o.touchObject = {}, o.$slider.trigger("swipe", [o, t]));
//     } else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), o.touchObject = {});
//   }, e.prototype.swipeHandler = function (i) {
//     var e = this;
//     if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== i.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = i.originalEvent && void 0 !== i.originalEvent.touches ? i.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), i.data.action) {
//       case "start":
//         e.swipeStart(i);
//         break;

//       case "move":
//         e.swipeMove(i);
//         break;

//       case "end":
//         e.swipeEnd(i);
//     }
//   }, e.prototype.swipeMove = function (i) {
//     var e,
//         t,
//         o,
//         s,
//         n,
//         r,
//         l = this;
//     return n = void 0 !== i.originalEvent ? i.originalEvent.touches : null, !(!l.dragging || l.scrolling || n && 1 !== n.length) && (e = l.getLeft(l.currentSlide), l.touchObject.curX = void 0 !== n ? n[0].pageX : i.clientX, l.touchObject.curY = void 0 !== n ? n[0].pageY : i.clientY, l.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(l.touchObject.curX - l.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(l.touchObject.curY - l.touchObject.startY, 2))), !l.options.verticalSwiping && !l.swiping && r > 4 ? (l.scrolling = !0, !1) : (!0 === l.options.verticalSwiping && (l.touchObject.swipeLength = r), t = l.swipeDirection(), void 0 !== i.originalEvent && l.touchObject.swipeLength > 4 && (l.swiping = !0, i.preventDefault()), s = (!1 === l.options.rtl ? 1 : -1) * (l.touchObject.curX > l.touchObject.startX ? 1 : -1), !0 === l.options.verticalSwiping && (s = l.touchObject.curY > l.touchObject.startY ? 1 : -1), o = l.touchObject.swipeLength, l.touchObject.edgeHit = !1, !1 === l.options.infinite && (0 === l.currentSlide && "right" === t || l.currentSlide >= l.getDotCount() && "left" === t) && (o = l.touchObject.swipeLength * l.options.edgeFriction, l.touchObject.edgeHit = !0), !1 === l.options.vertical ? l.swipeLeft = e + o * s : l.swipeLeft = e + o * (l.$list.height() / l.listWidth) * s, !0 === l.options.verticalSwiping && (l.swipeLeft = e + o * s), !0 !== l.options.fade && !1 !== l.options.touchMove && (!0 === l.animating ? (l.swipeLeft = null, !1) : void l.setCSS(l.swipeLeft))));
//   }, e.prototype.swipeStart = function (i) {
//     var e,
//         t = this;
//     if (t.interrupted = !0, 1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow) return t.touchObject = {}, !1;
//     void 0 !== i.originalEvent && void 0 !== i.originalEvent.touches && (e = i.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== e ? e.pageX : i.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== e ? e.pageY : i.clientY, t.dragging = !0;
//   }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
//     var i = this;
//     null !== i.$slidesCache && (i.unload(), i.$slideTrack.children(this.options.slide).detach(), i.$slidesCache.appendTo(i.$slideTrack), i.reinit());
//   }, e.prototype.unload = function () {
//     var e = this;
//     i(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
//   }, e.prototype.unslick = function (i) {
//     var e = this;
//     e.$slider.trigger("unslick", [e, i]), e.destroy();
//   }, e.prototype.updateArrows = function () {
//     var i = this;
//     Math.floor(i.options.slidesToShow / 2), !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && !i.options.infinite && (i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === i.currentSlide ? (i.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - i.options.slidesToShow && !1 === i.options.centerMode ? (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - 1 && !0 === i.options.centerMode && (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
//   }, e.prototype.updateDots = function () {
//     var i = this;
//     null !== i.$dots && (i.$dots.find("li").removeClass("slick-active").end(), i.$dots.find("li").eq(Math.floor(i.currentSlide / i.options.slidesToScroll)).addClass("slick-active"));
//   }, e.prototype.visibility = function () {
//     var i = this;
//     i.options.autoplay && (document[i.hidden] ? i.interrupted = !0 : i.interrupted = !1);
//   }, i.fn.slick = function () {
//     var i,
//         t,
//         o = this,
//         s = arguments[0],
//         n = Array.prototype.slice.call(arguments, 1),
//         r = o.length;

//     for (i = 0; i < r; i++) {
//       if ("object" == _typeof(s) || void 0 === s ? o[i].slick = new e(o[i], s) : t = o[i].slick[s].apply(o[i].slick, n), void 0 !== t) return t;
//     }

//     return o;
//   };
// });
// /*! WOW wow.js - v1.3.0 - 2016-10-04
// * https://wowjs.uk
// * Copyright (c) 2016 Thomas Grainger; Licensed MIT */

// !function (a, b) {
//   if ("function" == typeof define && define.amd) define(["module", "exports"], b);else if ("undefined" != typeof exports) b(module, exports);else {
//     var c = {
//       exports: {}
//     };
//     b(c, c.exports), a.WOW = c.exports;
//   }
// }(this, function (a, b) {
//   "use strict";

//   function c(a, b) {
//     if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
//   }

//   function d(a, b) {
//     return b.indexOf(a) >= 0;
//   }

//   function e(a, b) {
//     for (var c in b) {
//       if (null == a[c]) {
//         var d = b[c];
//         a[c] = d;
//       }
//     }

//     return a;
//   }

//   function f(a) {
//     return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a);
//   }

//   function g(a) {
//     var b = arguments.length <= 1 || void 0 === arguments[1] ? !1 : arguments[1],
//         c = arguments.length <= 2 || void 0 === arguments[2] ? !1 : arguments[2],
//         d = arguments.length <= 3 || void 0 === arguments[3] ? null : arguments[3],
//         e = void 0;
//     return null != document.createEvent ? (e = document.createEvent("CustomEvent"), e.initCustomEvent(a, b, c, d)) : null != document.createEventObject ? (e = document.createEventObject(), e.eventType = a) : e.eventName = a, e;
//   }

//   function h(a, b) {
//     null != a.dispatchEvent ? a.dispatchEvent(b) : b in (null != a) ? a[b]() : "on" + b in (null != a) && a["on" + b]();
//   }

//   function i(a, b, c) {
//     null != a.addEventListener ? a.addEventListener(b, c, !1) : null != a.attachEvent ? a.attachEvent("on" + b, c) : a[b] = c;
//   }

//   function j(a, b, c) {
//     null != a.removeEventListener ? a.removeEventListener(b, c, !1) : null != a.detachEvent ? a.detachEvent("on" + b, c) : delete a[b];
//   }

//   function k() {
//     return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight;
//   }

//   Object.defineProperty(b, "__esModule", {
//     value: !0
//   });

//   var l,
//       m,
//       n = function () {
//     function a(a, b) {
//       for (var c = 0; c < b.length; c++) {
//         var d = b[c];
//         d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), Object.defineProperty(a, d.key, d);
//       }
//     }

//     return function (b, c, d) {
//       return c && a(b.prototype, c), d && a(b, d), b;
//     };
//   }(),
//       o = window.WeakMap || window.MozWeakMap || function () {
//     function a() {
//       c(this, a), this.keys = [], this.values = [];
//     }

//     return n(a, [{
//       key: "get",
//       value: function value(a) {
//         for (var b = 0; b < this.keys.length; b++) {
//           var c = this.keys[b];
//           if (c === a) return this.values[b];
//         }
//       }
//     }, {
//       key: "set",
//       value: function value(a, b) {
//         for (var c = 0; c < this.keys.length; c++) {
//           var d = this.keys[c];
//           if (d === a) return this.values[c] = b, this;
//         }

//         return this.keys.push(a), this.values.push(b), this;
//       }
//     }]), a;
//   }(),
//       p = window.MutationObserver || window.WebkitMutationObserver || window.MozMutationObserver || (m = l = function () {
//     function a() {
//       c(this, a), "undefined" != typeof console && null !== console && (console.warn("MutationObserver is not supported by your browser."), console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content."));
//     }

//     return n(a, [{
//       key: "observe",
//       value: function value() {}
//     }]), a;
//   }(), l.notSupported = !0, m),
//       q = window.getComputedStyle || function (a) {
//     var b = /(\-([a-z]){1})/g;
//     return {
//       getPropertyValue: function getPropertyValue(c) {
//         "float" === c && (c = "styleFloat"), b.test(c) && c.replace(b, function (a, b) {
//           return b.toUpperCase();
//         });
//         var d = a.currentStyle;
//         return (null != d ? d[c] : void 0) || null;
//       }
//     };
//   },
//       r = function () {
//     function a() {
//       var b = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0];
//       c(this, a), this.defaults = {
//         boxClass: "wow",
//         animateClass: "animated",
//         offset: 0,
//         mobile: !0,
//         live: !0,
//         callback: null,
//         scrollContainer: null,
//         resetAnimation: !0
//       }, this.animate = function () {
//         return "requestAnimationFrame" in window ? function (a) {
//           return window.requestAnimationFrame(a);
//         } : function (a) {
//           return a();
//         };
//       }(), this.vendors = ["moz", "webkit"], this.start = this.start.bind(this), this.resetAnimation = this.resetAnimation.bind(this), this.scrollHandler = this.scrollHandler.bind(this), this.scrollCallback = this.scrollCallback.bind(this), this.scrolled = !0, this.config = e(b, this.defaults), null != b.scrollContainer && (this.config.scrollContainer = document.querySelector(b.scrollContainer)), this.animationNameCache = new o(), this.wowEvent = g(this.config.boxClass);
//     }

//     return n(a, [{
//       key: "init",
//       value: function value() {
//         this.element = window.document.documentElement, d(document.readyState, ["interactive", "complete"]) ? this.start() : i(document, "DOMContentLoaded", this.start), this.finished = [];
//       }
//     }, {
//       key: "start",
//       value: function value() {
//         var a = this;
//         if (this.stopped = !1, this.boxes = [].slice.call(this.element.querySelectorAll("." + this.config.boxClass)), this.all = this.boxes.slice(0), this.boxes.length) if (this.disabled()) this.resetStyle();else for (var b = 0; b < this.boxes.length; b++) {
//           var c = this.boxes[b];
//           this.applyStyle(c, !0);
//         }

//         if (this.disabled() || (i(this.config.scrollContainer || window, "scroll", this.scrollHandler), i(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live) {
//           var d = new p(function (b) {
//             for (var c = 0; c < b.length; c++) {
//               for (var d = b[c], e = 0; e < d.addedNodes.length; e++) {
//                 var f = d.addedNodes[e];
//                 a.doSync(f);
//               }
//             }
//           });
//           d.observe(document.body, {
//             childList: !0,
//             subtree: !0
//           });
//         }
//       }
//     }, {
//       key: "stop",
//       value: function value() {
//         this.stopped = !0, j(this.config.scrollContainer || window, "scroll", this.scrollHandler), j(window, "resize", this.scrollHandler), null != this.interval && clearInterval(this.interval);
//       }
//     }, {
//       key: "sync",
//       value: function value() {
//         p.notSupported && this.doSync(this.element);
//       }
//     }, {
//       key: "doSync",
//       value: function value(a) {
//         if ("undefined" != typeof a && null !== a || (a = this.element), 1 === a.nodeType) {
//           a = a.parentNode || a;

//           for (var b = a.querySelectorAll("." + this.config.boxClass), c = 0; c < b.length; c++) {
//             var e = b[c];
//             d(e, this.all) || (this.boxes.push(e), this.all.push(e), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(e, !0), this.scrolled = !0);
//           }
//         }
//       }
//     }, {
//       key: "show",
//       value: function value(a) {
//         return this.applyStyle(a), a.className = a.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(a), h(a, this.wowEvent), this.config.resetAnimation && (i(a, "animationend", this.resetAnimation), i(a, "oanimationend", this.resetAnimation), i(a, "webkitAnimationEnd", this.resetAnimation), i(a, "MSAnimationEnd", this.resetAnimation)), a;
//       }
//     }, {
//       key: "applyStyle",
//       value: function value(a, b) {
//         var c = this,
//             d = a.getAttribute("data-wow-duration"),
//             e = a.getAttribute("data-wow-delay"),
//             f = a.getAttribute("data-wow-iteration");
//         return this.animate(function () {
//           return c.customStyle(a, b, d, e, f);
//         });
//       }
//     }, {
//       key: "resetStyle",
//       value: function value() {
//         for (var a = 0; a < this.boxes.length; a++) {
//           var b = this.boxes[a];
//           b.style.visibility = "visible";
//         }
//       }
//     }, {
//       key: "resetAnimation",
//       value: function value(a) {
//         if (a.type.toLowerCase().indexOf("animationend") >= 0) {
//           var b = a.target || a.srcElement;
//           b.className = b.className.replace(this.config.animateClass, "").trim();
//         }
//       }
//     }, {
//       key: "customStyle",
//       value: function value(a, b, c, d, e) {
//         return b && this.cacheAnimationName(a), a.style.visibility = b ? "hidden" : "visible", c && this.vendorSet(a.style, {
//           animationDuration: c
//         }), d && this.vendorSet(a.style, {
//           animationDelay: d
//         }), e && this.vendorSet(a.style, {
//           animationIterationCount: e
//         }), this.vendorSet(a.style, {
//           animationName: b ? "none" : this.cachedAnimationName(a)
//         }), a;
//       }
//     }, {
//       key: "vendorSet",
//       value: function value(a, b) {
//         for (var c in b) {
//           if (b.hasOwnProperty(c)) {
//             var d = b[c];
//             a["" + c] = d;

//             for (var e = 0; e < this.vendors.length; e++) {
//               var f = this.vendors[e];
//               a["" + f + c.charAt(0).toUpperCase() + c.substr(1)] = d;
//             }
//           }
//         }
//       }
//     }, {
//       key: "vendorCSS",
//       value: function value(a, b) {
//         for (var c = q(a), d = c.getPropertyCSSValue(b), e = 0; e < this.vendors.length; e++) {
//           var f = this.vendors[e];
//           d = d || c.getPropertyCSSValue("-" + f + "-" + b);
//         }

//         return d;
//       }
//     }, {
//       key: "animationName",
//       value: function value(a) {
//         var b = void 0;

//         try {
//           b = this.vendorCSS(a, "animation-name").cssText;
//         } catch (c) {
//           b = q(a).getPropertyValue("animation-name");
//         }

//         return "none" === b ? "" : b;
//       }
//     }, {
//       key: "cacheAnimationName",
//       value: function value(a) {
//         return this.animationNameCache.set(a, this.animationName(a));
//       }
//     }, {
//       key: "cachedAnimationName",
//       value: function value(a) {
//         return this.animationNameCache.get(a);
//       }
//     }, {
//       key: "scrollHandler",
//       value: function value() {
//         this.scrolled = !0;
//       }
//     }, {
//       key: "scrollCallback",
//       value: function value() {
//         if (this.scrolled) {
//           this.scrolled = !1;

//           for (var a = [], b = 0; b < this.boxes.length; b++) {
//             var c = this.boxes[b];

//             if (c) {
//               if (this.isVisible(c)) {
//                 this.show(c);
//                 continue;
//               }

//               a.push(c);
//             }
//           }

//           this.boxes = a, this.boxes.length || this.config.live || this.stop();
//         }
//       }
//     }, {
//       key: "offsetTop",
//       value: function value(a) {
//         for (; void 0 === a.offsetTop;) {
//           a = a.parentNode;
//         }

//         for (var b = a.offsetTop; a.offsetParent;) {
//           a = a.offsetParent, b += a.offsetTop;
//         }

//         return b;
//       }
//     }, {
//       key: "isVisible",
//       value: function value(a) {
//         var b = a.getAttribute("data-wow-offset") || this.config.offset,
//             c = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset,
//             d = c + Math.min(this.element.clientHeight, k()) - b,
//             e = this.offsetTop(a),
//             f = e + a.clientHeight;
//         return d >= e && f >= c;
//       }
//     }, {
//       key: "disabled",
//       value: function value() {
//         return !this.config.mobile && f(navigator.userAgent);
//       }
//     }]), a;
//   }();

//   b["default"] = r, a.exports = b["default"];
// });
// $(document).ready(function () {
//   window.initPhoneMask = function (item) {
//     var $selector = $(item);
//     var events = {
//       onKeyPress: function onKeyPress(cep, e, currentField, options) {
//         e.target.classList.add('validation-error');
//       },
//       onComplete: function onComplete(cep, e) {
//         e.target.classList.remove('validation-error');
//       }
//     };

//     if ($selector.length) {
//       $selector.mask("+380 (99) 999-99-99", _objectSpread({
//         'translation': {
//           9: {
//             pattern: /[0-9]/
//           },
//           0: {
//             pattern: /[0]/,
//             fallback: '0'
//           }
//         },
//         placeholder: "+380 (__) ___-__-__"
//       }, events));
//       $selector.intlTelInput({
//         defaultCountry: "ua",
//         // allowExtensions: true,
//         autoPlaceholder: 'off',
//         autoHideDialCode: true,
//         formatOnDisplay: false,
//         preferredCountries: ['ua', 'ru', 'md', 'by', 'pl', 'il', 'kz', 'am'],
//         nationalMode: true // utilsScript: "./international-mask/js/utils.js",

//       });
//       $(document).on('click', '[data-country-code]', function () {
//         var country = $(this).data('country-code');

//         switch (country) {
//           case "ru":
//           case "kz":
//             $selector.mask("+7 (999) 999-99-99", _objectSpread({
//               placeholder: "+7 (___) ___-__-__"
//             }, events));
//             break;

//           case "ua":
//             $selector.mask("+380 (99) 999-99-99", _objectSpread({
//               'translation': {
//                 9: {
//                   pattern: /[0-9]/
//                 },
//                 0: {
//                   pattern: /[0]/,
//                   fallback: '0'
//                 }
//               },
//               placeholder: "+380 (__) ___-__-__"
//             }, events));
//             break;

//           case "md":
//             $selector.mask("+373 (99) 999-99-99", _objectSpread({
//               placeholder: "+373 (__) ___-__-__"
//             }, events));
//             break;

//           case "am":
//             $selector.mask("+374 (99) 999-99-99", _objectSpread({
//               placeholder: "+374 (__) ___-__-__"
//             }, events));
//             break;

//           case "by":
//             $selector.mask("+375 (99) 999-99-99", _objectSpread({
//               placeholder: "+375 (__) ___-__-__"
//             }, events));
//             break;

//           case "pl":
//             $selector.mask("+48 (999) 999-99-99", _objectSpread({
//               placeholder: "+48 (___) ___-__-__"
//             }, events));
//             break;

//           case "il":
//             $selector.mask("+072 (99) 999-99-99", _objectSpread({
//               'translation': {
//                 9: {
//                   pattern: /[0-9]/
//                 },
//                 0: {
//                   pattern: /[9]/,
//                   fallback: '9'
//                 }
//               },
//               placeholder: "+972 (__) ___-__-__"
//             }, events));
//             break;

//           default:
//             $selector.mask("+999 (99) 999-99ZZ", _objectSpread({
//               'translation': {
//                 'Z': {
//                   pattern: /[0-9]/,
//                   optional: true
//                 }
//               },
//               placeholder: "+___ (__) ___-__-__"
//             }, events));
//         }
//       });
//     }
//   };

//   $('.seo-text__show-more').on('click', function () {
//     $(this).next().slideToggle();
//   });
//   $('.video').on('click', function () {
//     $(this).addClass('load');
//     var _frame = $(this).find('[data-onclick-src]')[0];
//     _frame.src = _frame.dataset.onclickSrc;

//     _frame.onload = function () {
//       $(this).removeClass('load');
//     };

//     delete _frame.dataset.onclickSrc;
//   });
//   var $hamburger = $('.hamburger');
//   var $overlay = $('.overlay');
//   var $popup;

//   function togglePopup() {
//     $popup = $(this.dataset.togle);
//     $popup.toggleClass('active');
//     $('html').toggleClass('popupShow');
//   }

//   $('[data-togle]').on('click', togglePopup);
//   $('.js-close').on('click', function () {
//     $('html').removeClass('popupShow');

//     if ($popup && $popup[0]) {
//       $($popup).removeClass('active');
//     } else {
//       $('.popup.active').removeClass('active');
//     }
//   });
//   $('.js-shape-close').on('click', function (e) {
//     var $this = $(this);

//     if ($this[0] == e.target) {
//       $this.removeClass('active');
//       $('html').removeClass('popupShow');
//     }
//   });
//   $('form').on('submit', function (e) {
//     e.preventDefault();

//     var _form = this; // find category for gtag event


//     var categories = {
//       website: 'razrabotka_sayta',
//       ecommerce: 'internet_magazin',
//       zapros: 'zapros_video',
//       homepage: 'glavnaya',
//       portfolio__form: 'portfolio',
//       popupform: 'zadat_vopros',
//       contactform: 'contacts',
//       seopage: 'seo',
//       smm: 'smm_prod',
//       android: 'mob_appc'
//     };
//     var eventCategory = 'default';

//     for (var _i2 = 0, _Object$keys = Object.keys(categories); _i2 < _Object$keys.length; _i2++) {
//       var category = _Object$keys[_i2];

//       if (e.target.classList.contains(category)) {
//         eventCategory = categories[category];
//       }
//     }

//     var form = new FormData(e.target);
//     var formObject = {};
//     form.forEach(function (value, key) {
//       formObject[key] = value;
//     });
//     formObject.url = window.location.href;
//     var button = e.target.querySelector('.button');
//     var errorContainer = e.target.querySelector('.recaptchaError');

//     if (!errorContainer) {
//       return;
//     }

//     errorContainer.textContent = '';

//     if (formObject['g-recaptcha-response'].length) {
//       _form.reset();

//       errorContainer.textContent = '';
//       $(this).addClass('success');
//       showMessage(true);
//       $.ajax({
//         type: 'POST',
//         url: '/mail/send',
//         data: formObject,
//         success: function success(response) {
//           if (gtag) {
//             gtag('event', 'send_form', {
//               'event_category': eventCategory
//             });
//           }
//         },
//         error: function error(_error) {
//           errorContainer.textContent = 'Ошибка отправки формы';
//           console.error(_error);
//           button.disabled = false;
//         }
//       });
//     } else {
//       errorContainer.textContent = 'Введи капчу';
//     }
//   });
//   $('[name="user_name"]').on('keypress', function () {
//     var that = this;
//     setTimeout(function () {
//       var res = /[^а-яa-zєії'ґ ]/gi.exec(that.value);
//       that.value = that.value.replace(res, '');
//     }, 0);
//   });

//   function showMessage(result, message) {
//     $('html').removeClass('popupShow');
//     $($popup).removeClass('active');
//     $popup = $('#popupmessage');
//     $popup.toggleClass('active');
//     $('html').addClass('popupShow');

//     if (result) {
//       $popup.addClass('success');
//     } else {
//       $popup.addClass('error');
//     }
//   }

//   var inputs = document.querySelectorAll("[name='user_phone']");
//   inputs.forEach(function (input) {
//     initPhoneMask(input);
//   });
//   $hamburger.on('click', function () {
//     toggleMobileMenu();
//   });
//   $overlay.on('click', function () {
//     toggleMobileMenu();
//   });

//   function toggleMobileMenu() {
//     $hamburger.toggleClass('is-active').next().toggleClass('active');
//     $overlay.fadeToggle();
//     $('.mainHeader').toggleClass('active');
//   }
//   /*end показать скрытый блок под гамбургером*/

//   /*показать выпадающее меню*/
//   // document.addEventListener('click', toggleServicesMenu, false);
//   // function toggleServicesMenu(e) {
//   //   if (e.target.id === 'serviceCaret') {
//   //   }
//   // };


//   $('#serviceCaret').on('click', function (e) {
//     document.querySelector('.navigation__menu-list').classList.toggle('active');
//   });
//   /*end выпадающее меню*/

//   /*работа с popup*/

//   $('.popup-slider-link').magnificPopup({
//     removalDelay: 500,
//     callbacks: {
//       beforeOpen: function beforeOpen() {
//         this.st.mainClass = this.st.el.attr('data-effect');
//       },
//       open: function open() {
//         $('.gallery__slider').not('.slick-initialized').slick();
//       }
//     },
//     midClick: true
//   });
//   $('.popup-link').magnificPopup({
//     removalDelay: 500,
//     callbacks: {
//       beforeOpen: function beforeOpen() {
//         this.st.mainClass = this.st.el.attr('data-effect');
//       },
//       open: function open() {
//         $('.gallery__slider').not('.slick-initialized').slick();
//       }
//     },
//     midClick: true
//   });
//   $('.popup-link-success').magnificPopup({
//     removalDelay: 500,
//     callbacks: {
//       beforeOpen: function beforeOpen() {
//         this.st.mainClass = this.st.el.attr('data-effect');
//       },
//       open: function open() {
//         $('.gallery__slider').not('.slick-initialized').slick();
//       }
//     },
//     midClick: true
//   });
//   /*end работа с popup*/

//   /*работа со слайдерами*/

//   $('.results__slider').slick({
//     arrows: true,
//     adaptiveHeight: true,
//     slidesToShow: 1,
//     dots: true,
//     infinite: false,
//     appendDots: $('.results__buttons'),
//     appendArrows: $('.results__buttons'),
//     nextArrow: '.button.next',
//     prevArrow: '.button.prev'
//   });
//   $('.popup__clients-result-slider').slick({
//     arrows: false,
//     infinite: true,
//     slidesToScroll: 1,
//     dots: false,
//     adaptiveHeight: true,
//     currentProjectSlide: 0
//   });
//   /*end работа со слайдерами*/

//   var $techtab = $('.techtab-tab');
//   $('[data-jsTabToggle]').on('click', function () {
//     var $active = $('[data-jsTabToggle].active');
//     var $newActive = $(this);
//     $active.removeClass('active');
//     $techtab.removeClass($active[0].dataset.jstabtoggle);
//     $newActive.addClass('active');
//     $techtab.addClass($newActive[0].dataset.jstabtoggle);
//   });

//   var _loop = document.querySelector('.portfolio__projectsLoop');

//   if (_loop) {
//     _loop = $(_loop).clone();
//   } // filter type


//   $('[data-type]').on('click', function () {
//     $('[data-type].active').removeClass('active');
//     $(this).addClass('active');
//     var $loopClone = $(_loop).clone();
//     var projectsLoop = $('.portfolio__projectsLoop');
//     projectsLoop.after($loopClone);
//     projectsLoop.remove();
//     console.log({
//       projectsLoop: projectsLoop
//     });
//     var type = this.dataset.type;

//     if (type == 'all') {
//       portfolioHider();
//       return;
//     }

//     ;
//     $('.portfolio__projectsLoop [data-item-type]').each(function (i, item) {
//       if (item.dataset && item.dataset.itemType !== type) {
//         item.remove();
//       } else {
//         $(item).find('[data-togle]').on('click', togglePopup);
//       }
//     });
//     lazyLoadInstance.update();
//     portfolioHider();
//   });
//   portfolioHider();
//   interceptionObserver(document.querySelectorAll('#google-map, .g-recaptcha'));
//   onScroll();
// });
// var showMore = 12;
// $('.js-showMore').on('click', function () {
//   showMore += 6;
//   portfolioHider();
// });
// var mediaQuery = '(min-width: 767px)';
// var mediaQueryList = window.matchMedia(mediaQuery);

// if (mediaQueryList.matches) {
//   dataSrc();
//   new WOW().init();
// }

// mediaQueryList.addEventListener('change', function (event) {
//   if (event.matches) {
//     dataSrc();
//     new WOW().init();
//   }
// });

// function dataSrc() {
//   $('[data-src]').each(function (i, item) {
//     if (!item.dataset.src) return;
//     item.src = item.dataset.src;
//     delete item.dataset.src;
//   });
// }

// function isVideo(src) {
//   return /.mp4/.test(src);
// }

// function isMobile() {
//   return screen.width <= 768;
// }

// function portfolioHider() {
//   var items = $('.portfolio__projectsLoop [data-item-type]');
//   if (!items[0]) return;
//   items.each(function (i, item) {
//     if (i > showMore - 1) {
//       $(item).addClass('hidden');
//     } else {
//       $(item).removeClass('hidden'); // let $img = $(item).find('[data-hidden-src]');
//       // if($img[0] &&  $img[0].dataset){
//       //   $img[0].src = $img[0].dataset.hiddenSrc;
//       //   delete $img[0].dataset.hiddenSrc;
//       // }
//     }
//   });

//   if (!$('.portfolio__projectsLoop [data-item-type].hidden').length) {
//     $('.js-showMore').addClass('hidden');
//     $('.projects__footer').addClass('last');
//   } else {
//     $('.js-showMore').removeClass('hidden');
//     $('.projects__footer').removeClass('last');
//   }
// }

// function onScroll() {
//   if ($(window).scrollTop() > 10) {
//     $('.mainHeader').addClass('mainHeader-background');
//     $('.mainHeader__inner-item').addClass('item-hidden');
//   } else {
//     $('.mainHeader').removeClass('mainHeader-background');
//     $('.mainHeader__inner-item').removeClass('item-hidden');
//   }
// } //interceptionObserver


// function interceptionObserver(itemList) {
//   // устанавливаем настройки
//   var options = {
//     root: null,
//     rootMargin: '0px',
//     threshold: 0.5
//   }; // создаем наблюдатель

//   var observer = new IntersectionObserver(function (entries, observer) {
//     entries.forEach(function (entry) {
//       if (entry.isIntersecting) {
//         var target = entry.target;

//         if (target.id == 'google-map') {
//           initMap();
//         } else {
//           recaptchaInit();
//         }

//         observer.unobserve(target);
//       }
//     });
//   }, options);
//   itemList.forEach(function (item) {
//     observer.observe(item);
//   });
// } // mapInclude


// function initMap() {
//   window.initMap = function () {
//     var map = new google.maps.Map(document.getElementById("google-map"), {
//       zoom: 17,
//       center: {
//         lat: 46.458551,
//         lng: 30.740516
//       },
//       styles: [{
//         "featureType": "all",
//         "elementType": "all",
//         "stylers": [{
//           "saturation": -100
//         }, {
//           "gamma": 0.5
//         }]
//       }]
//     });
//     var image = '/img/icons/map-marker.png';
//     var marker = new google.maps.Marker({
//       position: {
//         lat: 46.458551,
//         lng: 30.740516
//       },
//       map: map,
//       icon: image
//     });
//   };

//   var script = document.createElement('script');
//   script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBjdeoiMUd__EiEU86NviKYb3Pad7DVh14&callback=initMap&libraries=&v=weekly';
//   script.async = true;
//   document.body.appendChild(script);
// } // captchaInclude


// function recaptchaInit() {
//   var key = '6LfTdqYaAAAAAFUhIZq7rac-iaNkSwCu_V8z-L2d';
//   var script = document.createElement('script');
//   var isExist = document.querySelector('#recaptchaInit');
//   if (isExist) return;
//   script.src = 'https://www.google.com/recaptcha/api.js?render=explicit';
//   script.async = true;
//   script.id = 'recaptchaInit';

//   script.onload = function () {
//     grecaptcha.ready(function () {
//       document.querySelectorAll('.g-recaptcha').forEach(function (el) {
//         grecaptcha.render(el, {
//           'sitekey': key
//         });
//       });
//     });
//   };

//   document.body.appendChild(script);
// }

// $('.ddl-selector').on('click', function () {
//   $('.ddl').toggleClass('active');
// });
// $('.ddl button').on('click', function () {
//   $('.ddl.active').removeClass('active');
//   $('.ddl-selector').text($(this).text());
// });

// function recaptchaCallback(e, item) {
//   console.log('recaptchaCallback', {
//     e: e,
//     item: item
//   });
// }

// $(window).on('scroll', onScroll);
// /*
//  * International Telephone Input v16.0.15
//  * https://github.com/jackocnr/intl-tel-input.git
//  * Licensed under the MIT license
//  */

// !function (a) {
//   "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = a(require("jquery"), window, document) : "function" == typeof define && define.amd ? define(["jquery"], function (b) {
//     a(b, window, document);
//   }) : a(jQuery, window, document);
// }(function (a, b, c, d) {
//   "use strict";

//   function e(a, b) {
//     if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
//   }

//   function f(a, b) {
//     for (var c = 0; c < b.length; c++) {
//       var d = b[c];
//       d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), Object.defineProperty(a, d.key, d);
//     }
//   }

//   function g(a, b, c) {
//     return b && f(a.prototype, b), c && f(a, c), a;
//   }

//   for (var h = [["Afghanistan (‫افغانستان‬‎)", "af", "93"], ["Albania (Shqipëri)", "al", "355"], ["Algeria (‫الجزائر‬‎)", "dz", "213"], ["American Samoa", "as", "1", 5, ["684"]], ["Andorra", "ad", "376"], ["Angola", "ao", "244"], ["Anguilla", "ai", "1", 6, ["264"]], ["Antigua and Barbuda", "ag", "1", 7, ["268"]], ["Argentina", "ar", "54"], ["Armenia (Հայաստան)", "am", "374"], ["Aruba", "aw", "297"], ["Australia", "au", "61", 0], ["Austria (Österreich)", "at", "43"], ["Azerbaijan (Azərbaycan)", "az", "994"], ["Bahamas", "bs", "1", 8, ["242"]], ["Bahrain (‫البحرين‬‎)", "bh", "973"], ["Bangladesh (বাংলাদেশ)", "bd", "880"], ["Barbados", "bb", "1", 9, ["246"]], ["Belarus (Беларусь)", "by", "375"], ["Belgium (België)", "be", "32"], ["Belize", "bz", "501"], ["Benin (Bénin)", "bj", "229"], ["Bermuda", "bm", "1", 10, ["441"]], ["Bhutan (འབྲུག)", "bt", "975"], ["Bolivia", "bo", "591"], ["Bosnia and Herzegovina (Босна и Херцеговина)", "ba", "387"], ["Botswana", "bw", "267"], ["Brazil (Brasil)", "br", "55"], ["British Indian Ocean Territory", "io", "246"], ["British Virgin Islands", "vg", "1", 11, ["284"]], ["Brunei", "bn", "673"], ["Bulgaria (България)", "bg", "359"], ["Burkina Faso", "bf", "226"], ["Burundi (Uburundi)", "bi", "257"], ["Cambodia (កម្ពុជា)", "kh", "855"], ["Cameroon (Cameroun)", "cm", "237"], ["Canada", "ca", "1", 1, ["204", "226", "236", "249", "250", "289", "306", "343", "365", "387", "403", "416", "418", "431", "437", "438", "450", "506", "514", "519", "548", "579", "581", "587", "604", "613", "639", "647", "672", "705", "709", "742", "778", "780", "782", "807", "819", "825", "867", "873", "902", "905"]], ["Cape Verde (Kabu Verdi)", "cv", "238"], ["Caribbean Netherlands", "bq", "599", 1, ["3", "4", "7"]], ["Cayman Islands", "ky", "1", 12, ["345"]], ["Central African Republic (République centrafricaine)", "cf", "236"], ["Chad (Tchad)", "td", "235"], ["Chile", "cl", "56"], ["China (中国)", "cn", "86"], ["Christmas Island", "cx", "61", 2], ["Cocos (Keeling) Islands", "cc", "61", 1], ["Colombia", "co", "57"], ["Comoros (‫جزر القمر‬‎)", "km", "269"], ["Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)", "cd", "243"], ["Congo (Republic) (Congo-Brazzaville)", "cg", "242"], ["Cook Islands", "ck", "682"], ["Costa Rica", "cr", "506"], ["Côte d’Ivoire", "ci", "225"], ["Croatia (Hrvatska)", "hr", "385"], ["Cuba", "cu", "53"], ["Curaçao", "cw", "599", 0], ["Cyprus (Κύπρος)", "cy", "357"], ["Czech Republic (Česká republika)", "cz", "420"], ["Denmark (Danmark)", "dk", "45"], ["Djibouti", "dj", "253"], ["Dominica", "dm", "1", 13, ["767"]], ["Dominican Republic (República Dominicana)", "do", "1", 2, ["809", "829", "849"]], ["Ecuador", "ec", "593"], ["Egypt (‫مصر‬‎)", "eg", "20"], ["El Salvador", "sv", "503"], ["Equatorial Guinea (Guinea Ecuatorial)", "gq", "240"], ["Eritrea", "er", "291"], ["Estonia (Eesti)", "ee", "372"], ["Ethiopia", "et", "251"], ["Falkland Islands (Islas Malvinas)", "fk", "500"], ["Faroe Islands (Føroyar)", "fo", "298"], ["Fiji", "fj", "679"], ["Finland (Suomi)", "fi", "358", 0], ["France", "fr", "33"], ["French Guiana (Guyane française)", "gf", "594"], ["French Polynesia (Polynésie française)", "pf", "689"], ["Gabon", "ga", "241"], ["Gambia", "gm", "220"], ["Georgia (საქართველო)", "ge", "995"], ["Germany (Deutschland)", "de", "49"], ["Ghana (Gaana)", "gh", "233"], ["Gibraltar", "gi", "350"], ["Greece (Ελλάδα)", "gr", "30"], ["Greenland (Kalaallit Nunaat)", "gl", "299"], ["Grenada", "gd", "1", 14, ["473"]], ["Guadeloupe", "gp", "590", 0], ["Guam", "gu", "1", 15, ["671"]], ["Guatemala", "gt", "502"], ["Guernsey", "gg", "44", 1, ["1481", "7781", "7839", "7911"]], ["Guinea (Guinée)", "gn", "224"], ["Guinea-Bissau (Guiné Bissau)", "gw", "245"], ["Guyana", "gy", "592"], ["Haiti", "ht", "509"], ["Honduras", "hn", "504"], ["Hong Kong (香港)", "hk", "852"], ["Hungary (Magyarország)", "hu", "36"], ["Iceland (Ísland)", "is", "354"], ["India (भारत)", "in", "91"], ["Indonesia", "id", "62"], ["Iran (‫ایران‬‎)", "ir", "98"], ["Iraq (‫العراق‬‎)", "iq", "964"], ["Ireland", "ie", "353"], ["Isle of Man", "im", "44", 2, ["1624", "74576", "7524", "7924", "7624"]], ["Israel (‫ישראל‬‎)", "il", "972"], ["Italy (Italia)", "it", "39", 0], ["Jamaica", "jm", "1", 4, ["876", "658"]], ["Japan (日本)", "jp", "81"], ["Jersey", "je", "44", 3, ["1534", "7509", "7700", "7797", "7829", "7937"]], ["Jordan (‫الأردن‬‎)", "jo", "962"], ["Kazakhstan (Казахстан)", "kz", "7", 1, ["33", "7"]], ["Kenya", "ke", "254"], ["Kiribati", "ki", "686"], ["Kosovo", "xk", "383"], ["Kuwait (‫الكويت‬‎)", "kw", "965"], ["Kyrgyzstan (Кыргызстан)", "kg", "996"], ["Laos (ລາວ)", "la", "856"], ["Latvia (Latvija)", "lv", "371"], ["Lebanon (‫لبنان‬‎)", "lb", "961"], ["Lesotho", "ls", "266"], ["Liberia", "lr", "231"], ["Libya (‫ليبيا‬‎)", "ly", "218"], ["Liechtenstein", "li", "423"], ["Lithuania (Lietuva)", "lt", "370"], ["Luxembourg", "lu", "352"], ["Macau (澳門)", "mo", "853"], ["Macedonia (FYROM) (Македонија)", "mk", "389"], ["Madagascar (Madagasikara)", "mg", "261"], ["Malawi", "mw", "265"], ["Malaysia", "my", "60"], ["Maldives", "mv", "960"], ["Mali", "ml", "223"], ["Malta", "mt", "356"], ["Marshall Islands", "mh", "692"], ["Martinique", "mq", "596"], ["Mauritania (‫موريتانيا‬‎)", "mr", "222"], ["Mauritius (Moris)", "mu", "230"], ["Mayotte", "yt", "262", 1, ["269", "639"]], ["Mexico (México)", "mx", "52"], ["Micronesia", "fm", "691"], ["Moldova (Republica Moldova)", "md", "373"], ["Monaco", "mc", "377"], ["Mongolia (Монгол)", "mn", "976"], ["Montenegro (Crna Gora)", "me", "382"], ["Montserrat", "ms", "1", 16, ["664"]], ["Morocco (‫المغرب‬‎)", "ma", "212", 0], ["Mozambique (Moçambique)", "mz", "258"], ["Myanmar (Burma) (မြန်မာ)", "mm", "95"], ["Namibia (Namibië)", "na", "264"], ["Nauru", "nr", "674"], ["Nepal (नेपाल)", "np", "977"], ["Netherlands (Nederland)", "nl", "31"], ["New Caledonia (Nouvelle-Calédonie)", "nc", "687"], ["New Zealand", "nz", "64"], ["Nicaragua", "ni", "505"], ["Niger (Nijar)", "ne", "227"], ["Nigeria", "ng", "234"], ["Niue", "nu", "683"], ["Norfolk Island", "nf", "672"], ["North Korea (조선 민주주의 인민 공화국)", "kp", "850"], ["Northern Mariana Islands", "mp", "1", 17, ["670"]], ["Norway (Norge)", "no", "47", 0], ["Oman (‫عُمان‬‎)", "om", "968"], ["Pakistan (‫پاکستان‬‎)", "pk", "92"], ["Palau", "pw", "680"], ["Palestine (‫فلسطين‬‎)", "ps", "970"], ["Panama (Panamá)", "pa", "507"], ["Papua New Guinea", "pg", "675"], ["Paraguay", "py", "595"], ["Peru (Perú)", "pe", "51"], ["Philippines", "ph", "63"], ["Poland (Polska)", "pl", "48"], ["Portugal", "pt", "351"], ["Puerto Rico", "pr", "1", 3, ["787", "939"]], ["Qatar (‫قطر‬‎)", "qa", "974"], ["Réunion (La Réunion)", "re", "262", 0], ["Romania (România)", "ro", "40"], ["Russia (Россия)", "ru", "7", 0], ["Rwanda", "rw", "250"], ["Saint Barthélemy", "bl", "590", 1], ["Saint Helena", "sh", "290"], ["Saint Kitts and Nevis", "kn", "1", 18, ["869"]], ["Saint Lucia", "lc", "1", 19, ["758"]], ["Saint Martin (Saint-Martin (partie française))", "mf", "590", 2], ["Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)", "pm", "508"], ["Saint Vincent and the Grenadines", "vc", "1", 20, ["784"]], ["Samoa", "ws", "685"], ["San Marino", "sm", "378"], ["São Tomé and Príncipe (São Tomé e Príncipe)", "st", "239"], ["Saudi Arabia (‫المملكة العربية السعودية‬‎)", "sa", "966"], ["Senegal (Sénégal)", "sn", "221"], ["Serbia (Србија)", "rs", "381"], ["Seychelles", "sc", "248"], ["Sierra Leone", "sl", "232"], ["Singapore", "sg", "65"], ["Sint Maarten", "sx", "1", 21, ["721"]], ["Slovakia (Slovensko)", "sk", "421"], ["Slovenia (Slovenija)", "si", "386"], ["Solomon Islands", "sb", "677"], ["Somalia (Soomaaliya)", "so", "252"], ["South Africa", "za", "27"], ["South Korea (대한민국)", "kr", "82"], ["South Sudan (‫جنوب السودان‬‎)", "ss", "211"], ["Spain (España)", "es", "34"], ["Sri Lanka (ශ්‍රී ලංකාව)", "lk", "94"], ["Sudan (‫السودان‬‎)", "sd", "249"], ["Suriname", "sr", "597"], ["Svalbard and Jan Mayen", "sj", "47", 1, ["79"]], ["Swaziland", "sz", "268"], ["Sweden (Sverige)", "se", "46"], ["Switzerland (Schweiz)", "ch", "41"], ["Syria (‫سوريا‬‎)", "sy", "963"], ["Taiwan (台灣)", "tw", "886"], ["Tajikistan", "tj", "992"], ["Tanzania", "tz", "255"], ["Thailand (ไทย)", "th", "66"], ["Timor-Leste", "tl", "670"], ["Togo", "tg", "228"], ["Tokelau", "tk", "690"], ["Tonga", "to", "676"], ["Trinidad and Tobago", "tt", "1", 22, ["868"]], ["Tunisia (‫تونس‬‎)", "tn", "216"], ["Turkey (Türkiye)", "tr", "90"], ["Turkmenistan", "tm", "993"], ["Turks and Caicos Islands", "tc", "1", 23, ["649"]], ["Tuvalu", "tv", "688"], ["U.S. Virgin Islands", "vi", "1", 24, ["340"]], ["Uganda", "ug", "256"], ["Ukraine (Україна)", "ua", "380"], ["United Arab Emirates (‫الإمارات العربية المتحدة‬‎)", "ae", "971"], ["United Kingdom", "gb", "44", 0], ["United States", "us", "1", 0], ["Uruguay", "uy", "598"], ["Uzbekistan (Oʻzbekiston)", "uz", "998"], ["Vanuatu", "vu", "678"], ["Vatican City (Città del Vaticano)", "va", "39", 1, ["06698"]], ["Venezuela", "ve", "58"], ["Vietnam (Việt Nam)", "vn", "84"], ["Wallis and Futuna (Wallis-et-Futuna)", "wf", "681"], ["Western Sahara (‫الصحراء الغربية‬‎)", "eh", "212", 1, ["5288", "5289"]], ["Yemen (‫اليمن‬‎)", "ye", "967"], ["Zambia", "zm", "260"], ["Zimbabwe", "zw", "263"], ["Åland Islands", "ax", "358", 1, ["18"]]], i = 0; i < h.length; i++) {
//     var j = h[i];
//     h[i] = {
//       name: j[0],
//       iso2: j[1],
//       dialCode: j[2],
//       priority: j[3] || 0,
//       areaCodes: j[4] || null
//     };
//   }

//   b.intlTelInputGlobals = {
//     getInstance: function getInstance(a) {
//       var c = a.getAttribute("data-intl-tel-input-id");
//       return b.intlTelInputGlobals.instances[c];
//     },
//     instances: {}
//   };
//   var k = 0,
//       l = {
//     allowDropdown: !0,
//     autoHideDialCode: !0,
//     autoPlaceholder: "polite",
//     customContainer: "",
//     customPlaceholder: null,
//     dropdownContainer: null,
//     excludeCountries: [],
//     formatOnDisplay: !0,
//     geoIpLookup: null,
//     hiddenInput: "",
//     initialCountry: "",
//     localizedCountries: null,
//     nationalMode: !0,
//     onlyCountries: [],
//     placeholderNumberType: "MOBILE",
//     preferredCountries: ["us", "gb"],
//     separateDialCode: !1,
//     utilsScript: ""
//   },
//       m = ["800", "822", "833", "844", "855", "866", "877", "880", "881", "882", "883", "884", "885", "886", "887", "888", "889"];
//   b.addEventListener("load", function () {
//     b.intlTelInputGlobals.windowLoaded = !0;
//   });

//   var n = function n(a, b) {
//     for (var c = Object.keys(a), d = 0; d < c.length; d++) {
//       b(c[d], a[c[d]]);
//     }
//   },
//       o = function o(a) {
//     n(b.intlTelInputGlobals.instances, function (c) {
//       b.intlTelInputGlobals.instances[c][a]();
//     });
//   },
//       p = function () {
//     function a(b, c) {
//       var d = this;
//       e(this, a), this.id = k++, this.a = b, this.b = null, this.c = null;
//       var f = c || {};
//       this.d = {}, n(l, function (a, b) {
//         d.d[a] = f.hasOwnProperty(a) ? f[a] : b;
//       }), this.e = Boolean(b.getAttribute("placeholder"));
//     }

//     return g(a, [{
//       key: "_init",
//       value: function value() {
//         var a = this;

//         if (this.d.nationalMode && (this.d.autoHideDialCode = !1), this.d.separateDialCode && (this.d.autoHideDialCode = this.d.nationalMode = !1), this.g = /Android.+Mobile|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent), this.g && (c.body.classList.add("iti-mobile"), this.d.dropdownContainer || (this.d.dropdownContainer = c.body)), "undefined" != typeof Promise) {
//           var b = new Promise(function (b, c) {
//             a.h = b, a.i = c;
//           }),
//               d = new Promise(function (b, c) {
//             a.i0 = b, a.i1 = c;
//           });
//           this.promise = Promise.all([b, d]);
//         } else this.h = this.i = function () {}, this.i0 = this.i1 = function () {};

//         this.s = {}, this._b(), this._f(), this._h(), this._i(), this._i3();
//       }
//     }, {
//       key: "_b",
//       value: function value() {
//         this._d(), this._d2(), this._e(), this.d.localizedCountries && this._d0(), (this.d.onlyCountries.length || this.d.localizedCountries) && this.p.sort(this._d1);
//       }
//     }, {
//       key: "_c",
//       value: function value(a, b, c) {
//         b.length > this.dialCodeMaxLen && (this.dialCodeMaxLen = b.length), this.q.hasOwnProperty(b) || (this.q[b] = []);

//         for (var e = 0; e < this.q[b].length; e++) {
//           if (this.q[b][e] === a) return;
//         }

//         var f = c !== d ? c : this.q[b].length;
//         this.q[b][f] = a;
//       }
//     }, {
//       key: "_d",
//       value: function value() {
//         if (this.d.onlyCountries.length) {
//           var a = this.d.onlyCountries.map(function (a) {
//             return a.toLowerCase();
//           });
//           this.p = h.filter(function (b) {
//             return a.indexOf(b.iso2) > -1;
//           });
//         } else if (this.d.excludeCountries.length) {
//           var b = this.d.excludeCountries.map(function (a) {
//             return a.toLowerCase();
//           });
//           this.p = h.filter(function (a) {
//             return -1 === b.indexOf(a.iso2);
//           });
//         } else this.p = h;
//       }
//     }, {
//       key: "_d0",
//       value: function value() {
//         for (var a = 0; a < this.p.length; a++) {
//           var b = this.p[a].iso2.toLowerCase();
//           this.d.localizedCountries.hasOwnProperty(b) && (this.p[a].name = this.d.localizedCountries[b]);
//         }
//       }
//     }, {
//       key: "_d1",
//       value: function value(a, b) {
//         return a.name.localeCompare(b.name);
//       }
//     }, {
//       key: "_d2",
//       value: function value() {
//         this.dialCodeMaxLen = 0, this.q = {};

//         for (var a = 0; a < this.p.length; a++) {
//           var b = this.p[a];

//           this._c(b.iso2, b.dialCode, b.priority);
//         }

//         for (var c = 0; c < this.p.length; c++) {
//           var d = this.p[c];
//           if (d.areaCodes) for (var e = this.q[d.dialCode][0], f = 0; f < d.areaCodes.length; f++) {
//             for (var g = d.areaCodes[f], h = 1; h < g.length; h++) {
//               var i = d.dialCode + g.substr(0, h);
//               this._c(e, i), this._c(d.iso2, i);
//             }

//             this._c(d.iso2, d.dialCode + g);
//           }
//         }
//       }
//     }, {
//       key: "_e",
//       value: function value() {
//         this.preferredCountries = [];

//         for (var a = 0; a < this.d.preferredCountries.length; a++) {
//           var b = this.d.preferredCountries[a].toLowerCase(),
//               c = this._y(b, !1, !0);

//           c && this.preferredCountries.push(c);
//         }
//       }
//     }, {
//       key: "_e2",
//       value: function value(a, b, d) {
//         var e = c.createElement(a);
//         return b && n(b, function (a, b) {
//           return e.setAttribute(a, b);
//         }), d && d.appendChild(e), e;
//       }
//     }, {
//       key: "_f",
//       value: function value() {
//         this.a.setAttribute("autocomplete", "off");
//         var a = "iti";
//         this.d.allowDropdown && (a += " iti--allow-dropdown"), this.d.separateDialCode && (a += " iti--separate-dial-code"), this.d.customContainer && (a += " ", a += this.d.customContainer);

//         var b = this._e2("div", {
//           "class": a
//         });

//         if (this.a.parentNode.insertBefore(b, this.a), this.k = this._e2("div", {
//           "class": "iti__flag-container"
//         }, b), b.appendChild(this.a), this.selectedFlag = this._e2("div", {
//           "class": "iti__selected-flag",
//           role: "combobox",
//           "aria-owns": "country-listbox",
//           "aria-expanded": "false"
//         }, this.k), this.l = this._e2("div", {
//           "class": "iti__flag lazy"
//         }, this.selectedFlag), this.d.separateDialCode && (this.t = this._e2("div", {
//           "class": "iti__selected-dial-code"
//         }, this.selectedFlag)), this.d.allowDropdown && (this.selectedFlag.setAttribute("tabindex", "0"), this.u = this._e2("div", {
//           "class": "iti__arrow"
//         }, this.selectedFlag), this.m = this._e2("ul", {
//           "class": "iti__country-list iti__hide",
//           id: "country-listbox",
//           role: "listbox"
//         }), this.preferredCountries.length && (this._g(this.preferredCountries, "iti__preferred"), this._e2("li", {
//           "class": "iti__divider",
//           role: "separator",
//           "aria-disabled": "true"
//         }, this.m)), this._g(this.p, "iti__standard"), this.d.dropdownContainer ? (this.dropdown = this._e2("div", {
//           "class": "iti iti--container"
//         }), this.dropdown.appendChild(this.m)) : this.k.appendChild(this.m)), this.d.hiddenInput) {
//           var c = this.d.hiddenInput,
//               d = this.a.getAttribute("name");

//           if (d) {
//             var e = d.lastIndexOf("[");
//             -1 !== e && (c = "".concat(d.substr(0, e), "[").concat(c, "]"));
//           }

//           this.hiddenInput = this._e2("input", {
//             type: "hidden",
//             name: c
//           }), b.appendChild(this.hiddenInput);
//         }
//       }
//     }, {
//       key: "_g",
//       value: function value(a, b) {
//         for (var c = "", d = 0; d < a.length; d++) {
//           var e = a[d];
//           c += "<li class='iti__country ".concat(b, "' tabIndex='-1' id='iti-item-").concat(e.iso2, "' role='option' data-dial-code='").concat(e.dialCode, "' data-country-code='").concat(e.iso2, "'>"), c += "<div class='iti__flag-box'><div class='iti__flag iti__".concat(e.iso2, "'></div></div>"), c += "<span class='iti__country-name'>".concat(e.name, "</span>"), c += "<span class='iti__dial-code'>+".concat(e.dialCode, "</span>"), c += "</li>";
//         }

//         this.m.insertAdjacentHTML("beforeend", c);
//       }
//     }, {
//       key: "_h",
//       value: function value() {
//         var a = this.a.value,
//             b = this._5(a),
//             c = this._w(a),
//             d = this.d,
//             e = d.initialCountry,
//             f = d.nationalMode,
//             g = d.autoHideDialCode,
//             h = d.separateDialCode;

//         b && !c ? this._v(a) : "auto" !== e && (e ? this._z(e.toLowerCase()) : b && c ? this._z("us") : (this.j = this.preferredCountries.length ? this.preferredCountries[0].iso2 : this.p[0].iso2, a || this._z(this.j)), a || f || g || h || (this.a.value = "+".concat(this.s.dialCode))), a && this._u(a);
//       }
//     }, {
//       key: "_i",
//       value: function value() {
//         this._j(), this.d.autoHideDialCode && this._l(), this.d.allowDropdown && this._i2(), this.hiddenInput && this._i0();
//       }
//     }, {
//       key: "_i0",
//       value: function value() {
//         var a = this;
//         this._a14 = function () {
//           a.hiddenInput.value = a.getNumber();
//         }, this.a.form && this.a.form.addEventListener("submit", this._a14);
//       }
//     }, {
//       key: "_i1",
//       value: function value() {
//         for (var a = this.a; a && "LABEL" !== a.tagName;) {
//           a = a.parentNode;
//         }

//         return a;
//       }
//     }, {
//       key: "_i2",
//       value: function value() {
//         var a = this;

//         this._a9 = function (b) {
//           a.m.classList.contains("iti__hide") ? a.a.focus() : b.preventDefault();
//         };

//         var b = this._i1();

//         b && b.addEventListener("click", this._a9), this._a10 = function () {
//           !a.m.classList.contains("iti__hide") || a.a.disabled || a.a.readOnly || a._n();
//         }, this.selectedFlag.addEventListener("click", this._a10), this._a11 = function (b) {
//           a.m.classList.contains("iti__hide") && -1 !== ["ArrowUp", "Up", "ArrowDown", "Down", " ", "Enter"].indexOf(b.key) && (b.preventDefault(), b.stopPropagation(), a._n()), "Tab" === b.key && a._2();
//         }, this.k.addEventListener("keydown", this._a11);
//       }
//     }, {
//       key: "_i3",
//       value: function value() {
//         var a = this;
//         this.d.utilsScript && !b.intlTelInputUtils ? b.intlTelInputGlobals.windowLoaded ? b.intlTelInputGlobals.loadUtils(this.d.utilsScript) : b.addEventListener("load", function () {
//           b.intlTelInputGlobals.loadUtils(a.d.utilsScript);
//         }) : this.i0(), "auto" === this.d.initialCountry ? this._i4() : this.h();
//       }
//     }, {
//       key: "_i4",
//       value: function value() {
//         b.intlTelInputGlobals.autoCountry ? this.handleAutoCountry() : b.intlTelInputGlobals.startedLoadingAutoCountry || (b.intlTelInputGlobals.startedLoadingAutoCountry = !0, "function" == typeof this.d.geoIpLookup && this.d.geoIpLookup(function (a) {
//           b.intlTelInputGlobals.autoCountry = a.toLowerCase(), setTimeout(function () {
//             return o("handleAutoCountry");
//           });
//         }, function () {
//           return o("rejectAutoCountryPromise");
//         }));
//       }
//     }, {
//       key: "_j",
//       value: function value() {
//         var a = this;
//         this._a12 = function () {
//           a._v(a.a.value) && a._8();
//         }, this.a.addEventListener("keyup", this._a12), this._a13 = function () {
//           setTimeout(a._a12);
//         }, this.a.addEventListener("cut", this._a13), this.a.addEventListener("paste", this._a13);
//       }
//     }, {
//       key: "_j2",
//       value: function value(a) {
//         var b = this.a.getAttribute("maxlength");
//         return b && a.length > b ? a.substr(0, b) : a;
//       }
//     }, {
//       key: "_l",
//       value: function value() {
//         var a = this;
//         this._a8 = function () {
//           a._l2();
//         }, this.a.form && this.a.form.addEventListener("submit", this._a8), this.a.addEventListener("blur", this._a8);
//       }
//     }, {
//       key: "_l2",
//       value: function value() {
//         if ("+" === this.a.value.charAt(0)) {
//           var a = this._m(this.a.value);

//           a && this.s.dialCode !== a || (this.a.value = "");
//         }
//       }
//     }, {
//       key: "_m",
//       value: function value(a) {
//         return a.replace(/\D/g, "");
//       }
//     }, {
//       key: "_m2",
//       value: function value(a) {
//         var b = c.createEvent("Event");
//         b.initEvent(a, !0, !0), this.a.dispatchEvent(b);
//       }
//     }, {
//       key: "_n",
//       value: function value() {
//         this.m.classList.remove("iti__hide"), this.selectedFlag.setAttribute("aria-expanded", "true"), this._o(), this.b && (this._x(this.b, !1), this._3(this.b, !0)), this._p(), this.u.classList.add("iti__arrow--up"), this._m2("open:countrydropdown");
//       }
//     }, {
//       key: "_n2",
//       value: function value(a, b, c) {
//         c && !a.classList.contains(b) ? a.classList.add(b) : !c && a.classList.contains(b) && a.classList.remove(b);
//       }
//     }, {
//       key: "_o",
//       value: function value() {
//         var a = this;

//         if (this.d.dropdownContainer && this.d.dropdownContainer.appendChild(this.dropdown), !this.g) {
//           var d = this.a.getBoundingClientRect(),
//               e = b.pageYOffset || c.documentElement.scrollTop,
//               f = d.top + e,
//               g = this.m.offsetHeight,
//               h = f + this.a.offsetHeight + g < e + b.innerHeight,
//               i = f - g > e;

//           if (this._n2(this.m, "iti__country-list--dropup", !h && i), this.d.dropdownContainer) {
//             var j = !h && i ? 0 : this.a.offsetHeight;
//             this.dropdown.style.top = "".concat(f + j, "px"), this.dropdown.style.left = "".concat(d.left + c.body.scrollLeft, "px"), this._a4 = function () {
//               return a._2();
//             }, b.addEventListener("scroll", this._a4);
//           }
//         }
//       }
//     }, {
//       key: "_o2",
//       value: function value(a) {
//         for (var b = a; b && b !== this.m && !b.classList.contains("iti__country");) {
//           b = b.parentNode;
//         }

//         return b === this.m ? null : b;
//       }
//     }, {
//       key: "_p",
//       value: function value() {
//         var a = this;
//         this._a0 = function (b) {
//           var c = a._o2(b.target);

//           c && a._x(c, !1);
//         }, this.m.addEventListener("mouseover", this._a0), this._a1 = function (b) {
//           var c = a._o2(b.target);

//           c && a._1(c);
//         }, this.m.addEventListener("click", this._a1);
//         var b = !0;
//         this._a2 = function () {
//           b || a._2(), b = !1;
//         }, c.documentElement.addEventListener("click", this._a2);
//         var d = "",
//             e = null;
//         this._a3 = function (b) {
//           b.preventDefault(), "ArrowUp" === b.key || "Up" === b.key || "ArrowDown" === b.key || "Down" === b.key ? a._q(b.key) : "Enter" === b.key ? a._r() : "Escape" === b.key ? a._2() : /^[a-zA-ZÀ-ÿа-яА-Я ]$/.test(b.key) && (e && clearTimeout(e), d += b.key.toLowerCase(), a._s(d), e = setTimeout(function () {
//             d = "";
//           }, 1e3));
//         }, c.addEventListener("keydown", this._a3);
//       }
//     }, {
//       key: "_q",
//       value: function value(a) {
//         var b = "ArrowUp" === a || "Up" === a ? this.c.previousElementSibling : this.c.nextElementSibling;
//         b && (b.classList.contains("iti__divider") && (b = "ArrowUp" === a || "Up" === a ? b.previousElementSibling : b.nextElementSibling), this._x(b, !0));
//       }
//     }, {
//       key: "_r",
//       value: function value() {
//         this.c && this._1(this.c);
//       }
//     }, {
//       key: "_s",
//       value: function value(a) {
//         for (var b = 0; b < this.p.length; b++) {
//           if (this._t(this.p[b].name, a)) {
//             var c = this.m.querySelector("#iti-item-".concat(this.p[b].iso2));
//             this._x(c, !1), this._3(c, !0);
//             break;
//           }
//         }
//       }
//     }, {
//       key: "_t",
//       value: function value(a, b) {
//         return a.substr(0, b.length).toLowerCase() === b;
//       }
//     }, {
//       key: "_u",
//       value: function value(a) {
//         var c = a;

//         if (this.d.formatOnDisplay && b.intlTelInputUtils && this.s) {
//           var d = !this.d.separateDialCode && (this.d.nationalMode || "+" !== c.charAt(0)),
//               e = intlTelInputUtils.numberFormat,
//               f = e.NATIONAL,
//               g = e.INTERNATIONAL,
//               h = d ? f : g;
//           c = intlTelInputUtils.formatNumber(c, this.s.iso2, h);
//         }

//         c = this._7(c), this.a.value = c;
//       }
//     }, {
//       key: "_v",
//       value: function value(a) {
//         var b = a,
//             c = this.s.dialCode,
//             d = "1" === c;
//         b && this.d.nationalMode && d && "+" !== b.charAt(0) && ("1" !== b.charAt(0) && (b = "1".concat(b)), b = "+".concat(b)), this.d.separateDialCode && c && "+" !== b.charAt(0) && (b = "+".concat(c).concat(b));

//         var e = this._5(b),
//             f = this._m(b),
//             g = null;

//         if (e) {
//           var h = this.q[this._m(e)],
//               i = -1 !== h.indexOf(this.s.iso2) && f.length <= e.length - 1;

//           if (!("1" === c && this._w(f)) && !i) for (var j = 0; j < h.length; j++) {
//             if (h[j]) {
//               g = h[j];
//               break;
//             }
//           }
//         } else "+" === b.charAt(0) && f.length ? g = "" : b && "+" !== b || (g = this.j);

//         return null !== g && this._z(g);
//       }
//     }, {
//       key: "_w",
//       value: function value(a) {
//         var b = this._m(a);

//         if ("1" === b.charAt(0)) {
//           var c = b.substr(1, 3);
//           return -1 !== m.indexOf(c);
//         }

//         return !1;
//       }
//     }, {
//       key: "_x",
//       value: function value(a, b) {
//         var c = this.c;
//         c && c.classList.remove("iti__highlight"), this.c = a, this.c.classList.add("iti__highlight"), b && this.c.focus();
//       }
//     }, {
//       key: "_y",
//       value: function value(a, b, c) {
//         for (var d = b ? h : this.p, e = 0; e < d.length; e++) {
//           if (d[e].iso2 === a) return d[e];
//         }

//         if (c) return null;
//         throw new Error("No country data for '".concat(a, "'"));
//       }
//     }, {
//       key: "_z",
//       value: function value(a) {
//         var b = this.s.iso2 ? this.s : {};
//         this.s = a ? this._y(a, !1, !1) : {}, this.s.iso2 && (this.j = this.s.iso2), this.l.setAttribute("class", "iti__flag iti__".concat(a));
//         var c = a ? "".concat(this.s.name, ": +").concat(this.s.dialCode) : "Unknown";

//         if (this.selectedFlag.setAttribute("title", c), this.d.separateDialCode) {
//           var d = this.s.dialCode ? "+".concat(this.s.dialCode) : "";
//           this.t.innerHTML = d;

//           var e = this.selectedFlag.offsetWidth || this._getHiddenSelectedFlagWidth();

//           this.a.style.paddingLeft = "".concat(e + 6, "px");
//         }

//         if (this._0(), this.d.allowDropdown) {
//           var f = this.b;

//           if (f && (f.classList.remove("iti__active"), f.setAttribute("aria-selected", "false")), a) {
//             var g = this.m.querySelector("#iti-item-".concat(a));
//             g.setAttribute("aria-selected", "true"), g.classList.add("iti__active"), this.b = g, this.selectedFlag.setAttribute("aria-activedescendant", g.getAttribute("id"));
//           }
//         }

//         return b.iso2 !== a;
//       }
//     }, {
//       key: "_getHiddenSelectedFlagWidth",
//       value: function value() {
//         var a = this.a.parentNode.cloneNode();
//         a.style.visibility = "hidden", c.body.appendChild(a);
//         var b = this.selectedFlag.cloneNode(!0);
//         a.appendChild(b);
//         var d = b.offsetWidth;
//         return a.parentNode.removeChild(a), d;
//       }
//     }, {
//       key: "_0",
//       value: function value() {
//         var a = "aggressive" === this.d.autoPlaceholder || !this.e && "polite" === this.d.autoPlaceholder;

//         if (b.intlTelInputUtils && a) {
//           var c = intlTelInputUtils.numberType[this.d.placeholderNumberType],
//               d = this.s.iso2 ? intlTelInputUtils.getExampleNumber(this.s.iso2, this.d.nationalMode, c) : "";
//           d = this._7(d), "function" == typeof this.d.customPlaceholder && (d = this.d.customPlaceholder(d, this.s)), this.a.setAttribute("placeholder", d);
//         }
//       }
//     }, {
//       key: "_1",
//       value: function value(a) {
//         var b = this._z(a.getAttribute("data-country-code"));

//         this._2(), this._4(a.getAttribute("data-dial-code"), !0), this.a.focus();
//         var c = this.a.value.length;
//         this.a.setSelectionRange(c, c), b && this._8();
//       }
//     }, {
//       key: "_2",
//       value: function value() {
//         this.m.classList.add("iti__hide"), this.selectedFlag.setAttribute("aria-expanded", "false"), this.u.classList.remove("iti__arrow--up"), c.removeEventListener("keydown", this._a3), c.documentElement.removeEventListener("click", this._a2), this.m.removeEventListener("mouseover", this._a0), this.m.removeEventListener("click", this._a1), this.d.dropdownContainer && (this.g || b.removeEventListener("scroll", this._a4), this.dropdown.parentNode && this.dropdown.parentNode.removeChild(this.dropdown)), this._m2("close:countrydropdown");
//       }
//     }, {
//       key: "_3",
//       value: function value(a, d) {
//         var e = this.m,
//             f = b.pageYOffset || c.documentElement.scrollTop,
//             g = e.offsetHeight,
//             h = e.getBoundingClientRect().top + f,
//             i = h + g,
//             j = a.offsetHeight,
//             k = a.getBoundingClientRect().top + f,
//             l = k + j,
//             m = k - h + e.scrollTop,
//             n = g / 2 - j / 2;
//         if (k < h) d && (m -= n), e.scrollTop = m;else if (l > i) {
//           d && (m += n);
//           var o = g - j;
//           e.scrollTop = m - o;
//         }
//       }
//     }, {
//       key: "_4",
//       value: function value(a, b) {
//         var c,
//             d = this.a.value,
//             e = "+".concat(a);

//         if ("+" === d.charAt(0)) {
//           var f = this._5(d);

//           c = f ? d.replace(f, e) : e;
//         } else {
//           if (this.d.nationalMode || this.d.separateDialCode) return;
//           if (d) c = e + d;else {
//             if (!b && this.d.autoHideDialCode) return;
//             c = e;
//           }
//         }

//         this.a.value = c;
//       }
//     }, {
//       key: "_5",
//       value: function value(a) {
//         var b = "";
//         if ("+" === a.charAt(0)) for (var c = "", d = 0; d < a.length; d++) {
//           var e = a.charAt(d);
//           if (!isNaN(parseInt(e, 10)) && (c += e, this.q[c] && (b = a.substr(0, d + 1)), c.length === this.dialCodeMaxLen)) break;
//         }
//         return b;
//       }
//     }, {
//       key: "_6",
//       value: function value() {
//         var a = this.a.value.trim(),
//             b = this.s.dialCode,
//             c = this._m(a);

//         return (this.d.separateDialCode && "+" !== a.charAt(0) && b && c ? "+".concat(b) : "") + a;
//       }
//     }, {
//       key: "_7",
//       value: function value(a) {
//         var b = a;

//         if (this.d.separateDialCode) {
//           var c = this._5(b);

//           if (c) {
//             c = "+".concat(this.s.dialCode);
//             var d = " " === b[c.length] || "-" === b[c.length] ? c.length + 1 : c.length;
//             b = b.substr(d);
//           }
//         }

//         return this._j2(b);
//       }
//     }, {
//       key: "_8",
//       value: function value() {
//         this._m2("countrychange");
//       }
//     }, {
//       key: "handleAutoCountry",
//       value: function value() {
//         "auto" === this.d.initialCountry && (this.j = b.intlTelInputGlobals.autoCountry, this.a.value || this.setCountry(this.j), this.h());
//       }
//     }, {
//       key: "handleUtils",
//       value: function value() {
//         b.intlTelInputUtils && (this.a.value && this._u(this.a.value), this._0()), this.i0();
//       }
//     }, {
//       key: "destroy",
//       value: function value() {
//         var a = this.a.form;

//         if (this.d.allowDropdown) {
//           this._2(), this.selectedFlag.removeEventListener("click", this._a10), this.k.removeEventListener("keydown", this._a11);

//           var c = this._i1();

//           c && c.removeEventListener("click", this._a9);
//         }

//         this.hiddenInput && a && a.removeEventListener("submit", this._a14), this.d.autoHideDialCode && (a && a.removeEventListener("submit", this._a8), this.a.removeEventListener("blur", this._a8)), this.a.removeEventListener("keyup", this._a12), this.a.removeEventListener("cut", this._a13), this.a.removeEventListener("paste", this._a13), this.a.removeAttribute("data-intl-tel-input-id");
//         var d = this.a.parentNode;
//         d.parentNode.insertBefore(this.a, d), d.parentNode.removeChild(d), delete b.intlTelInputGlobals.instances[this.id];
//       }
//     }, {
//       key: "getExtension",
//       value: function value() {
//         return b.intlTelInputUtils ? intlTelInputUtils.getExtension(this._6(), this.s.iso2) : "";
//       }
//     }, {
//       key: "getNumber",
//       value: function value(a) {
//         if (b.intlTelInputUtils) {
//           var c = this.s.iso2;
//           return intlTelInputUtils.formatNumber(this._6(), c, a);
//         }

//         return "";
//       }
//     }, {
//       key: "getNumberType",
//       value: function value() {
//         return b.intlTelInputUtils ? intlTelInputUtils.getNumberType(this._6(), this.s.iso2) : -99;
//       }
//     }, {
//       key: "getSelectedCountryData",
//       value: function value() {
//         return this.s;
//       }
//     }, {
//       key: "getValidationError",
//       value: function value() {
//         if (b.intlTelInputUtils) {
//           var a = this.s.iso2;
//           return intlTelInputUtils.getValidationError(this._6(), a);
//         }

//         return -99;
//       }
//     }, {
//       key: "isValidNumber",
//       value: function value() {
//         var a = this._6().trim(),
//             c = this.d.nationalMode ? this.s.iso2 : "";

//         return b.intlTelInputUtils ? intlTelInputUtils.isValidNumber(a, c) : null;
//       }
//     }, {
//       key: "setCountry",
//       value: function value(a) {
//         var b = a.toLowerCase();
//         this.l.classList.contains("iti__".concat(b)) || (this._z(b), this._4(this.s.dialCode, !1), this._8());
//       }
//     }, {
//       key: "setNumber",
//       value: function value(a) {
//         var b = this._v(a);

//         this._u(a), b && this._8();
//       }
//     }, {
//       key: "setPlaceholderNumberType",
//       value: function value(a) {
//         this.d.placeholderNumberType = a, this._0();
//       }
//     }]), a;
//   }();

//   b.intlTelInputGlobals.getCountryData = function () {
//     return h;
//   };

//   var q = function q(a, b, d) {
//     var e = c.createElement("script");
//     e.onload = function () {
//       o("handleUtils"), b && b();
//     }, e.onerror = function () {
//       o("rejectUtilsScriptPromise"), d && d();
//     }, e.className = "iti-load-utils", e.async = !0, e.src = a, c.body.appendChild(e);
//   };

//   b.intlTelInputGlobals.loadUtils = function (a) {
//     if (!b.intlTelInputUtils && !b.intlTelInputGlobals.startedLoadingUtilsScript) {
//       if (b.intlTelInputGlobals.startedLoadingUtilsScript = !0, "undefined" != typeof Promise) return new Promise(function (b, c) {
//         return q(a, b, c);
//       });
//       q(a);
//     }

//     return null;
//   }, b.intlTelInputGlobals.defaults = l, b.intlTelInputGlobals.version = "16.0.15";

//   a.fn.intlTelInput = function (c) {
//     var e = arguments;
//     if (c === d || "object" == _typeof(c)) return this.each(function () {
//       if (!a.data(this, "plugin_intlTelInput")) {
//         var d = new p(this, c);
//         d._init(), b.intlTelInputGlobals.instances[d.id] = d, a.data(this, "plugin_intlTelInput", d);
//       }
//     });

//     if ("string" == typeof c && "_" !== c[0]) {
//       var f;
//       return this.each(function () {
//         var b = a.data(this, "plugin_intlTelInput");
//         b instanceof p && "function" == typeof b[c] && (f = b[c].apply(b, Array.prototype.slice.call(e, 1))), "destroy" === c && a.data(this, "plugin_intlTelInput", null);
//       }), f !== d ? f : this;
//     }
//   };
// }); // jQuery Mask Plugin v1.14.16
// // github.com/igorescobar/jQuery-Mask-Plugin

// var $jscomp = $jscomp || {};
// $jscomp.scope = {};

// $jscomp.findInternal = function (a, n, f) {
//   a instanceof String && (a = String(a));

//   for (var p = a.length, k = 0; k < p; k++) {
//     var b = a[k];
//     if (n.call(f, b, k, a)) return {
//       i: k,
//       v: b
//     };
//   }

//   return {
//     i: -1,
//     v: void 0
//   };
// };

// $jscomp.ASSUME_ES5 = !1;
// $jscomp.ASSUME_NO_NATIVE_MAP = !1;
// $jscomp.ASSUME_NO_NATIVE_SET = !1;
// $jscomp.SIMPLE_FROUND_POLYFILL = !1;
// $jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function (a, n, f) {
//   a != Array.prototype && a != Object.prototype && (a[n] = f.value);
// };

// $jscomp.getGlobal = function (a) {
//   return "undefined" != typeof window && window === a ? a : "undefined" != typeof global && null != global ? global : a;
// };

// $jscomp.global = $jscomp.getGlobal(this);

// $jscomp.polyfill = function (a, n, f, p) {
//   if (n) {
//     f = $jscomp.global;
//     a = a.split(".");

//     for (p = 0; p < a.length - 1; p++) {
//       var k = a[p];
//       k in f || (f[k] = {});
//       f = f[k];
//     }

//     a = a[a.length - 1];
//     p = f[a];
//     n = n(p);
//     n != p && null != n && $jscomp.defineProperty(f, a, {
//       configurable: !0,
//       writable: !0,
//       value: n
//     });
//   }
// };

// $jscomp.polyfill("Array.prototype.find", function (a) {
//   return a ? a : function (a, f) {
//     return $jscomp.findInternal(this, a, f).v;
//   };
// }, "es6", "es3");

// (function (a, n, f) {
//   "function" === typeof define && define.amd ? define(["jquery"], a) : "object" === (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" === typeof Meteor ? module.exports = a(require("jquery")) : a(n || f);
// })(function (a) {
//   var n = function n(b, d, e) {
//     var c = {
//       invalid: [],
//       getCaret: function getCaret() {
//         try {
//           var a = 0,
//               r = b.get(0),
//               h = document.selection,
//               d = r.selectionStart;

//           if (h && -1 === navigator.appVersion.indexOf("MSIE 10")) {
//             var e = h.createRange();
//             e.moveStart("character", -c.val().length);
//             a = e.text.length;
//           } else if (d || "0" === d) a = d;

//           return a;
//         } catch (C) {}
//       },
//       setCaret: function setCaret(a) {
//         try {
//           if (b.is(":focus")) {
//             var c = b.get(0);
//             if (c.setSelectionRange) c.setSelectionRange(a, a);else {
//               var g = c.createTextRange();
//               g.collapse(!0);
//               g.moveEnd("character", a);
//               g.moveStart("character", a);
//               g.select();
//             }
//           }
//         } catch (B) {}
//       },
//       events: function events() {
//         b.on("keydown.mask", function (a) {
//           b.data("mask-keycode", a.keyCode || a.which);
//           b.data("mask-previus-value", b.val());
//           b.data("mask-previus-caret-pos", c.getCaret());
//           c.maskDigitPosMapOld = c.maskDigitPosMap;
//         }).on(a.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", c.behaviour).on("paste.mask drop.mask", function () {
//           setTimeout(function () {
//             b.keydown().keyup();
//           }, 100);
//         }).on("change.mask", function () {
//           b.data("changed", !0);
//         }).on("blur.mask", function () {
//           f === c.val() || b.data("changed") || b.trigger("change");
//           b.data("changed", !1);
//         }).on("blur.mask", function () {
//           f = c.val();
//         }).on("focus.mask", function (b) {
//           !0 === e.selectOnFocus && a(b.target).select();
//         }).on("focusout.mask", function () {
//           e.clearIfNotMatch && !k.test(c.val()) && c.val("");
//         });
//       },
//       getRegexMask: function getRegexMask() {
//         for (var a = [], b, c, e, t, f = 0; f < d.length; f++) {
//           (b = l.translation[d.charAt(f)]) ? (c = b.pattern.toString().replace(/.{1}$|^.{1}/g, ""), e = b.optional, (b = b.recursive) ? (a.push(d.charAt(f)), t = {
//             digit: d.charAt(f),
//             pattern: c
//           }) : a.push(e || b ? c + "?" : c)) : a.push(d.charAt(f).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
//         }

//         a = a.join("");
//         t && (a = a.replace(new RegExp("(" + t.digit + "(.*" + t.digit + ")?)"), "($1)?").replace(new RegExp(t.digit, "g"), t.pattern));
//         return new RegExp(a);
//       },
//       destroyEvents: function destroyEvents() {
//         b.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "));
//       },
//       val: function val(a) {
//         var c = b.is("input") ? "val" : "text";

//         if (0 < arguments.length) {
//           if (b[c]() !== a) b[c](a);
//           c = b;
//         } else c = b[c]();

//         return c;
//       },
//       calculateCaretPosition: function calculateCaretPosition(a) {
//         var d = c.getMasked(),
//             h = c.getCaret();

//         if (a !== d) {
//           var e = b.data("mask-previus-caret-pos") || 0;
//           d = d.length;
//           var g = a.length,
//               f = a = 0,
//               l = 0,
//               k = 0,
//               m;

//           for (m = h; m < d && c.maskDigitPosMap[m]; m++) {
//             f++;
//           }

//           for (m = h - 1; 0 <= m && c.maskDigitPosMap[m]; m--) {
//             a++;
//           }

//           for (m = h - 1; 0 <= m; m--) {
//             c.maskDigitPosMap[m] && l++;
//           }

//           for (m = e - 1; 0 <= m; m--) {
//             c.maskDigitPosMapOld[m] && k++;
//           }

//           h > g ? h = 10 * d : e >= h && e !== g ? c.maskDigitPosMapOld[h] || (e = h, h = h - (k - l) - a, c.maskDigitPosMap[h] && (h = e)) : h > e && (h = h + (l - k) + f);
//         }

//         return h;
//       },
//       behaviour: function behaviour(d) {
//         d = d || window.event;
//         c.invalid = [];
//         var e = b.data("mask-keycode");

//         if (-1 === a.inArray(e, l.byPassKeys)) {
//           e = c.getMasked();
//           var h = c.getCaret(),
//               g = b.data("mask-previus-value") || "";
//           setTimeout(function () {
//             c.setCaret(c.calculateCaretPosition(g));
//           }, a.jMaskGlobals.keyStrokeCompensation);
//           c.val(e);
//           c.setCaret(h);
//           return c.callbacks(d);
//         }
//       },
//       getMasked: function getMasked(a, b) {
//         var h = [],
//             f = void 0 === b ? c.val() : b + "",
//             g = 0,
//             k = d.length,
//             n = 0,
//             p = f.length,
//             m = 1,
//             r = "push",
//             u = -1,
//             w = 0;
//         b = [];

//         if (e.reverse) {
//           r = "unshift";
//           m = -1;
//           var x = 0;
//           g = k - 1;
//           n = p - 1;

//           var A = function A() {
//             return -1 < g && -1 < n;
//           };
//         } else x = k - 1, A = function A() {
//           return g < k && n < p;
//         };

//         for (var z; A();) {
//           var y = d.charAt(g),
//               v = f.charAt(n),
//               q = l.translation[y];
//           if (q) v.match(q.pattern) ? (h[r](v), q.recursive && (-1 === u ? u = g : g === x && g !== u && (g = u - m), x === u && (g -= m)), g += m) : v === z ? (w--, z = void 0) : q.optional ? (g += m, n -= m) : q.fallback ? (h[r](q.fallback), g += m, n -= m) : c.invalid.push({
//             p: n,
//             v: v,
//             e: q.pattern
//           }), n += m;else {
//             if (!a) h[r](y);
//             v === y ? (b.push(n), n += m) : (z = y, b.push(n + w), w++);
//             g += m;
//           }
//         }

//         a = d.charAt(x);
//         k !== p + 1 || l.translation[a] || h.push(a);
//         h = h.join("");
//         c.mapMaskdigitPositions(h, b, p);
//         return h;
//       },
//       mapMaskdigitPositions: function mapMaskdigitPositions(a, b, d) {
//         a = e.reverse ? a.length - d : 0;
//         c.maskDigitPosMap = {};

//         for (d = 0; d < b.length; d++) {
//           c.maskDigitPosMap[b[d] + a] = 1;
//         }
//       },
//       callbacks: function callbacks(a) {
//         var g = c.val(),
//             h = g !== f,
//             k = [g, a, b, e],
//             l = function l(a, b, c) {
//           "function" === typeof e[a] && b && e[a].apply(this, c);
//         };

//         l("onChange", !0 === h, k);
//         l("onKeyPress", !0 === h, k);
//         l("onComplete", g.length === d.length, k);
//         l("onInvalid", 0 < c.invalid.length, [g, a, b, c.invalid, e]);
//       }
//     };
//     b = a(b);
//     var l = this,
//         f = c.val(),
//         k;
//     d = "function" === typeof d ? d(c.val(), void 0, b, e) : d;
//     l.mask = d;
//     l.options = e;

//     l.remove = function () {
//       var a = c.getCaret();
//       l.options.placeholder && b.removeAttr("placeholder");
//       b.data("mask-maxlength") && b.removeAttr("maxlength");
//       c.destroyEvents();
//       c.val(l.getCleanVal());
//       c.setCaret(a);
//       return b;
//     };

//     l.getCleanVal = function () {
//       return c.getMasked(!0);
//     };

//     l.getMaskedVal = function (a) {
//       return c.getMasked(!1, a);
//     };

//     l.init = function (g) {
//       g = g || !1;
//       e = e || {};
//       l.clearIfNotMatch = a.jMaskGlobals.clearIfNotMatch;
//       l.byPassKeys = a.jMaskGlobals.byPassKeys;
//       l.translation = a.extend({}, a.jMaskGlobals.translation, e.translation);
//       l = a.extend(!0, {}, l, e);
//       k = c.getRegexMask();
//       if (g) c.events(), c.val(c.getMasked());else {
//         e.placeholder && b.attr("placeholder", e.placeholder);
//         b.data("mask") && b.attr("autocomplete", "off");
//         g = 0;

//         for (var f = !0; g < d.length; g++) {
//           var h = l.translation[d.charAt(g)];

//           if (h && h.recursive) {
//             f = !1;
//             break;
//           }
//         }

//         f && b.attr("maxlength", d.length).data("mask-maxlength", !0);
//         c.destroyEvents();
//         c.events();
//         g = c.getCaret();
//         c.val(c.getMasked());
//         c.setCaret(g);
//       }
//     };

//     l.init(!b.is("input"));
//   };

//   a.maskWatchers = {};

//   var f = function f() {
//     var b = a(this),
//         d = {},
//         e = b.attr("data-mask");
//     b.attr("data-mask-reverse") && (d.reverse = !0);
//     b.attr("data-mask-clearifnotmatch") && (d.clearIfNotMatch = !0);
//     "true" === b.attr("data-mask-selectonfocus") && (d.selectOnFocus = !0);
//     if (p(b, e, d)) return b.data("mask", new n(this, e, d));
//   },
//       p = function p(b, d, e) {
//     e = e || {};
//     var c = a(b).data("mask"),
//         f = JSON.stringify;
//     b = a(b).val() || a(b).text();

//     try {
//       return "function" === typeof d && (d = d(b)), "object" !== _typeof(c) || f(c.options) !== f(e) || c.mask !== d;
//     } catch (w) {}
//   },
//       k = function k(a) {
//     var b = document.createElement("div");
//     a = "on" + a;
//     var e = (a in b);
//     e || (b.setAttribute(a, "return;"), e = "function" === typeof b[a]);
//     return e;
//   };

//   a.fn.mask = function (b, d) {
//     d = d || {};
//     var e = this.selector,
//         c = a.jMaskGlobals,
//         f = c.watchInterval;
//     c = d.watchInputs || c.watchInputs;

//     var k = function k() {
//       if (p(this, b, d)) return a(this).data("mask", new n(this, b, d));
//     };

//     a(this).each(k);
//     e && "" !== e && c && (clearInterval(a.maskWatchers[e]), a.maskWatchers[e] = setInterval(function () {
//       a(document).find(e).each(k);
//     }, f));
//     return this;
//   };

//   a.fn.masked = function (a) {
//     return this.data("mask").getMaskedVal(a);
//   };

//   a.fn.unmask = function () {
//     clearInterval(a.maskWatchers[this.selector]);
//     delete a.maskWatchers[this.selector];
//     return this.each(function () {
//       var b = a(this).data("mask");
//       b && b.remove().removeData("mask");
//     });
//   };

//   a.fn.cleanVal = function () {
//     return this.data("mask").getCleanVal();
//   };

//   a.applyDataMask = function (b) {
//     b = b || a.jMaskGlobals.maskElements;
//     (b instanceof a ? b : a(b)).filter(a.jMaskGlobals.dataMaskAttr).each(f);
//   };

//   k = {
//     maskElements: "input,td,span,div",
//     dataMaskAttr: "*[data-mask]",
//     dataMask: !0,
//     watchInterval: 300,
//     watchInputs: !0,
//     keyStrokeCompensation: 10,
//     useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && k("input"),
//     watchDataMask: !1,
//     byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
//     translation: {
//       0: {
//         pattern: /\d/
//       },
//       9: {
//         pattern: /\d/,
//         optional: !0
//       },
//       "#": {
//         pattern: /\d/,
//         recursive: !0
//       },
//       A: {
//         pattern: /[a-zA-Z0-9]/
//       },
//       S: {
//         pattern: /[a-zA-Z]/
//       }
//     }
//   };
//   a.jMaskGlobals = a.jMaskGlobals || {};
//   k = a.jMaskGlobals = a.extend(!0, {}, k, a.jMaskGlobals);
//   k.dataMask && a.applyDataMask();
//   setInterval(function () {
//     a.jMaskGlobals.watchDataMask && a.applyDataMask();
//   }, k.watchInterval);
// }, window.jQuery, window.Zepto);
